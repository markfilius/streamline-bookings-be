/**
 * Helper functions
 *
 * @description :: Server-side
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	isEmptyObject(obj) {
		for(var key in obj) {
			if(obj.hasOwnProperty(key))
				return false;
		}
		return true;
	},

	removeEmptyObjects(arr) {
		let filtered = arr.filter(el => {
			return !this.isEmptyObject(el);
		});
		return filtered;
	},

	uniquifyArray(arr) {
		// Return only the unique, non-empty elements, based on element.id
		var uniqueElements = [];
		arr.filter(el => {

			if (this.isEmptyObject(el)) return;

			var i = uniqueElements.findIndex(x => x.id === el.id);
			if(i <= -1)
				uniqueElements.push(el);
		});
		return uniqueElements;
	},
	
	collateResults(collateTo, collateThese) {

		// Collect the info i.e. collate all properties from collateThese to unique collateTo
		let collatedList = [];
		collateTo.forEach((item, index) => {

			if (this.isEmptyObject(item)) return;       // Skip empty items

			let i = collatedList.findIndex(cl => cl.id === item.id);
			if (i < 0) {
				// Create the first instance of this item in the collated list, with an empty list per key
				Object.keys(collateThese).forEach(k => item[k] = []);

				collatedList.push(item);
				i = collatedList.length - 1;
			}

			// Add
			Object.keys(collateThese).forEach(k => {
				collatedList[i][k].push(collateThese[k][index]);
			});
		});

		// Uniquify the results
		collatedList.forEach(item => {
			Object.keys(collateThese).forEach(k => {
				item[k] = this.uniquifyArray(item[k]);
			})
		});

		return collatedList;
	}
}

