/**
 * isAllowedService
 *
 * @description ::
 * @help        ::
 */

module.exports = {

	isAllowed(token, what) {

		const decoded = jwTokenService.decode(token);
		const person = decoded.payload.person;          // Logged in person
		const venue = decoded.payload.venue;            // Venue attached to this logged in person

		// sails.log.info('ISALLOWED', decoded, venue, person, what);

		// First test if this person has specific permissions (e.g. a carer)
		// Then if the venue allows that action (e.g. for a staff member)
		// Note: venue permissions are defined at login and are also dependant on that logged in person

		if (person[what]) return true;

		return venue[what] ? true : false;
	}
}

