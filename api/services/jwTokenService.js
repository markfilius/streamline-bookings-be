'use strict';

const jwt = require('jsonwebtoken');
const tokenSecret = 'vr4p-6-A5K_Zgn_a3XEawqXX+VBJQu9Mvr4p-6-A5K_Zgn_a3XEawqXX+VBJQu9Mvr4p-6-A5K_Zgn_a3XEawqXX+VBJQu9M';

module.exports = {
	// Generates a token from supplied payload
	issue(payload) {
		return jwt.sign(
			payload,
			tokenSecret,
			{
				expiresIn: "5 days"
			});
	},

	// Verifies token on a request
	verify(token, callback) {
		return jwt.verify(
			token,              // The token to be verified
			tokenSecret,        // Same token we used to sign
			{},                 // No Option, for more see https://github.com/auth0/node-jsonwebtoken#jwtverifytoken-secretorpublickey-options-callback
			callback            // Pass errors or decoded token to callback
		);
	},

	decode(token) {
		return jwt.decode(token, {complete: true});
	}
};