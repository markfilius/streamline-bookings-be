/**
 * DbController
 *
 * @description :: Server-side logic for ...............
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const neo4j = require('neo4j-driver').v1;


module.exports = {

	connect() {

		const neo4jDriver = neo4j.driver(
			sails.config.neo4j.url,
			neo4j.auth.basic(
				sails.config.neo4j.username,
				sails.config.neo4j.password
			)
		);

		neo4jDriver.onError = function (error) {
			sails.log.info('neo4jDriver instantiation failed', error);
		};

		// neo4jDriver.onCompleted = function () { sails.log.info('neo4jDriver ok'); };

		return neo4jDriver.session();
	},

	formatResult (result, key) {
		let values = [];
		let keyData;
		result.records.forEach(data => {

			keyData = data.get(key) || {};
			if (neo4j.isInt(keyData)) {
				if (neo4j.integer.inSafeRange(keyData)) {
					values.push(keyData.toNumber());
				} else {
					values.push(keyData.toString());
				}
			} else {
				values.push(keyData.properties || {});
			}
		});
		return values;
	},

	migrate() {

		sails.log.info('MIGRATION Started');
		const db = this.connect();

		if (1) {
			sails.log.info('Creating SwimKwik');
			sails.log.info('Creating Schools...');
			db.run('merge (s:School {name: "SwimKwik", phoneNumber: "07-swimkwik", id: "9123456"})')
				.then(function (s) {
					sails.log.info('Suc cc School: ', s)
				}).catch(function (e) {
				sails.log.info('Err cc School: ', e)
			});

			sails.log.info('Creating Venues...');
			db.run('merge (v:Venue {name: "Bondi", phoneNumber: "02-bondi", id:"b1234567"})')
				.then(function (s) {
					sails.log.info('Suc cc Venue: ', s)
				}).catch(function (e) {
				sails.log.info('Err cc Venue: ', e)
			});
			db.run('merge (v:Venue {name: "Mooloolaba", phoneNumber: "07-moo", id:"m1234567"})')
				.then(function (s) {
					sails.log.info('Suc cc Venue: ')
				}).catch(function (e) {
				sails.log.info('Err cc Venue: ', e)
			});

			sails.log.info('Creating HasVenue');
			db.run(
				'match (s:School {name: "SwimKwik"}) ' +
				'match (v:Venue {name: "Mooloolaba"}) ' +
				'merge (s)-[:HasVenue]->(v)')
				.then(function (s) {
					sails.log.info('Suc create HasVenue')
				}).catch(function (e) {
				sails.log.info('Err create HasVenue:', e)
			});

			db.run(
				'match (s:School {name: "SwimKwik"}) ' +
				'match (v:Venue {name: "Bondi"}) ' +
				'merge (s)-[:HasVenue]->(v)')
				.then(function (s) {
					sails.log.info('Suc create HasVenue')
				}).catch(function (e) {
				sails.log.info('Err create HasVenue:', e)
			});

			sails.log.info('Creating Calendar');
			db.run(
				'match (v:Venue {name: "Mooloolaba"}) ' +
				'merge (v)-[:HasCalendar]->(:Calendar {id: "mc1234567"})' +
				'with v ' +
				'match (v2:Venue {name: "Bondi"}) ' +
				'merge (v2)-[:HasCalendar]->(:Calendar {id: "bc234567"})')
				.then(function (s) {
					sails.log.info('Suc create Calendar')
				}).catch(function (e) {
				sails.log.info('Err create Calendar:', e)
			});

			sails.log.info('Creating Persons...');
			db.run("merge (p:Person {" +
				"id : 'bartonstevens123456', " +
				"firstName : 'Barton', " +
				"lastName : 'Stevens', " +
				"displayName : 'Barton', " +
				"name : 'Barton Stevens', " +
				"phoneNumber : '04-123', " +
				"email : 'barton@streamlinebookings.com', " +
				"password : '$2a$10$LWGf0eVXuRRvGkAKg2ZIOOTswf6GBX1E1SgSwJLHRlgtGQl5FuFiC' " +
				"})"
			)
				.then(function (s) {
					sails.log.info('Suc cv BS: ')
				}).catch(function (e) {
				sails.log.info('Err cv BS: ', e)
			});

			db.run("merge (p:Person {" +
				"id : 'markfilius123456', " +
				"firstName : 'Mark', " +
				"lastName : 'Filius', " +
				"displayName : 'Mark', " +
				"name : 'Mark Filius', " +
				"phoneNumber : '04-456', " +
				"email : 'mark@streamlinebookings.com', " +
				"password : '$2a$10$LWGf0eVXuRRvGkAKg2ZIOOTswf6GBX1E1SgSwJLHRlgtGQl5FuFiC' " +
				"})"
			)
				.then(function (s) {
					sails.log.info('Suc cv MF: ')
				}).catch(function (e) {
				sails.log.info('Err cv MF: ', e)
			});

			db.run("merge (p:Person {" +
				"id : 'andrewandrewson234567', " +
				"firstName : 'Andrew', " +
				"lastName : 'Andrewson', " +
				"displayName : 'Andy', " +
				"name : 'Andrew Andrewson', " +
				"phoneNumber : '04-789', " +
				"email : 'andrew@streamlinebookings.com', " +
				"password : '$2a$10$LWGf0eVXuRRvGkAKg2ZIOOTswf6GBX1E1SgSwJLHRlgtGQl5FuFiC' " +
				"})"
			)
				.then(function (s) {
					sails.log.info('Suc cv AA: ')
				}).catch(function (e) {
				sails.log.info('Err cv AA: ', e)
			});

			sails.log.info('Creating Employs');
			db.run(
				'match (v:Venue {name: "Bondi"}) ' +
				'match (p:Person {email: "barton@streamlinebookings.com"}) ' +
				'merge (v)-[:Employs {instructor: true, admin: true}]->(p)')
				.then(function (s) {
					sails.log.info('Suc create Employs B')
				}).catch(function (e) {
				sails.log.info('Err create Employs:', e)
			});

			db.run(
				'match (v:Venue {name: "Mooloolaba"}) ' +
				'match (p:Person {email: "mark@streamlinebookings.com"}) ' +
				'merge (v)-[:Employs {instructor: true, admin: true}]->(p)')
				.then(function (s) {
					sails.log.info('Suc create Employs M')
				}).catch(function (e) {
				sails.log.info('Err create Employs:', e)
			});

			db.run(
				'match (v:Venue {name: "Mooloolaba"}) ' +
				'match (p:Person {email: "andrew@streamlinebookings.com"}) ' +
				'merge (v)-[:Employs {instructor: true}]->(p)')
				.then(function (s) {
					sails.log.info('Suc create Employs AA')
				}).catch(function (e) {
				sails.log.info('Err create Employs AA:', e)
			});

		}

		if (1) {
			sails.log.info('Creating SamfordAquatics');
			sails.log.info('Creating Schools...');
			db.run('merge (s:School {name: "Samford Aquatics", ' +
				'website: "http://www.samfordaquatics.com.au", ' +
				'phoneNumber: "(07) 3289 3815", ' +
				'id: "samford98787210"})')
				.then(function (s) {
					sails.log.info('Suc cc School: ', s)
				}).catch(function (e) {
				sails.log.info('Err cc School: ', e)
			});

			sails.log.info('Creating Venues...');
			db.run('merge (v:Venue {name: "Samford", phoneNumber: "(07) 3289 3815", id:"samfordvenue548913"})')
				.then(function (s) {
					sails.log.info('Suc cc Venue: ', s)
				}).catch(function (e) {
				sails.log.info('Err cc Venue: ', e)
			});

			sails.log.info('Creating HasVenue');
			db.run(
				'match (s:School {name: "Samford Aquatics"}) ' +
				'match (v:Venue {name: "Samford"}) ' +
				'merge (s)-[:HasVenue]->(v)')
				.then(function (s) {
					sails.log.info('Suc create HasVenue')
				}).catch(function (e) {
				sails.log.info('Err create HasVenue:', e)
			});

			sails.log.info('Creating Calendar');
			db.run(
				'match (v:Venue {name: "Samford"}) ' +
				'merge (v)-[:HasCalendar]->(:Calendar {id: "samfordcalendar83187023"})')
				.then(function (s) {
					sails.log.info('Suc create Calendar')
				}).catch(function (e) {
				sails.log.info('Err create Calendar:', e)
			});

			sails.log.info('Creating Persons...');
			db.run("merge (p:Person {" +
				"id : 'catherine9827340923', " +
				"firstName : 'Catherine', " +
				"lastName : 'de Samford', " +
				"displayName : 'Catherine', " +
				"name : 'Catherine de Samford', " +
				"phoneNumber : '04-catherine', " +
				"email : 'catherine@samfordaquatics.com.au', " +
				"password : '$2a$10$LWGf0eVXuRRvGkAKg2ZIOOTswf6GBX1E1SgSwJLHRlgtGQl5FuFiC' " +
				"})"
			)
				.then(function (s) {
					sails.log.info('Suc cp Catherine: ')
				}).catch(function (e) {
				sails.log.info('Err cp Catherine: ', e)
			});

			sails.log.info('Creating Employs');
			db.run(
				'match (v:Venue {name: "Samford"}) ' +
				'match (p:Person {email: "catherine@samfordaquatics.com.au"}) ' +
				'merge (v)-[:Employs {instructor: true, admin: true}]->(p)')
				.then(function (s) {
					sails.log.info('Suc create Employs C')
				}).catch(function (e) {
				sails.log.info('Err create Employs C:', e)
			});
		}

		return true;
	}
}

