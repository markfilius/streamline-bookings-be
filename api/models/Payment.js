/**
 * Payment.js
 *
 * @description :: A model definition for :Payment
 *
 * "A model manages itself and outgoing relationships"
 *
 */

const db            = dbService.connect();
const hashService   = require('sails-service-hash');
const bcrypt        = hashService('bcrypt');
const shortid       = require('shortid');
const moment        = require('moment');
const pdfDocument   = require('pdfkit');
const fs            = require('fs');
const fetch         = require('node-fetch');
const nodemailer    = require('nodemailer');
const Message       = require('../models/Message');

const momentDatetimeFormat = 'dddd, Do MMM h:mma';


module.exports = {

	attributes: {
		payment: {
			type: 'ref',
			required: true,
		},
	},

	/*
	 * Execute a payment in the payment gateway and record in our db
	 * The payment is for the swimmer in the class (that contains the price)
	 * Expect the payment object (of the new payment) to contain:
	 *      paymentMethod or paymentMethodId
     */
	execute: async function(token, payment, oneClass, swimmer) {
		sails.log.info('PAYMENTEXECUTE', payment, oneClass.id, swimmer.id);

		if (!token) return;
		if (!payment) return;
		if (!oneClass) return;
		if (!swimmer) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		// Get the payment method and the person who own's it
		let paymentPromise = db
			.run(
				'match (venue:Venue {id: $venueId}) ' +
				'match (paymentMethod:PaymentMethod {id : $paymentMethodId})<-[:PaysUsing]-(payer:Person)<--(:Group)<--(venue) ' +
				'optional match (payer)-[:HasCredit]->(credits:Credit) ' +
				'return paymentMethod, payer, credits',
				{
					venueId: venue.id,
					paymentMethodId: payment.paymentMethodId,
				})
			.then(response => {
				sails.log.info('PAYMENTEXECUTE response:', response);

				let paymentMethod = dbService.formatResult(response, 'paymentMethod')[0];
				let payer = dbService.formatResult(response, 'payer')[0];                   // The payer belonging to the payment method
				let credits = dbService.formatResult(response, 'credits');                  // The credits accrued by this payer

				// Decide the price
				let price = 0;
				if (oneClass.terms && oneClass.terms.length > 0 && oneClass.terms[0].singleRate) {
					price = oneClass.terms[0].singleRate;

				} else {
					sails.log.warn('ERROR: TERMs OR TERMs-PRICES NOT FOUND', oneClass);
					return false;
				}

				// Define a paymentId
				let paymentId = shortid.generate();

				// Use credits in the payment
				credits = helperService.uniquifyArray(credits);
				let totalCredits = 0;
				credits.forEach(credit => totalCredits += credit.amount);
				let toPay = Math.max(0, price - totalCredits);
				let usedCredits = price - toPay;
				
				if (toPay <= 0) {
					// Payment not required, all is covered by credits, so just update our db and return
					let paymentDescription = {
						amountPaid: toPay,
						payer: payer,
						paymentId: paymentId,
						paymentMethod: {},
						usedCredits: usedCredits,
					};
					return this.create(token, paymentDescription, oneClass, swimmer);
				}

				// Request a payment thru payment gateway
				let gatewayPromise = fetch(sails.config.payGateUrl + 'purchases', {
						method: 'post',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': sails.config.payGateAuth,
						},
						body: JSON.stringify({
							amount: price,
							reference: paymentId + ' ' + swimmer.firstName + swimmer.lastName,
							customer_ip: '123.456.789.012',
							card_token: paymentMethod.cardToken,
							currency: sails.config.currency,
						}),
					})
					.then(response => {
						sails.log.info('PAYGRAWRESPONSE success', response.status, response);

						let responseDataPromise = response
							.json()
							.then(responseData => {
								sails.log.info('PAYGRESPONSE', responseData);

								if (!responseData.response.successful) {
									sails.log.warn('PAYGRESPONSE UNSUCCESSFUL', responseData.response.message, responseData.errors.join(', '));
									return false;
								}

								// Payment was successful, so update our db and return
								let paymentDescription = {
									amountPaid: price,
									payer: payer,
									paymentId: paymentId,
									paymentMethodId: payment.paymentMethodId,
									receiptId: responseData.response.transaction_id,
								};
								return this.create(token, paymentDescription, oneClass, swimmer);
							})
							.catch(response => {
								sails.log.warn('PAYGRESPONSE error', response);
								return false;
							});
						
						return responseDataPromise;
					})
					.catch(response => {
						sails.log.warn('PAYGRAWRESPONSE error', response);
						return false;
					});
				
				return gatewayPromise;
			})
			.catch(response => {
				sails.log.warn('MODELPAYMENT Err execute:', response)
				return false;
			});

		return paymentPromise;
	},

	/*
	 * Create a local payment (ie. in our db).
	 * Payments can be executed on mobile, they create a payment Id there so it can be given to the payment gateway.
	 * Payments can be executed in be, they also create a payment Id.
	 *
	 * If credits have been used: generate a negative credit and link to this payment
	 *
	 * Expect the payment object (of the new payment) to contain:
	 *      amountPaid
	 *      paymentId (optional)
	 *      paymentMethod or paymentMethodId
	 *      receiptId
	 *      usedCredits (optional)
	 */
	create(token, payment = null, oneClass = null, swimmer = null) {

		if (!token) return;
		if (!payment) return;
		if (!oneClass) return;
		if (!swimmer) return;

		const decoded = jwTokenService.decode(token);
		const person = decoded.payload.person;
		const venue = decoded.payload.venue;

		// The payer is either specified (probably through execute)
		// or the logged in person (from the token, in that case probably from a carer through mobile)
		const payer = payment.payer || person;

		sails.log.info('MODELPAYMENT CREATE1', venue, person, payer);
		sails.log.info('MODELPAYMENT CREATE2', payment, oneClass, swimmer);

		// Get or generate an unique id
		let paymentId = payment.id || shortid.generate();

		// Generate the invoice file name
		let invoiceFileName = moment().format('YYYYMMDD-') +
								payer.firstName + payer.lastName + '-' +
								swimmer.firstName + '-' +
								paymentId + '.pdf';

		// Create the nodes and relationships
		let cypher =
			// Create the payment node
			'match (payer:Person {id: $payerId}) ' +
			'match (venue:Venue {id: $venueId})-[:HasGroup]->(group:Group)-[:HasPerson]->(payer) ' +
			// Get next invoice number
			'merge (sequence:Sequence {name: $invoiceSequenceVenue}) ' +
			'on create set sequence.current = 1000 ' +
			'on match set sequence.current = sequence.current + 1 ' +
			'with sequence.current as invoiceNumber, venue, group, payer ' +
			// Create the payment
			'merge (group)-[:HasPayment]->(payment:Payment { ' +
				'id: $paymentId, ' +
				'amount: $paymentAmount, ' +
				'datetime: $paymentDatetime, ' +
				'singleRate: $singleRate, ' +
				'termRate: $termRate, ' +
				'receiptId: $receiptId, ' +
				'invoiceNumber: invoiceNumber, ' +
				'invoiceFileName: $invoiceFileName ' +
			'}) ' +
			'with venue, payer, payment ';

		cypher += payment.usedCredits ?
			// Create the used credit
			'merge (payment)-[:UsesCredit]->(usedCredits:Credit {' +
				'id: $usedCreditsId, ' +
				'amount: $usedCredits, ' +
				'datetime: $usedCreditsDatetime ' +
			'}) ' +
			'merge (payer)-[:HasCredit]->(usedCredits) ' +
			'with venue, payer, payment '
			: '';

		cypher +=
			// Find for who, by, etc
			// Note: the class relationship to the swimmer may not yet exist (it's created in parallel)
			'match (swimmer:Person {id: $swimmerId}) ' +
			'match (venue)-[:HasCalendar]->(:Calendar)-[:HasClass]->(class:Class {id: $classId}) ' +
			// Create relationships to payers and paidFor
			'merge (payment)-[:PaidBy]->(payer) ' +
			'merge (payment)-[:PaidFor]->(swimmer) ' +
			'merge (payment)-[:PaidFor]->(class) ' +
			'with payment ';

		if (!_.isEmpty(payment.paymentMethod)) {
			// Add the payment method
			cypher +=
				'match (paymentMethod {id: $paymentMethodId}) ' +
				'merge (payment)-[:PaidUsing]->(paymentMethod) ' +
				'return payment, paymentMethod';
		} else {
			cypher +=
				'return payment';

		}
		sails.log.info('MODELPAYMENT parameters:', cypher,
			payer.id, venue.id, paymentId, payment.amountPaid, payment.paymentMethodId, payment.paymentMethod,
			moment().format(),
			swimmer.id, oneClass.id);

		let paymentPromise = db
			.run(cypher,
				{
					payerId: payer.id,
					venueId: venue.id,
					invoiceSequenceVenue: 'invoiceNumbersFor' + venue.id,
					paymentId: paymentId,
					paymentAmount: payment.amountPaid,
					paymentMethodId: payment.paymentMethodId || payment.paymentMethod.id || '(no payment method)',
					paymentDatetime: moment().format(),
					singleRate: oneClass.terms.length > 0 ? oneClass.terms[0].singleRate : null,
					termRate:   oneClass.terms.length > 0 ? oneClass.terms[0].termRate : null,
					receiptId: payment.receiptId || '(no receipt)',
					invoiceFileName: invoiceFileName || '(invoice missing)',
					usedCredits: -1 * (payment.usedCredits || 0),
					usedCreditsDatetime: moment().format(),
					usedCreditsId: shortid.generate(),
					swimmerId: swimmer.id,
					classId: oneClass.id,
				}
			)
			.then(function (result) {
				sails.log.info('MODELPAYMENT Success create:', result);
				let paymentDb = dbService.formatResult(result, 'payment')[0];
				let paymentMethod = !_.isEmpty(payment.paymentMethod) ? dbService.formatResult(result, 'paymentMethod')[0] : null;

				// Generate the actual invoice pdf
				let pdfDoc = new pdfDocument;
				let description = swimmer.firstName + ' ' + swimmer.lastName +
					' in ' + oneClass.level.name +
					' at ' + moment(oneClass.datetime).format(momentDatetimeFormat);
				pdfDoc.pipe(fs.createWriteStream(sails.config.invoicesRootFolder + invoiceFileName));
				pdfDoc.text('INVOICE');
				pdfDoc.text('Date        : ' + moment().format('DD MMMM YYYY'));
				pdfDoc.text('Invoice nr  : ' + paymentDb.invoiceNumber);
				pdfDoc.moveDown();
				pdfDoc.text('Amount paid : $ ' + (paymentDb.amount / 100).toFixed(2));
				if (payment.usedCredits) {
					pdfDoc.text('Credits used: $ ' + (payment.usedCredits / 100).toFixed(2));
				}
				pdfDoc.text('Paid for    : ' + swimmer.firstName + ' ' + swimmer.lastName);
				pdfDoc.text('Level       : ' + oneClass.level.name);

				if (oneClass.oneOrTerm != 'term') {
					pdfDoc.text('Class       : ' + moment(oneClass.datetime).format(momentDatetimeFormat));
				} else {
					pdfDoc.text('Classes     : ' + oneClass.remainingLessons + ' classes starting ' + moment(oneClass.datetime).format(momentDatetimeFormat));
					pdfDoc.text('Recurring   : ' + oneClass.repeatShortDescription);
				}

				pdfDoc.text('Paid by     : ' + payer.firstName + ' ' + payer.lastName );
				if (paymentMethod) {
					pdfDoc.text('Paid using  : ' + paymentMethod.type + ': ****' + paymentMethod.last4);
				}
				pdfDoc.moveDown();
				pdfDoc.end();

				// Email the invoice
				let br = '<br>';
				let emailContent = '';
				emailContent += 'Hello ' + payer.firstName + ',';
				emailContent += br;
				emailContent += br + 'Thanks for booking ' + (oneClass === 'term' ? oneClass.remainingLessons + ' lessons ' : 'a lesson for ') + swimmer.firstName + '.';
				emailContent += br;
				emailContent += br + 'Please find attached to this email your invoice.';
				emailContent += br;
				emailContent += br + 'Kind regards,';
				emailContent += br + venue.name;
				emailContent += br;
				emailContent += br + 'Please note: you can\'t reply to this email address';
				Message
					.sendEmail({
						token: token,
						from: 'noReply',
						to: payer.email,
						subject: 'Your invoice for lessons for ' + swimmer.firstName + '. Thank you!',
						content: emailContent,
						attachments: [sails.config.invoicesRootFolder + invoiceFileName],
						isHtml: true,
					})
					.then(response => {
						sails.log.info('MODELPAYMENT Email invoice success:', response)
					})
					.catch(response => {
						sails.log.warn('MODELPAYMENT Email invoice error:', response)
					});

				return paymentDb;
			})
			.catch(function (err) {
				sails.log.warn('MODELPAYMENT Err create:', err)
				return err;
			});

		return paymentPromise;
	},

	/*
	 * Credit the payment for a class.
	 * Credit the class single lesson rate to the payer (ie the logged in person)
	 */
	creditPayment(token, oneClass, swimmer) {

		if (!token) return;
		if (!oneClass) return;
		if (!swimmer) return;

		const decoded = jwTokenService.decode(token);
		const person = decoded.payload.person;
		const venue = decoded.payload.venue;
		sails.log.info('MODELPAYMENT CREDIT', venue, person, oneClass, swimmer);

		// Generate an unique id
		let creditId = shortid.generate();

		let cypher =
			// Find our group
			'match (person:Person {id: $personId}) ' +
			'match (venue:Venue {id: $venueId})-[:HasGroup]->(group:Group)-[:HasPerson]->(person) ' +
			// Find the group's payment for the swimmer
			'match (group)-[:HasPayment]->(payment:Payment) ' +
			'match (payment)-[:PaidFor]->(swimmer:Person {id: $swimmerId}) ' +
			// Find the group's payment for the class. A repeating class can have been paid through one of the other
			// occurrences of the class, so find the first payment for the repeated class. 'First payment' because
			// repeated classes can be booked separately from ach other
			'match (payment)-[:PaidFor]->' +
				(oneClass.repeatId ? '(:Class {repeatId: $repeatId}) ' : '(:Class {id: $classId}) ') +
				'with payment limit 1 ' +
			// Find who paid
			'match (payment)-[:PaidBy]->(payer:Person)<-[:HasPerson]-(group) ' +
			'with payer, payment ' +
			// Create the credit and back-link
			'merge (payer)-[:HasCredit]->(credit:Credit {' +
				'id: $creditId, ' +
				'amount: payment.singleRate, ' +
				'datetime: $datetime ' +
			'}) ' +
			'merge (credit)-[:CreditFor]->(payment) ';

		let params = {
			personId: person.id,
			venueId: venue.id,
			swimmerId: swimmer.id,
			classId: oneClass.id,
			repeatId: oneClass.repeatId,
			creditId: creditId,
			datetime: moment().format(),
		};

		let paymentPromise = db
			.run(cypher, params)
			.then(response => {
				sails.log.info('MODELCREDIT Success:', response, params);

			})
			.catch(response => {
				sails.log.warn('MODELCREDIT Error:', response, params);

			});

		return paymentPromise;
	},
}




