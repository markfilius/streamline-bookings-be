/**
 * Class.js
 *
 * @description :: A model definition for :Class
 *
 * "A model manages itself and outgoing relationships"
 *
 */

const db            = dbService.connect();
const hashService   = require('sails-service-hash');
const bcrypt        = hashService('bcrypt');
let shortid         = require('shortid');
let moment          = require('moment');
let Message         = require('../models/Message');
let Payment         = require('../models/Payment');
const _             = require('@sailshq/lodash');


module.exports = {

	attributes: {
		payment: {
			type: 'ref',
			required: true,
		},
	},

	// TODO cypher queries using params

	/*
	 * Get all the classes for the venue
	 * Optionally only for 1 level and only future classes
	 */
	listAll(token, levelId = null, futureOnly = false) {

		if (!token) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;
		sails.log.info('LISTCLASSES for ', venue, levelId, futureOnly);

		let classPromise = db
			.run(
				'match (venue:Venue {id: $venueId}) ' +
				'match (venue)-[hasCalendar:HasCalendar]->(calendar:Calendar) ' +
				'optional match (venue)-[:HasTerm]->(terms:Term) ' +                            // All terms
					'where not exists(terms.deletedAt) ' +                                      // and terms not deleted
				'optional match (calendar)-[hasClasses:HasClass]->(classes:Class) ' +           // All classes
				(levelId ? '-[:TeachesLevel]->(:Level {id: $levelId}) ' : '') +                 // or only classes at this level
					'where not exists(classes.deletedAt) ' +                                    // and classes not deleted
				(futureOnly ? 'and classes.datetime >= $now ' : '') +                           // or only future classes
				'optional match (classes)-[:TeachesLevel]->(levels:Level) ' +                   // Get the levels for the classes
					'where not exists(levels.deletedAt) ' +                                     // and levels not deleted
				'optional match (classes)<-[:Instructs]-(instructors:Person) ' +                // The instructors
					'where not exists (instructors.deletedAt) ' +                               // and instructors not deleted
				'optional match (classes)<-[:SwimsIn]-(swimmers:Person) ' +                     // The swimmers
					'where not exists(swimmers.deletedAt) ' +                                   // and swimmers not deleted
				'optional match (swimmers)-[swimsInRepeats:SwimsIn]->(classes) ' +
					'where exists(classes.repeatId) ' +
				'optional match (classes)-[:HasWaitingList]->(:WaitingList)<-[onList:OnWaitingList]-(waiters:Person) ' +    // The waiters
					'where not exists(waiters.deletedAt) ' +                                                                // and waiters not deleted
				'optional match (classes)-[:InTerm]->(classTerms:Term) ' +                                                  // The terms for each class
				'return  terms, calendar, hasClasses, levels, instructors, swimmers, waiters, onList, classTerms, ' +
						'count(swimsInRepeats) as countInRepeats, ' +
						'classes order by classes.datetime asc',
				{
					venueId: venue.id,
					levelId: levelId,
					now: moment().subtract(1, 'day').format(),
				})
			.then(result => {
				sails.log.info('AfterClassesFetch1:', venue.id, levelId, result);

				let calendar = dbService.formatResult(result, 'calendar');
				let classes = dbService.formatResult(result, 'classes');
				let classTerms = dbService.formatResult(result, 'classTerms');
				let countInRepeats = dbService.formatResult(result, 'countInRepeats');
				let hasClasses = dbService.formatResult(result, 'hasClasses');
				let instructors = dbService.formatResult(result, 'instructors');
				let levels = dbService.formatResult(result, 'levels');
				let onList = dbService.formatResult(result, 'onList');
				let swimmers = dbService.formatResult(result, 'swimmers');
				let terms = dbService.formatResult(result, 'terms');
				let waiters = dbService.formatResult(result, 'waiters');

				if (!calendar || calendar.length == 0) {
					return 'No calendar has been found for this location';
				}

				// Nullify passwords
				instructors.forEach(instructor => { if (instructor.password) instructor.password = null });
				swimmers.forEach(swimmer => { if (swimmer.password) swimmer.password = null });

				// Add the swimmers and waiters to the class
				let uniqueClasses = [];
				let uniqueRepeatedClasses = {};
				classes.forEach((oneClass, index) => {
					if (helperService.isEmptyObject(oneClass)) return;

					let cIndex = uniqueClasses.findIndex(c => c.id === oneClass.id);
					if (cIndex < 0) {
						// Create the first instance of this class in the collated list, and create an empty waiters, terms, etc list
						oneClass.level = levels[index];
						oneClass.instructors = [];
						oneClass.swimmers = [];
						oneClass.terms = [];
						oneClass.waiters = [];
						uniqueClasses.push(oneClass);
						cIndex = uniqueClasses.length - 1;
					}

					uniqueClasses[cIndex].level = levels[index];
					uniqueClasses[cIndex].cap = uniqueClasses[cIndex].cap || uniqueClasses[cIndex].level.cap;
					uniqueClasses[cIndex].duration = uniqueClasses[cIndex].duration || levels[index].duration;

					let term = classTerms[index];
					if (!helperService.isEmptyObject(term)) {
						if (_.findIndex(uniqueClasses[cIndex].terms, {id: term.id}) < 0)
							uniqueClasses[cIndex].terms.push(term)

							// Presume we are the first term
							uniqueClasses[cIndex].term = uniqueClasses[cIndex].terms[0];
					}

					let instructor = instructors[index];
					if (!helperService.isEmptyObject(instructor)) {
						if (_.findIndex(uniqueClasses[cIndex].instructors, {id: instructor.id}) < 0)
							uniqueClasses[cIndex].instructors.push(instructor)
					}

					let swimmer = swimmers[index];
					if (!helperService.isEmptyObject(swimmer)) {
						if (_.findIndex(uniqueClasses[cIndex].swimmers, {id: swimmer.id}) < 0)
							uniqueClasses[cIndex].swimmers.push(swimmer)
					}

					let waiter = waiters[index];
					if (!helperService.isEmptyObject(waiter)) {
						if (_.findIndex(uniqueClasses[cIndex].waiters, {id: waiter.id}) < 0) {
							waiter.joinDate = onList[index].joinDate;
							uniqueClasses[cIndex].waiters.push(waiter);
						}
					}

					// While going thru the classes, find the number of classes a swimmer attends within a repeated series
					// Create an object of repeatIds => object of swimmerIds => maps to an array of classIds
					if (countInRepeats[index] && oneClass.repeatId) {
						let repeatId = oneClass.repeatId;
						let swimmerId = swimmers[index].id;
						let classId = oneClass.id;

						if (!uniqueRepeatedClasses[repeatId]) uniqueRepeatedClasses[repeatId] = {};
						if (!uniqueRepeatedClasses[repeatId][swimmerId]) uniqueRepeatedClasses[repeatId][swimmerId] = [];
						if (uniqueRepeatedClasses[repeatId][swimmerId].indexOf(classId) < 0) uniqueRepeatedClasses[repeatId][swimmerId].push(classId);
					}
				});

				// Add some flags for filling
				uniqueClasses.forEach(oneClass => {
					oneClass.countBooked = oneClass.swimmers.length;
					oneClass.isFull = oneClass.countBooked >= oneClass.cap ? true : false;

					if (oneClass.repeatId && uniqueRepeatedClasses[oneClass.repeatId]) {
						oneClass.repeatedClassesPerSwimmer = uniqueRepeatedClasses[oneClass.repeatId];
					}
				});

				// Sort the swimmers on firstName
				uniqueClasses.forEach(oneClass => {
					oneClass.swimmers.sort((a, b) => {
						if (!a.firstName || !b.firstName) return 0;
						return a.firstName.localeCompare(b.firstName)
					});
				});

				// Sort the waiters on joinDate
				uniqueClasses.forEach(oneClass => {
					oneClass.waiters.sort((a, b) => {
						if (!a.joinDate || !b.joinDate) return 0;
						return a.joinDate.localeCompare(b.joinDate)
					});
				});

				// Return only the unique terms (multiples maybe present)
				var uniqueTerms = helperService.uniquifyArray(terms);

				sails.log.info('AfterCalendarFetch2:', uniqueClasses); // , levels, instructors, swimmers);
				return {
					terms: uniqueTerms,
					calendar: calendar[0],
					classes: uniqueClasses
				};
			})
			.catch(err => {
				sails.log.warn('Error while getting calendar for this location', err);
				return 'Error while getting calendar for this location: ' + err;
			});

		return classPromise;
	},

	/*
	 * Create one class for the selected calendar of the venue with all relevant relationships
	 * Return the created class basic info (so not term, level, waiting list etc)
	 */
	create(token, calendar = null, newClass = null) {
		sails.log.info('CREATING CLASS', newClass.datetime);

		if (!token) return;
		if (!calendar) return;
		if (!newClass) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		// Check if the datetime of this class is in the future (past classes may not be created)
		if (moment(newClass.datetime).isBefore()) {
			return Promise.reject('Classes cannot be created in the past');
		}

		// Generate an unique id, the name (useful only for Neo4J)
		let id = shortid.generate();
		let name = id;

		// Create the node
		let classPromise = db
			.run(
				'merge (class:Class {' +
				' id      : $classId ' +
				',name    : $name ' +
				',laneId  : $laneId ' +
				',datetime: $datetime ' +
				(newClass.cap      ? ',cap      : $cap ' : '') +
				(newClass.duration ? ',duration : $duration ' : '') +
				(newClass.note     ? ',note     : $note ' : '') +
				(newClass.nrLanes  ? ',nrLanes  : $nrLanes ' : '') +
				(newClass.repeatId ? ',repeatId : $repeatId ' : '') +
				(newClass.repeatShortDescription ? ',repeatShortDescription : $repeatShortDescription ' : '') +
				'}) return class', {
					classId: id,
					name: name,
					laneId: newClass.laneId.toString(),
					datetime: newClass.datetime,
					duration: newClass.duration,
					cap: newClass.cap,
					note: newClass.note,
					nrLanes: newClass.nrLanes,
					repeatId: newClass.repeatId,
					repeatShortDescription: newClass.repeatShortDescription,
				}
			)
			.then(function (result) {

				sails.log.info('Suc create class', id, result);

				let createdClass = dbService.formatResult(result, 'class')[0];

				// Create the relationships: calendar + term + staff + level
				let classPromise = db
					.run(
						'match (venue:Venue {id: $venueId})-->(calendar:Calendar {id: $calendarId}) ' +
						'match (class:Class {id: $classId}) ' +
						'optional match (venue)-->(terms:Term) where terms.startDate <= class.datetime and class.datetime <= terms.endDate ' +

						// Relationship to calendar
						'merge (calendar)-[:HasClass]->(class) ' +

						// Relationship to staff
						(newClass.instructorId
							?   'with class, calendar, terms ' +
							'match (instructor:Person {id: $instructorId}) where not exists(instructor.deletedAt) ' +
							'merge (instructor)-[:Instructs]->(class) '
							:   '') +

						// Relationship to level
						(newClass.levelId
							?   'with class, calendar, terms ' +
							'match (level:Level {id: $levelId}) ' +
							'merge (class)-[:TeachesLevel]->(level) '
							:   '') +

						// Relationship to term
						'with class, calendar, terms limit 1 ' +
						'where terms is not null merge (class)-[:InTerm]->(terms) '     // The 'where is not null' clause must be last

						, {
							venueId: venue.id,
							calendarId: calendar.id,
							classId: createdClass.id,
							instructorId: newClass.instructorId,
							levelId: newClass.levelId,
						}

						// Return list of all classes -> for now: do this clientside after return
						// 'with calendar ' +
						// 'match (calendar)-[:HasClass]->(classes:Class) ' +
						// 'return classes'
					)
					.then(function (result) {
						sails.log.info('Suc create HasClass', result, createdClass)
						return {
							class: createdClass,
						};
					})
					.catch(function (err) {
						sails.log.warn('Err create HasClass:', err)
						return 'Err create HasClass: ' + err;
					});

				return classPromise;
			})
			.catch(function (err) {
				sails.log.warn('Err create class:', err)
				return 'Err create class: ' + err;
			});

		return classPromise;
	},

	/*
	 * Update a class
	 * Return the updated class extended info: include term and level (but not swimmers, waiting list, etc)
	 * The extended info is used in case waiters are added and payments are executed
	 */
	async update(token, oneClass = null) {
		sails.log.info('UPDATING CLASS', oneClass.datetime);

		// TODO cypher queries using params

		if (!token) return;
		if (!oneClass) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		// Check if the datetime of this class is in the future (past classes may not be updated)
		if (moment(oneClass.datetime).isBefore()) {
			return Promise.reject(`The past and past classes cannot be updated`);
		}

		// Check on a valid minimum cap and if a new cap allows room for waiters to join the class
		// TODO should this check go to validate ??
		let nrWaitersToJoin = 0;
		let newCap = parseInt(oneClass.cap) || parseInt(oneClass.level && oneClass.level.cap) || null;
		if (newCap && !isNaN(newCap)) {
			if (newCap < oneClass.swimmers.length) {
				return Promise.reject(`Cap may not be less than number of enrolled swimmers (now: ${oneClass.swimmers.length})`);
			} else if (newCap > oneClass.swimmers.length) {
				nrWaitersToJoin = newCap - oneClass.swimmers.length;
			}
		}

		// Build the query to update the class
		let cypher;
		cypher =
			'match (:Venue {id: $venueId})-->(:Calendar)-->(class:Class {id: $classId}) ';

		// If datetime, lane, cap, duration, note: update those
		if (oneClass.datetime && oneClass.laneId >= 0) {
			cypher += 'set class.datetime = "' + oneClass.datetime + '",class.laneId="' + oneClass.laneId + '" ';
		}
		cypher += 'set class.cap = "' + (oneClass.cap || '') + '" ';
		cypher += 'set class.duration = "' + (oneClass.duration || '') + '" ';
		cypher += 'set class.note = "' + (oneClass.note || '') + '" ';
		cypher += 'with class ';

		// Get term information too
		cypher += 'optional match (class)-[:InTerm]->(term:Term) ' +
				  'with class, term ';

		// Instructors: remove all existing instructors and add all instructors in the data
		cypher +=
			'optional match (class)<-[instructs:Instructs]-(:Person) ' +
			'delete instructs ' +
			'with class, term ';
		if (oneClass.instructors && oneClass.instructors.length) {
			oneClass.instructors.forEach(instructor => {
				cypher +=
					'match (instructor:Person {id: "' + instructor.id + '"}) ' +
					'merge (class)<-[:Instructs]-(instructor) ' +
					'with class, term ';
			});
		}

		// if levelId: remake the relationship to level
		if (!oneClass.levelId && oneClass.level && oneClass.level.id) oneClass.levelId = oneClass.level.id; // convenience property
		if (oneClass.levelId) {
			cypher +=
				'optional match (class)-[teachesLevel:TeachesLevel]->(level:Level) where not exists(level.deletedAt) ' +
				'delete teachesLevel ' +
				'with class, term ' +
				'match (level:Level {id: $levelId}) ' +
				'merge (class)-[:TeachesLevel]->(level) ' +
				'with class, term, level ';
		}

		// Update repeat information
		cypher += oneClass.repeatId ? 'set class.repeatId = $repeatId ' : '' ;
		cypher += oneClass.isRepeating ? 'set class.isRepeating = true ' : '' ;
		cypher += oneClass.repeatStarts ? 'set class.repeatStarts = $repeatStarts ' : '';
		cypher += oneClass.repeatEnds ? 'set class.repeatEnds = $repeatEnds ' : '';
		cypher += oneClass.bookSeries ? 'set class.bookSeries = $bookSeries ' : '';
		cypher += oneClass.repeatShortDescription ? 'set class.repeatShortDescription = $repeatShortDescription ' : '';

		if (oneClass.repeatDays) {
			cypher += 'set class.repeatDays = $repeatDays ';
			cypher += 'set class.exampleDescription = $exampleDescription ';
		}

		cypher += 'return class, term ';
		if (oneClass.levelId) {
			cypher += ', level';
		}

		let exampleDescription = null;
		if (oneClass.repeatDays) {
			exampleDescription = 'Classes every ' + (oneClass.bookSeries === 'weekly'
				? _.capitalize(oneClass.repeatDays[0])
				: _.startCase(oneClass.repeatDays.join(', ')));
		}

		classPromise = db
			.run(cypher, {
				venueId: venue.id,
				classId: oneClass.id,
				levelId: oneClass.levelId,
				repeatId: oneClass.repeatId,
				isRepeating: oneClass.isRepeating,
				repeatDays: oneClass.repeatDays,
				repeatStarts: oneClass.repeatStarts,
				repeatEnds: oneClass.repeatEnds,
				bookSeries: oneClass.bookSeries,
				exampleDescription: exampleDescription,
				repeatShortDescription: oneClass.repeatShortDescription,
			})
			.then(result => {
				sails.log.info('AfterClassUpdate:', result);
				let updatedClass = dbService.formatResult(result, 'class')[0];
				let level = oneClass.levelId ? dbService.formatResult(result, 'level')[0] : {};
				let terms = oneClass.levelId ? dbService.formatResult(result, 'term') : [];

				updatedClass.level = level;
				updatedClass.terms = terms;

				sails.log.info('AfterClassUpdate2:', updatedClass);

				// If there was an increased cap => add the relevant number of waiters
				let waitersPromise = this.promoteWaiters(token, updatedClass, nrWaitersToJoin)
					.then(response => {

						// Return the updated class
						return {
							class: updatedClass,
						};
					})
					.catch(response => {
						return response;
					});

				return waitersPromise;
			})
			.catch(response => {
				sails.log.warn('Error while finding class to update ', response);
				return 'Error while finding class to update ' + response;
			});

		return classPromise;
	},

	/*
	 * Generate the repeated classes
	 */
	async generate(token, calendar = null, oneClass = null) {
		sails.log.info('GENERATING REPEATS', oneClass.term);

		if (!token) return;
		if (!calendar) return;
		if (!oneClass) return;

		// Check if the datetime of this class is in the future
		if (moment(oneClass.datetime).isBefore()) {
			return Promise.reject(`Classes cannot be generated from a class in the past`);
		}

		// Check if persons enrolled (ie same rules as not deletable)
		if (await this.isNotDeletable(token, oneClass)) {
			return Promise.reject(`Classes cannot be generated from a class with enrolled swimmers`);
		}

		// Sanitise start and end dates
		if ((oneClass.repeatStarts === 'sot' || oneClass.repeatEnds === 'eot') && helperService.isEmptyObject(oneClass.term)) {
			return Promise.reject(`This date is not in a term`);
		}

		let startDate = moment(oneClass.repeatStarts === 'sot' ? oneClass.term.startDate : oneClass.repeatStarts);
		if (!startDate.isValid()) return Promise.reject(`Start date to generate classes isn't valid`);

		let maxDate = moment('2099-12-31');
		if (!helperService.isEmptyObject(oneClass.term)) {
			maxDate = moment(oneClass.term.endDate).add(1, 'day');
			if (!maxDate.isValid()) return Promise.reject(`End date to generate classes isn't valid`);
		}

		let maxClasses = 99;
		if (oneClass.repeatEnds !== 'eot') {
			maxClasses = parseInt(oneClass.repeatEnds) || 1;
		}

		// Sort the repeatDays in same order as website is showing (Monday first)
		// This makes that classes get added chronologically - so easier to understand in case of a failure
		oneClass.repeatDays.sort((a,b) => {
			aIndex = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su'].indexOf(a);
			bIndex = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su'].indexOf(b);
			return aIndex - bIndex;
		});

		// Create a list of dates on which classes will be created
		let startDow;
		let nextDate;
		let dow;
		let allSeries = [];
		let seriesCount = 0;
		let classesCount = 0;
		let iterations = 999;            // prevent infinite loop
		let stopGeneration = false;

		while (!stopGeneration && iterations-- > 0) {

			startDow = startDate.day();

			// Create a series of dates starting at the days-of-week on of after the repeatStarts date
			oneClass.repeatDays.forEach(dowHuman => {

				// Find date of the first day-of-week on or after the startDate
				dow = ['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'].indexOf(dowHuman);
				nextDate = startDate.clone();
				nextDate.add((dow - startDow + 7) % 7, 'days');

				// Check here if max number of classes reached OR eot
				if (nextDate.isSameOrBefore(maxDate) && classesCount < maxClasses) {
					if (!Array.isArray(allSeries[seriesCount])) allSeries.push([]);
					allSeries[seriesCount].push(nextDate)

				} else {
					stopGeneration = true;
				}

				// Weekly classes have their own series. 'Days' is just one series of classes to be booked
				if (oneClass.bookSeries === 'weekly') {
					seriesCount++;
				} else {
					classesCount++;
				}
			});

			// Bump the classes count after all days for this week have been done
			if (oneClass.bookSeries === 'weekly') classesCount++;

			// Add the dates for the subsequent weeks
			startDate.add(1, 'week');
			seriesCount = 0;
		}
		sails.log.info('ALLSERIES', allSeries, maxDate, maxClasses);

		// Sanitise the dates i.e. nothing in the past. Find the latest date
		let anythingInThePast = false;
		let latestDate = allSeries[0][0];
		allSeries.forEach(series => {
			series.forEach(classDate => {
				if (moment(classDate).isBefore()) anythingInThePast = true;
				latestDate = classDate > latestDate ? classDate : latestDate;
			});
		});
		if (anythingInThePast) return Promise.reject(`Classes cannot be generated for dates in the past. No classes have been generated`);

		// Per date (in each series) generate the class as a copy of the original
		let originalClass = _.clone(oneClass);
		let seriesPromisesArray = [];
		allSeries.forEach(series => {

			// The repeatId is the same per series. I.e it allows repeating classes to be booked at once
			let repeatId = shortid.generate();

			series.forEach(classDate => {

				oneClass = _.clone(originalClass);

				// Create a short description to show when booking
				oneClass.repeatShortDescription = 'Every ' +
					(oneClass.bookSeries === 'weekly'
						? _.capitalize(moment(classDate).format('ddd'))
						: _.startCase(oneClass.repeatDays.join(', '))) +
					' until ' + moment(series[series.length - 1]).format('Do MMM');

				// Set a repeatId
				oneClass.repeatId = repeatId;

				// Ensure the time is the original class time (because sot starts at midnight)
				classDate.hour(moment(oneClass.datetime).hour());
				classDate.minute(moment(oneClass.datetime).minute());
				classDate.second(moment(oneClass.datetime).second());

				if (classDate.isSame(oneClass.datetime, 'minute')) {

					// The dateTimes match, i.e. we're recreating the original. So, only update with the repeatId
					seriesPromisesArray.push( this.update(token, oneClass) );

				} else {

					// Generate a repeated class. Add ids that the create method expect's
					oneClass.datetime = classDate.format();
					oneClass.levelId = oneClass.level.id;
					oneClass.termId = oneClass.term && oneClass.term.id;
					oneClass.instructorId = oneClass.instructors.length && oneClass.instructors[0].id;

					seriesPromisesArray.push( this.create(token, calendar, oneClass) );
				}
			});
		});

		return Promise
			.all(seriesPromisesArray)
			.then(responseArray => {
				sails.log.info('PROMISEALL SUCCESS', responseArray);
				return `Generated ${responseArray.length} classes up to ${moment(latestDate).format('Do MMM')}`;
			})
			.catch(response => {
				sails.log.info('PROMISEALL ERROR', response + ' (Some classes may have been generated)');
				return Promise.reject(response + ' (Some classes may have been generated)');
			});
	},

	/*
	 * Delete a class
	 */
	async delete(token, calendar = null, oneClass = null) {
		sails.log.info('DELETING RELATIONSHIP FOR', calendar, oneClass);

		if (!token) return;
		if (!calendar) return;
		if (!oneClass) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		let isNotDeletable = await this.isNotDeletable(token, oneClass);
		if (isNotDeletable) {
			return Promise.reject(isNotDeletable);
		}

		cypher =
			'match (calendar:Calendar {id: $calendarId})-[:HasClass]->(class:Class {id: $classId}) ' +
			'set class.deletedAt = $deletedAt ' +
			'set class.deletedWhat = $deletedWhat ' +
			'with class ' +
			'merge (venue:Venue {id: $venueId})-[:HasDeletedList]->(deletedList:DeletedList) ' +
			'merge (deletedList)-[:IsDeleted]->(class) ';

		let classPromise = db
			.run(cypher, {
				calendarId: calendar.id,
				classId: oneClass.id,
				venueId: venue.id,
				deletedAt: moment().format(),
				deletedWhat: 'Class ' + oneClass.level.name + ' in lane ' + oneClass.laneId + ' at ' + moment(oneClass.datetime).format(sails.config.globals.momentDatetimeFormat),
			})
			.then(function () {
				sails.log.info('Suc after delete class: ');
				return true
			})
			.catch(function (err) {
				sails.log.warn('Err after delete class: ', err);
				return 'Err after delete class: ' + err;
			});

		return classPromise;
	},

	/*
     * Disallow delete of a class
	 * Return false (= delete is allowed) or a string with the reason
	 * rules:
	 *      only possible if no enrolled swimmers
	 *
     * These rules also apply to allow generation of repeating classes
	 */
	async isNotDeletable(token, oneClass) {

		sails.log.info('ISNOTDELETABLECLASS:', oneClass);

		if (!token) return;
		if (!oneClass) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		// Check if the datetime of this class is in the future (past classes may not be deleted)
		if (moment(oneClass.datetime).isBefore()) {
			return 'Past classes cannot be removed';
		}

		let cypher = '' +
			'match (venue:Venue {id: $venueId})-[:HasCalendar]->(:Calendar)-[:HasClass]->(class:Class {id: $classId}) ' +
			'optional match (swimmers:Person)-[:SwimsIn]->(class) ' +
			'return count(swimmers) as nrSwimmers ';

		let classPromise = db
			.run(cypher, {
				classId: oneClass.id,
				venueId: venue.id,
			})
			.then(response => {
				let nrSwimmers = dbService.formatResult(response, 'nrSwimmers')[0];
				sails.log.info('Found numbers swimmers in class:', nrSwimmers);

				if (nrSwimmers === 0) return false;

				return 'Still ' + nrSwimmers + ' swimmers enrolled in this class';
			})
			.catch(response => {
				sails.log.info('Error while determining class deletability ', response);
				return 'Error while determining class deletability ' + response;
			});

		return classPromise;
	},

	/*
	 * Remove a swimmer from a class and promote the first waiter into the class
	 */
	removeSwimmer(token, oneClass = null, swimmer = null) {
		sails.log.info('CLASSREMOVESWIMMER:', oneClass);

		if (!token) return;
		if (!oneClass) return;
		if (!swimmer) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		// Remove the swimmer from the class
		let cypher =
			'match (class:Class {id: $classId}) ' +
			'optional match (person:Person {id: $swimmerId})-[swimsIn:SwimsIn]->(class) ' +
			'delete swimsIn ';

		let classPromise = db
			.run(cypher, {
				classId: oneClass.id,
				swimmerId: swimmer.id,
			})
			.then(result => {
				sails.log.info('AfterRemoveSwimmer:', result);

				// Send cancel message to admins
				Message.send(token,
					{
						title: swimmer.firstName + ' cancelled for ' + oneClass.level.name,
						content: swimmer.firstName + ' ' + swimmer.lastName +
						' has cancelled for ' + oneClass.level.name +
						' on ' + moment(oneClass.datetime).format(sails.config.globals.momentDatetimeFormat),
					}, {
						to: 'admins',
					});

				// Move the next waiter to the class (and don't wait for the result)
				this.promoteWaiters(token, oneClass, 1)
					.then(response => sails.log.info('promoteWaiters success:', response))
					.catch(response => sails.log.warn('promoteWaiters error:', response));

				return true;
			})
			.catch(err => {
				sails.log.warn('Error while removing swimmer from class ', err);
				return 'Error while removing swimmer from class ' + err;
			});

		return classPromise;
	},

	/*
	 * Book (=add) a person into a class, remove them from the waiting list (if necessary) and message the admins
	 */
	book(token, person, oneClass, swimmer) {
		sails.log.info('BOOK A CLASS:', oneClass, swimmer);

		if (!token) return;
		if (!person) return;
		if (!oneClass) return;
		if (!swimmer) return;

		// Book 1 class or the term
		if (oneClass.oneOrTerm !== 1 && oneClass.oneOrTerm !== 'term')
			return Promise.reject('Please book either 1 class or for the term');

		// Add the swimmer to relevant classes and remove them from waiting list
		let cypher;
		if (oneClass.oneOrTerm === 'term' && oneClass.repeatId) {
			// Add the swimmer to all the repeating classes
			cypher =
				'match (class:Class {id: $classId }) ' +
				'match (repeatClasses:Class {repeatId: $repeatId }) ' +
				'where repeatClasses.datetime >= class.datetime ' +
				'with class, repeatClasses limit $nrClasses ' +
				'match (repeatClasses)-[:TeachesLevel]->(level:Level) where not exists(level.deletedAt) ' +
				'match (swimmer:Person {id: $swimmerId}) ' +
				'merge (swimmer)-[:SwimsIn]->(repeatClasses) ' +
				'with class, repeatClasses, level, swimmer ' +
				'optional match (swimmer)-[onList:OnWaitingList]->(:WaitingList)<-[:HasWaitingList]-(repeatClasses) ' +
				'delete onList ' +
				'with level, class limit 1 ' +
				'optional match (class)<-[swimsIn:SwimsIn]-(swimmers:Person) where not exists(swimmers.deletedAt) ' +
				'return count(swimsIn) as countBooked, level';

		} else {
			// Add the swimmer to the single class
			cypher =
				'match (class:Class {id: $classId}) ' +
				'match (class)-[:TeachesLevel]->(level:Level) where not exists(level.deletedAt) ' +
				'match (swimmer:Person {id: $swimmerId})' +
				'merge (swimmer)-[:SwimsIn]->(class) ' +
				'with class, level, swimmer ' +
				'optional match (swimmer)-[onList:OnWaitingList]->(:WaitingList)<-[:HasWaitingList]-(class) ' +
				'delete onList ' +
				'with level, class limit 1 ' +
				'optional match (class)<-[swimsIn:SwimsIn]-(swimmers:Person) where not exists(swimmers.deletedAt) ' +
				'return count(swimsIn) as countBooked, level';
		}

		let cypherParams = {
			classId: oneClass.id,
			repeatId: oneClass.repeatId,
			nrClasses: oneClass.remainingLessons,
			swimmerId: swimmer.id
		};

		let classPromise = db
			.run(cypher, cypherParams)
			.then(result => {
				sails.log.info('AfterBook:', result, cypherParams);

				let countBooked = dbService.formatResult(result, 'countBooked')[0];
				let level = dbService.formatResult(result, 'level')[0];

				// Send a message to admins that a booking is made
				let message = {
					title: (oneClass.oneOrTerm === 'term' ? 'Term classes' : 'Class') + ' booked for ' + swimmer.firstName + ' ' + swimmer.lastName,
					content: swimmer.firstName + ' is booked ' +
					(oneClass.oneOrTerm === 'term' ? 'for a term' : '') +
					' into ' + level.name +
					(oneClass.oneOrTerm === 'term' ? ' starting ' : ' on ') +
					moment(oneClass.datetime).format(sails.config.globals.momentDatetimeFormat) +
					' by ' + person.firstName + ' ' + person.lastName +
					'. That class has now a total of ' + countBooked + ' swimmers',
				};
				Message
					.send(token, message, {to: 'admins'})
					.then(messages => sails.log.info('MESSAGESPROMISE-RETURNED', messages))
					.catch(err => sails.log.info('MESSAGESPROMISE-ERROR', err));

				// Send a message to admins if the cap is reached
				if (parseInt(countBooked) >= parseInt(oneClass.cap)) {
					let message = {
						title: 'Class cap reached',
						content: 'Class ' + level.name + ' on ' +
						moment(oneClass.datetime).format(sails.config.globals.momentDatetimeFormat) +
						' has reached it\'s cap of ' + oneClass.cap + ' swimmers' +
						'. There is now a total of ' + countBooked + ' swimmers',
					};
					Message
						.send(token, message, {to: 'admins'})
						.then(messages => sails.log.info('MESSAGESPROMISE-RETURNED', messages))
						.catch(err => sails.log.info('MESSAGESPROMISE-ERROR', err));
				}

				return true;
			})
			.catch(err => {
				sails.log.info('Error while booking person into class ', err);
				return err;
			});

		return classPromise;
	},

	/*
	 * Join the waiting list for a class
	 * Return the queue length
	 */
	joinWaitingList(token, venue, person, oneClass, swimmer, paymentMethodId) {

		let cypher =
			// Create the waiting list
			'match (class:Class {id: $classId}) ' +
			'merge (class)-[:HasWaitingList]->(waitingList:WaitingList) ' +
			'with waitingList ' +
			// Add the person to the list
			'match (person:Person {id: $swimmerId}) ' +
			'merge (person)-[onList:OnWaitingList]->(waitingList) ' +
			'set onList.joinDate = $joinDate ' +
			'set onList.paymentMethodId = $paymentMethodId ' +
			'with waitingList ' +
			// Count the number on the list
			'optional match (waiters:Person)-[waiting:OnWaitingList]->(waitingList) where not exists(waiters.deletedAt) ' +
			'return count(waiting) as countWaiting';

		let classPromise = db
			.run(cypher, {
				classId: oneClass.id,
				swimmerId: swimmer.id,
				joinDate: moment().format(),
				paymentMethodId: paymentMethodId,
			})
			.then(result => {
				sails.log.info('AfterWaitingListUpdate:', result);
				let queueLength = dbService.formatResult(result, 'countWaiting')[0];

				// Send a message to the admins
				Message.send(token,
					{
						title: swimmer.firstName + ' on waiting list for ' + oneClass.level.name,
						content: swimmer.firstName + ' ' + swimmer.lastName +
						' has been placed on the waiting list for ' + oneClass.level.name +
						' on ' + moment(oneClass.datetime).format(sails.config.globals.momentDatetimeFormat) + '. ' +
						' There are now ' + queueLength + ' swimmers waiting',
					}, {
						to: 'admins',
					});

				return {queueLength: queueLength};
			})
			.catch(err => {
				sails.log.info('Error while joining waitingList ', err);
				return err;
			});

		return classPromise;
	},

	/*
	 * Leave the waiting list for a class
	 * Return the queue length
	 */
	leaveWaitingList(token, venue, person, oneClass, swimmer) {

		let cypher =
			// Find the swimmer on the list and remove
			'match (person:Person {id: $swimmerId}) ' +
			'match (class:Class {id: $classId})-[:HasWaitingList]->(waitingList:WaitingList) ' +
			'match (person)-[onList:OnWaitingList]->(waitingList) ' +
			'delete onList ' +
			'with waitingList ' +
			// Count the number on the list
			'optional match (waiters:Person)-[waiting:OnWaitingList]->(waitingList) where not exists(waiters.deletedAt) ' +
			'return count(waiting) as countWaiting';

		let classPromise = db
			.run(cypher, {
				classId: oneClass.id,
				swimmerId: swimmer.id,
			})
			.then(result => {
				sails.log.info('AfterWaitingListUpdate:', result);
				let queueLength = dbService.formatResult(result, 'countWaiting')[0];

				return {queueLength: queueLength};
			})
			.catch(err => {
				sails.log.warn('Error while leaving waitingList ', err);
				return err;
			});

		return classPromise;
	},

	/*
	 * Promote a number of waiters to a class
	 * Perform their payments: if unsuccessful, get the next waiter
     */
	async promoteWaiters(token, oneClass = null, nrWaiters = 0) {
		sails.log.info('promoteWaiters:', oneClass);

		if (!token) return;
		if (!oneClass) return;
		if (!nrWaiters) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		// Find all the waiters for the class
		let cypher =
			'match (class:Class {id: $classId}) ' +
			'optional match (class)-[:HasWaitingList]->(waitingList:WaitingList) ' +
			'optional match (waitingList)<-[onList:OnWaitingList]-(waiters:Person) where not exists(waiters.deletedAt) ' +
			'with waiters, onList order by onList.joinDate asc ' +
			'return waiters, onList ';

		let classPromise = db
			.run(cypher, {
				classId: oneClass.id,
			})
			.then(async result => {
				sails.log.info('AfterFindWaiters:', result);

				let waiters = dbService.formatResult(result, 'waiters');
				let onList = dbService.formatResult(result, 'onList');
				sails.log.info('Found waiters:', waiters, onList);

				// Move the correct number of waiters to the class
				// Defined by a successful payment
				let promoted;
				let index = 0;
				for (let waiter of waiters) {

					sails.log.info('waiterPaymentAndMovePROMOTION', waiter);

					try {
						promoted = await this.waiterPaymentAndMove(token, oneClass, waiter, onList[index++], venue);
						sails.log.info('waiterPaymentAndMovePROMOTED', promoted);
						if (_.isString(promoted)) return Promise.reject(promoted);  // There was an error
						if (promoted === true && --nrWaiters <= 0) break;           // Correct nr of waiters have been promoted

					} catch(err) {
						return err;                                                 // There was an error
					}
				}
				return true;
			})
			.catch(err => {
				sails.log.warn('Error while promoting waiter to class', err);
				return 'Error while promoting waiter to class ' + err;
			});

		return classPromise;
	},

	/*
     * Perform payment for the waiter, ie actual execution and db update
     * returns:
     *      true = waiter paid and moved to class
     *      false = payment failed
     *      string = general error message
	 */
	async waiterPaymentAndMove(token, oneClass, waiter, onList, venue) {

		if (helperService.isEmptyObject(waiter)) return;

		sails.log.info('waiterPaymentAndMove:', waiter, onList);

		let payment = {
			paymentMethodId: onList.paymentMethodId,
		};

		// Execute the authorised payment. The paymentMethod is linked to the payer. This will also check for credits
		// Pay for the specified class and waiter
		let paymentResult = Payment
			.execute(token, payment, oneClass, waiter)
			.then(response => {
				// A success payment returns an object with an id
				// A failed payment returns false
				// Other errors return false or a string
				sails.log.info('AFTERPAYMENTEXECUTE:', response);

				if (response === false) {

					// Payment has failed => message admins and carers
					// Send payment-failed message to admins
					Message.send(token,
						{
							title: 'Payment failed for ' + waiter.firstName + ' waiting on level ' + oneClass.level.name,
							content: waiter.firstName + ' ' + waiter.lastName +
							' hasn\'t moved from waiting list to the class ' + oneClass.level.name +
							' because the payment failed ' +
							' at ' + moment().format(sails.config.globals.momentDatetimeFormat),
						}, {
							to: 'admins',
						});

					// Email the carer(s) that payment for the waiter has failed
					let carers = dbService.formatResult(result, 'carers');
					carers.forEach(carer => {
						let subject = 'Payment for ' + waiter.firstName + ' to move into a class has failed';
						let br = '<br>';
						let emailContent = '';
						emailContent += 'Hello ' + carer.firstName + ',';
						emailContent += br;
						emailContent += br + 'We couldn\'t execute a payment for ' + waiter.firstName + ' to move to a class at level ' + oneClass.level.name;
						emailContent += br;
						emailContent += br +  waiter.firstName + ' will remain on the waiting list until space becomes available again.'
						emailContent += br;
						emailContent += br + 'Please contact us if you feel this to be in error.'
						emailContent += br;
						emailContent += br + 'Kind regards,';
						emailContent += br + venue.name;
						emailContent += br;
						emailContent += br + 'Please note: you can\'t reply to this email address';
						Message.sendEmail({
							token: token,
							from: 'noReply',
							to: carer.email,
							subject: subject,
							content: emailContent,
							isHtml: true,
						});
					});

					return false;

				} else if (_.isObject(response) && response.id && response.receiptId) {

					// Payment was successful => move the waiter from waiting list to the class and send messages
					let cypher =
						'match (waiter:Person {id: $waiterId}) ' +
						'match (class:Class {id: $classId}) ' +
						'match (waiter)-[onList:OnWaitingList]->(:WaitingList)<-[:HasWaitingList]-(class) ' +
						'delete onList ' +
						'merge (waiter)-[:SwimsIn]->(class) ' +
						'with waiter ' +
						'optional match (waiter)<-[:HasPerson]-(:Group)-[:HasPerson]->(carers:Person {isCarer: true}) ' +
						'where not exists(carers.deletedAt) ' +
						'return waiter, carers';

					let waiterPromise = db
						.run(cypher, {
							waiterId: waiter.id,
							classId: oneClass.id,
						})
						.then(result => {
							sails.log.info('AfterUpdateWaiter:', waiter.id, oneClass.id, result);

							// Send not-waiting-anymore message to admins
							Message.send(token,
								{
									title: waiter.firstName + ' moved from waiting list to ' + oneClass.level.name,
									content: waiter.firstName + ' ' + waiter.lastName +
									' has moved from waiting list to the class ' + oneClass.level.name +
									' on ' + moment(oneClass.datetime).format(sails.config.globals.momentDatetimeFormat),
								}, {
									to: 'admins',
								});

							// Email the carer(s) that the waiter has joined the class
							let carers = dbService.formatResult(result, 'carers');
							carers.forEach(carer => {
								let subject = waiter.firstName + ' is booked in class';
								let br = '<br>';
								let emailContent = '';
								emailContent += 'Hello ' + carer.firstName + ',';
								emailContent += br;
								emailContent += br + 'Place has become available in a class for ' + waiter.firstName + '.';
								emailContent += br;
								emailContent += br + waiter.firstName + ' is welcome in a class for level ' + oneClass.level.name;
								emailContent += br + 'The class is held on: ' + moment(oneClass.datetime).format(sails.config.globals.momentDatetimeFormat);
								emailContent += br;
								emailContent += br + 'Looking forward to seeing you there!'
								emailContent += br + 'Kind regards,';
								emailContent += br + venue.name;
								emailContent += br;
								emailContent += br + 'Please note: you can\'t reply to this email address';
								Message.sendEmail({
									token: token,
									from: 'noReply',
									to: carer.email,
									subject: subject,
									content: emailContent,
									isHtml: true,
								});
							});

							return true;
						})
						.catch(response => {
							sails.log.warn('Error while updating waiter in class ', response);
							return 'Error while updating waiter in class ' + response;
						});

					return waiterPromise;

				} else {
					return Promise.reject('Unexpected error after executing payment: ' + response);
				}
			})
			.catch(response => {
				sails.log.warn('Error executing payment: ', response);
				return 'Error executing payment: ' + response;
			});

		sails.log.info('PAYMENTRESULT:', paymentResult);
		return paymentResult;
	},
	
	/*
	 * Find classes for a term
	 * Report back
	 * - number of classes in that term
	 * - number of classes without a term
	 */
	async attachClassesToTerm(token, term) {

		sails.log.info('attachClassesToTerm:', term);

		if (!token) return;
		if (!term) return;

		// In 3 separate queries: find classes in term, remove out of term and find total orphaned
		// This doesn't work in 1 query as any subsequent query always works on the result set of the previous query

		// 1) Add classes to the term
		let cypher1 =
			'match (term:Term {id: $termId}) ' +
			'match (classes:Class) ' +
				'where not exists(classes.deletedAt) ' +
				'and classes.datetime >= term.startDate and classes.datetime <= term.endDate ' +
			'merge (classes)-[:InTerm]->(term) ' +
			'with term, classes ' +
			'return count(classes) as countClassesInTerm ';
		let result1 = await db
			.run(cypher1, {
				termId: term.id,
			});
		let countClassesInTerm = dbService.formatResult(result1, 'countClassesInTerm')[0];

		// 2) Remove classes outside the term dates
		let cypher2 =
			'match (term:Term {id: $termId}) ' +
			'match (classesOut:Class)-[notInTerm:InTerm]->(term) ' +
				'where not exists(classesOut.deletedAt) ' +
				'and classesOut.datetime < term.startDate or classesOut.datetime > term.endDate ' +
			'delete notInTerm ';
		let result2 = await db
			.run(cypher2, {
				termId: term.id,
			});
		let countClassesOut = dbService.formatResult(result2, 'countClassesOut')[0];

		// 3) Count number of orphaned classes, ie classes without a term
		let cypher3 =
			'match (classesNotIn:Class) ' +
				'where not exists(classesNotIn.deletedAt) ' +
				'and not (classesNotIn)-[:InTerm]->(:Term) ' +
			'return count(classesNotIn) as countClassesNotInTerm ';
		let result3 = await db
			.run(cypher3, {
				termId: term.id,
			});
		let countClassesNotInTerm = dbService.formatResult(result3, 'countClassesNotInTerm')[0];

		return {
			countClassesInTerm: countClassesInTerm,
			countClassesNotInTerm: countClassesNotInTerm,
		};
	},

	validate (oneClass) {
		let errors = [];
		let isValid = true;

		if (!oneClass.levelId &&
			!(oneClass.level && oneClass.level.id)) {
			errors.push('A level must be selected');
			isValid = false;
		}

		// Check the repeat info
		if (oneClass.isRepeating) {
			// Rule: the class day must be in the repeatDays
			let day = moment(oneClass.datetime).format('dd').toLowerCase();
			if (oneClass.repeatDays && oneClass.repeatDays.indexOf(day) < 0) {
				errors.push('The day of the class must be in the repeat days');
				isValid = false;
			}

			// Rule: start at sot OR a date
			if (oneClass.repeatStarts !== 'sot' && !moment(oneClass.repeatStarts).isValid()) {
				errors.push('Please give a repeat start date');
				isValid = false;
			}

			// Rule: until eot OR a number
			if (oneClass.repeatEnds !== 'eot' && isNaN(parseInt(oneClass.repeatEnds))) {
				errors.push('Please specify the repeat \'until\'');
				isValid = false;
			}
		}

		return {
			isValid: isValid,
			errors: (errors.length > 0 ? errors : null),
			class: oneClass,
		};
	},

}

