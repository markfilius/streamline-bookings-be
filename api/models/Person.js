/**
 * Person.js
 *
 * @description :: A model definition for :Person
 *
 * "A model manages itself and outgoing relationships"
 *
 */

const db            = dbService.connect();
const hashService   = require('sails-service-hash');
const bcrypt        = hashService('bcrypt');
let shortid         = require('shortid');
let moment          = require('moment');


// Provide an empty person to ensure all fields are available
const emptyPerson = {
	firstName: '',
	lastName: '',
	displayName: '',
	email: '',
	phoneNumber: '',
	address1: '',
	address2: '',
	address3: '',
	emergency1: '',
	emergency2: '',
	dob: '',
	password: '',
	passwordConfirm: '',
	setPassword: false,
	isInstructor: false,
	isAdmin: false,
	isActive: false,
};


module.exports = {

	attributes: {
		firstName: {
			type: 'string',
			required: true,
		},
	},

	/**
	 * Return a list of persons for a venue
	 * For now this is only used for staff
	 * Perhaps later on reuse to get other lists of persons
	 */
	list(token, relationshipType = null, role = null) {

		if (!token) return;
		if (['staff'].indexOf(relationshipType) < 0) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		let selector = role ? '{' + role + ':true}' : '';

		sails.log.info('LISTSTAFF', venue, selector);

		let cypher =
			'match ' +
			(relationshipType === 'staff' ? '(venue:Venue {id: $venueId})-[employs:Employs ' + selector + ']-> ' : '') +
			'(persons:Person) ' +
			'where not exists(persons.deletedAt) ' +
			'return persons, employs';

		let personPromise = db
			.run(cypher, {
				venueId: venue.id,
			})
			.then(result => {

				let staff = dbService.formatResult(result, 'persons');
				let employs = dbService.formatResult(result, 'employs');

				sails.log.info('AfterStaffFetch:', result, staff, employs);

				if (!staff || staff.length == 0) {
					return Promise.reject('No staff have been found for location ' + venue.name);
				}

				// Per staff member, nullify passwords, add role for this venue
				for (var i = 0; i < staff.length; i++) {

					// Merge the person with empty person to fill any default values
					staff[i] = Object.assign({}, emptyPerson, staff[i]);

					staff[i].password = null;
					staff[i].isAdmin = employs[i].admin ? true : false;
					staff[i].isInstructor = employs[i].instructor ? true : false;
					staff[i].isActive = staff[i].isAdmin || staff[i].isInstructor ? true : false;
				}

				// Sort the staff: first active-status, then alphabetical
				staff.sort((a, b) => {
					if (a.isActive !== b.isActive) {
						return a.isActive ? -1 : 1;
					}

					return a.firstName.localeCompare(b.firstName);
				});

				return {
					staff: staff
				}
			})
			.catch(err => {
				sails.log.error(err);
				return 'Error while getting staff for this location';
			});

		return personPromise;
	},

	/*
	 * Create a person - specifically for a group
	 */
	create(token, person = null, groupId = null) {

		if (!token) return;
		if (!person) return;
		if (!groupId) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;
		sails.log.info('MODELPERSON CREATE', venue, person);

		// Generate an unique id
		let id = shortid.generate();

		// Create the node
		// Note: for new persons there's no need to add level processing (see the update processing) because a new person
		// won't have a name yet and won't pass validation
		let personPromise = db
			.run(
				'merge (person:Person {' +
				'id              : $id, ' +
				'title           : $title, ' +
				'firstName       : $firstName, ' +
				'lastName        : $lastName, ' +
				'address         : $address, ' +
				'suburb          : $suburb, ' +
				'postcode        : $postcode, ' +
				'state           : $state, ' +
				'phone           : $phone, ' +
				'email           : $email, ' +
			    (person.setPassword ? 'password: $password, ' : '') +
			    'dob             : $dob, ' +
			    'gender          : $gender, ' +
			    'relationship    : $relationship, ' +
			    'useGroupAddress : true, ' +
			    'isMainContact   : $isMainContact, ' +
			    'isCarer         : $isCarer, ' +
			    'isDependant     : $isDependant, ' +
			    'isSwimmer       : $isSwimmer, ' +
			    'hasMedicalIndication: $hasMedicalIndication, ' +
			    'medicalIndication: $medicalIndication, ' +
			    'notes           : $notes ' +
				'}) return person'
				,
				{
					id: id,
					title: person.title || '',
					firstName: person.firstName || '',
					lastName: person.lastName || '',
					address: person.address || '',
					suburb: person.suburb || '',
					postcode: person.postcode || '',
					state: person.state || '',
					phone: person.phone || '',
					email: person.email || '',
					password: bcrypt.hashSync(person.password || '_r4Nd0m-') || '',
					dob: person.dob || '',
					gender: person.gender || '',
					relationship: person.relationship || '',
					useGroupAddress: true,
					isMainContact: person.isMainContact || false,
					isCarer: person.isCarer || false,
					isDependant: person.isDependant || false,
					isSwimmer: person.isSwimmer || false,
					hasMedicalIndication: person.hasMedicalIndication || false,
					medicalIndication: person.medicalIndication || '',
					notes: person.notes || '',
				}
			)
			.then(function (result) {
				sails.log.info('MODELPERSON Suc create groupPerson', result);

				let newPersons = dbService.formatResult(result, 'person');
				let newPerson = newPersons[0];

				// Create the relationship, must be within our venue
				return db
					.run(
						'match (:Venue {id: $venueId})-[:HasGroup]->(g:Group {id: $groupId}) ' +
						'with g ' +
						'match (p:Person {id: $personId}) ' +
						'merge (g)-[:HasPerson]->(p)'
						,
						{
							venueId: venue.id,
							groupId: groupId,
							personId: newPerson.id,
						}
					)
					.then(function (s) {
						sails.log.info('MODELPERSON Suc create HasPerson', s);
						return {
							person: newPerson
						};
					})
					.catch(function (e) {
						sails.log.info('MODELPERSON Err create HasPerson:', e);
						return e;
					});

			})
			.catch(function (e) {
				sails.log.info('MODELPERSON Err create newPerson:', e)
				return e;
			});

		return personPromise;
	},

	/*
	 * Find a person. Add their details for:
	 *      group basics,
	 *      levelsHistory,
	 *      level (current),
	 *      classes (enrolled in) and relevant info such as term
	 *      paymentMethods
	 *      credits
	 *      persons in their group
	 *
	 * Not suitable for admins - they're not in a group
	 */
	async find(personId) {

		sails.log.info('PERSON.FIND', personId)

		if (!personId) return Promise.resolve({});

		return db
			.run('optional match (venueForGroup:Venue)-[:HasGroup]->(group:Group)-[:HasPerson]->(personInGroup:Person {id: $personId}) ' +
				     'where not exists(personInGroup.deletedAt) ' +
				 'optional match (venueForStaff:Venue)-[:Employs]->(personInStaff:Person {id: $personId}) ' +
				     'where not exists(personInStaff.deletedAt) ' +
				 'optional match (group)-[:HasPerson]->(persons:Person) where not exists(persons.deletedAt) ' +
				 'optional match (persons)-[atLevels:AtLevel]->(levelHistory:Level) ' +
				 'optional match (persons)-[swimsIn:SwimsIn]->(classes:Class)-[:TeachesLevel]->(classLevels:Level) ' +
				 'optional match (classes)-[:InTerm]->(terms:Term) ' +
				 'optional match (persons)-[:PaysUsing]->(paymentMethods:PaymentMethod) ' +
				 'optional match (personInGroup)-[:HasCredit]->(credits:Credit) ' +
				 'return venueForGroup, venueForStaff, group, personInStaff, personInGroup, persons, ' +
				     'atLevels, levelHistory, swimsIn, classes, classLevels, terms, paymentMethods, credits',
				{
					personId: personId,
				})
			.then(result => {
				sails.log.debug('AFTERPERSON.FIND', personId, result);

				// TODO For now: only take the first venue and first group
				let venueForGroup = dbService.formatResult(result, 'venueForGroup')[0]; // My venue
				let venueForStaff = dbService.formatResult(result, 'venueForStaff')[0]; // My venue
				let group = dbService.formatResult(result, 'group')[0];                 // My group
				let personInGroup = dbService.formatResult(result, 'personInGroup')[0]; // The person requested ('me')
				let personInStaff = dbService.formatResult(result, 'personInStaff')[0]; // The person requested ('me')
				let persons = dbService.formatResult(result, 'persons');                // All persons in the group (incl me)
				let atLevels = dbService.formatResult(result, 'atLevels');              // Info over the level change
				let levelHistory = dbService.formatResult(result, 'levelHistory');      // The levels the persons were
				let swimsIn = dbService.formatResult(result, 'swimsIn');                // Info over class cancellation
				let classes = dbService.formatResult(result, 'classes');                // The classes the persons are enrolled in
				let terms = dbService.formatResult(result, 'terms');                    // The terms the classes are in
				let classLevels = dbService.formatResult(result, 'classLevels');        // The levels of the classes
				let paymentMethods = dbService.formatResult(result, 'paymentMethods');  // The creditcards or similar this person has
				let credits = dbService.formatResult(result, 'credits');                // The credits accrued by this person


				sails.log.debug('Venue(s), group, person(s), persons', venueForGroup, venueForStaff, group, personInGroup, personInStaff, persons);
				// sails.log.info('atLevels, levelHistory, swimsIn', atLevels, levelHistory, swimsIn);
				// sails.log.info('classes, classLevels, paymentMethods', classes, classLevels, paymentMethods);
				// sails.log.info('CREDITS', credits);

				// Is the person a staff member or a person in a group?
				let personRequested;
				let venue;
				if (!helperService.isEmptyObject(personInStaff)) {
					personRequested = personInStaff;
					personRequested.isStaff = true;
					venue = venueForStaff;
				} else {
					personRequested = personInGroup;
					personRequested.inGroup = true;
					venue = venueForGroup;
				}

				// Nullify passwords
				personRequested.password = null;
				persons.map(person => person.password = null);

				// For staff just return the basics
				if (personRequested.isStaff) {
					return {
						person: personRequested,
						venue: venue,
					};
				}

				// For group members we need more info
				if (group.isPaused) {
					return 'Error during login. Please call us on ' + venue.phoneNumber;
				}

				// The default role and permissions
				personRequested.canMakeBookings = personRequested.canManageGroup = false;
				if (personRequested.isCarer) {
					personRequested.canMakeBookings = true;
					personRequested.canManageGroup = true;
				}

				// Add basic group info
				personRequested.group = group;

				// Sort info for all persons in the group (probably multiples) returned
				// Map class and other information to each person ie. add it to the person's object
				persons = persons.map((person, index) => {

					// Current and historic levels
					person.atLevel = {};
					person.levelHistory = {};
					if (atLevels[index] && !_.isEmpty(atLevels[index])) {
						let level = Object.assign({}, levelHistory[index], atLevels[index]);
						if (atLevels[index].current) {
							person.atLevel = level;
						} else {
							person.levelHistory = level;
						}
					}

					// Class info
					person.class = {};
					if (!_.isEmpty(classes[index]) && !swimsIn[index].cancelled) {
						person.class = classes[index];
						person.class.level = classLevels[index] || {};
						person.class.terms = [];
						if (!helperService.isEmptyObject(terms[index])) {
							person.class.terms.push(terms[index]);
						}
					}

					// Payment methods
					person.paymentMethod = {};
					if (paymentMethods[index] && !_.isEmpty(paymentMethods[index])) {
						person.paymentMethod = paymentMethods[index];
						person.paymentMethod.details = null;
					}

					// Credits
					person.credit = {};
					if (!_.isEmpty(credits[index])) {
						person.credit = credits[index];
					}

					return person;
				});

				// Collect the info/Collate all to unique persons
				let collatedPersons = [];
				persons.forEach(person => {
					let collatedPerson = collatedPersons.find(cp => cp.id === person.id);
					if (!collatedPerson) {
						person.atLevels = [person.atLevel];
						person.levelsHistory = [person.levelHistory];
						person.classes = [person.class];
						person.paymentMethods = [person.paymentMethod];
						person.credits = [person.credit];
						collatedPersons.push(person);
					} else {
						collatedPerson.atLevels.push(person.atLevel);
						collatedPerson.levelsHistory.push(person.levelHistory);
						collatedPerson.classes.push(person.class);
						collatedPerson.paymentMethods.push(person.paymentMethod);
						collatedPerson.credits.push(person.credit);
					}
				});

				// Uniquify, remove empties and sort all info items for each person
				collatedPersons.forEach(person => {

					person.atLevels = helperService.uniquifyArray(person.atLevels);
					person.levelsHistory = helperService.uniquifyArray(person.levelsHistory);
					person.classes = helperService.uniquifyArray(person.classes);
					person.paymentMethods = helperService.uniquifyArray(person.paymentMethods);
					person.credits = helperService.uniquifyArray(person.credits);

					// Do we need to sort the items? Here would be a good place

					// Clean up of temporary properties
					person.atLevel = null;
					person.levelHistory = null;
					person.class = null;
					person.paymentMethod = null;
					person.credit = null;

					return person;
				});

				// Update the requested person's data with the collated version
				personRequested = Object.assign({}, personRequested, collatedPersons.filter(person => person.id === personRequested.id)[0]);

				sails.log.info('PERSON.FIND IN GROUP RESULT', collatedPersons[0].classes);

				return {
					person: personRequested,
					venue: venue,
					group: group,
					persons: collatedPersons,
				};
			})
			.catch(err => {
				sails.log.error('Error while getting person info', err);
				return 'Error while getting location and group for this person (';
			});

	},

	/*
	 * Get the credit amount for this person
	 */
	async getCredit(personId) {

		sails.log.info('PERSON.GETCREDIT', personId)

		if (!personId) return Promise.resolve(0);

		return db
			.run('match (venue:Venue)-[:HasGroup]->(group:Group)-[:HasPerson]->(person:Person {id: $personId}) where not exists(person.deletedAt) ' +
				'optional match (person)-[:HasCredit]->(credits:Credit) ' +
				'return credits',
				{
					personId: personId,
				})
			.then(result => {

				let credits = dbService.formatResult(result, 'credits');

				let totalCredits = 0;
				credits.map(credit => totalCredits += credit.amount);
				return totalCredits;
			})
			.catch(response => {
				sails.log.warn('ERROR IN PERSON.GETCREDIT', response);
				return response;
			});
	},
	
	/*
	 * Update a person and their level
	 * Better: move the level update to a separate method ????????????????
	 */
	update(token, person = null) {

		if (!token) return;
		if (!person) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;
		sails.log.info('MODELPERSON UPDATE', venue, person);

		let personPromise = db
			.run(
				// Update the person
				'match (:Venue {id: $venueId})-[:HasGroup]->(:Group)-[:HasPerson]->(person:Person {id: $personId}) ' +
				'with person ' +
				'set ' +
				'person.title           = $title, ' +
				'person.firstName       = $firstName, ' +
				'person.lastName        = $lastName, ' +
				'person.address         = $address, ' +
				'person.suburb          = $suburb, ' +
				'person.postcode        = $postcode, ' +
				'person.state           = $state, ' +
				'person.phone           = $phone, ' +
				'person.email           = $email, ' +
				(person.setPassword ? 'person.password = $password, ' : '') +
				'person.dob             = $dob, ' +
				'person.gender          = $gender, ' +
				'person.relationship    = $relationship, ' +
				'person.useGroupAddress = $useGroupAddress, ' +
				'person.isMainContact   = $isMainContact, ' +
				'person.isCarer         = $isCarer, ' +
				'person.isDependant     = $isDependant, ' +
				'person.isSwimmer       = $isSwimmer, ' +
				'person.hasMedicalIndication = $hasMedicalIndication, ' +
				'person.medicalIndication = $medicalIndication, ' +
				'person.notes           = $notes ' +
				'return person'
				,
				{
					venueId: venue.id,
					personId: person.id,
					title: person.title || '',
					firstName: person.firstName || '',
					lastName: person.lastName,
					address: person.address || '',
					suburb: person.suburb || '',
					postcode: person.postcode || '',
					state: person.state || '',
					phone: person.phone || '',
					email: person.email || '',
					password: bcrypt.hashSync(person.password || '-_r4Nd0m-_') || '',
					dob: person.dob || '',
					gender: person.gender || '',
					relationship: person.relationship || '',
					useGroupAddress: true,
					isMainContact: person.isMainContact || false,
					isCarer: person.isCarer || false,
					isDependant: person.isDependant || false,
					isSwimmer: person.isSwimmer || false,
					hasMedicalIndication: person.hasMedicalIndication || false,
					medicalIndication: person.medicalIndication || '',
					notes: person.notes || '',
				}
			);

		// Handle levels: reset the level in case there is a new level for a swimmer
		let currentLevelsPromise = true;
		let newLevelId = person.newLevel;
		if (newLevelId && newLevelId.length > 0) {
			currentLevelsPromise = db
				.run(
					// First set all levels to non-current
					'match (:Venue {id: $venueId})-[:HasGroup]->(:Group)-[:HasPerson]->(person:Person {id: $personId}) ' +
					'with person ' +
					'optional match (person)-[atLevels:AtLevel]->(:Level) ' +
					'set atLevels.current = false '
					,
					{
						venueId: venue.id,
						personId: person.id,
					}
				);
		}

		// Handle a payment method: if no id, it's new, otherwise update it
		// Note: expect never to update a creditcard (they're deleted and recreated)
		let paymentMethodPromise = true;
		let paymentMethod = person.paymentMethodTokenized;
		if (paymentMethod && !_.isEmpty(paymentMethod)) {
			let cypher = '';

			// Find, create or delete the payment method
			if (paymentMethod.id) {
				cypher += 'match (paymentMethod:PaymentMethod {id: $paymentMethodId})';
			} else {
				cypher += 'create (paymentMethod:PaymentMethod {id: $paymentMethodId})';
			}

			let paymentId = paymentMethod.id || shortid.generate();
			let last4 = '';
			let cardToken = '';
			switch (paymentMethod.type) {
				case 'creditcard':
					last4 = paymentMethod.last4 || paymentMethod.cardDetails.cardLastfour;
					cardToken = paymentMethod.cardToken || paymentMethod.cardDetails.cardToken;
					break;

				case 'paypal':
					last4 = 'ypal';
					break;

			}

			if (person.paymentMethodDelete) {
				// Remove the relation between the payment method and the person
				cypher += `
					match (person {id: $personId})
					match (person)-[paysUsing:PaysUsing]->(paymentMethod)
					delete paysUsing
				`;

			} else {

				// Setup the payment method and relation to the person
				cypher += `
					set 
					paymentMethod.cardToken = $cardToken,
					paymentMethod.last4 = $last4,
					paymentMethod.type = $type
					with paymentMethod
					match (person {id: $personId})
					merge (person)-[:PaysUsing]->(paymentMethod)
				`;
			}
			sails.log.info('CYPHER', cypher, paymentId, last4, person.id);

			paymentMethodPromise = db
				.run(cypher, {
					cardToken: cardToken,
					last4: last4,
					paymentMethodId: paymentId,
					personId: person.id,
					type: paymentMethod.type,
				});
		}

		// Execute all promises, find all levels and paymentMethods for this person
		return Promise
			.all([personPromise, currentLevelsPromise, paymentMethodPromise])
			.then(results => {
				sails.log.info('AfterGroupPersonUpdate: ok', newLevelId);

				// Run the new level update after the update of all current levels. This avoids apparant timing(?)
				// issues of updating all levels and then only one in one cypher query.
				// First add the new level, if it's not empty or 'none'
				let newLevelPromise = true;
				if (newLevelId && newLevelId != 'none') {

					// TODO this query and previous where all levels are reset: must be doable in 1 query
					// TODO do that above in currentLevelsPromise, and then all we need to do here is person.find & return

					let cypher = '';
					cypher +=
						'match (:Venue {id: $venueId})-[:HasGroup]->(:Group)-[:HasPerson]->(person:Person {id: $personId}) ' +
						'match (level:Level {id: $levelId}) ' +
						'with distinct person, level ' +                 // Ensure only 1 person, level
						'create (person)-[:AtLevel {current: true, startDate: $now}]->(level) ';

					newLevelPromise = db.run(cypher, {
						levelId: newLevelId,
						now: moment().format(),
						personId: person.id,
						venueId: venue.id,
					});
				}

				return Promise
					.all([newLevelPromise])
					.then(response => {

						// Get the updated info for the person
						return this.find(person.id);
					})
					.catch(response => {
						sails.log.info('Error while setting new level', response);
						return 'Error while setting new level ' + response;
					});
			})
			.catch(err => {
				sails.log.error(err);
				sails.log.info('Error while updating groupPerson ', err);
				return 'Error while updating person ' + err;
			});
	},

	/*
	 * Check is this person exist in the database
	 * return values:
	 *      0 = doesn't exists
	 *      1 = exists anywhere
	 * in the future maybe do this:
	 *      2 = exists in the venue in the token
	 *      3 = exists in another venue
	 */
	exists(token, person = null) {

		if (!token) return;
		if (!person) return;

		sails.log.info('MODELPERSON EXISTS', person);

		let personPromise = db
			.run(
				'match (person:Person {email: $email}) where not exists(person.deletedAt) ' +
				'return person'
				,
				{
					email: person.email,
				}
			);

		return personPromise
			.then(results => {
				return dbService.formatResult(results, 'person')[0] ? 1 : 0;
			})
			.catch(err => {
				return 'Error while finding if person exists ' + err;
			});
	},

	/*
	 * Delete a person. We need to know if we're trying to delete a staff member or group member
	 */
	async delete(token, person = null, relationshipType = null) {
		sails.log.info('MODELPERSON DELETE', person);

		if (!token) return;
		if (!person) return;
		if (['staff', 'group', 'mobile'].indexOf(relationshipType) < 0) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		let isNotDeletable = await this.isNotDeletable(token, person);
		if (isNotDeletable) {
			let reason = isNotDeletable + (relationshipType === 'mobile'
				? ''
				: '. Set ' + person.firstName + ' as not-a-carer to disable their login');
			return Promise.reject(reason);
		}

		let cypher =
			'match ' +
			(relationshipType === 'staff' ? '(venue:Venue {id: $venueId})-[:Employs]->' : '') +
			(relationshipType === 'group' ? '(venue:Venue {id: $venueId})-[:HasGroup]->(:Group)-[:HasPerson]->' : '') +
			(relationshipType === 'mobile'? '(venue:Venue {id: $venueId})-[:HasGroup]->(:Group)-[:HasPerson]->' : '') +
			'(person:Person {id: $personId}) ' +
			'set person.deletedAt = $deletedAt ' +
			'set person.deletedWhat = $deletedWhat ' +
			'with venue, person ' +
			'merge (venue)-[:HasDeletedList]->(deletedList:DeletedList) ' +
			'merge (deletedList)-[:IsDeleted]->(person) ';

		let personPromise = db
			.run(cypher, {
				venueId: venue.id,
				personId: person.id,
				deletedAt: moment().format(),
				deletedWhat: relationshipType + ': ' + person.firstName + ' ' + person.lastName,
			})
			.then(results => {
				return true;
			})
			.catch(err => {
				return 'Error while deleting person ' + err;
			});

		return personPromise;
	},

	/*
     * Disallow delete of a person
	 * Return false (= delete is allowed) or a string with the reason
	 * rules:
	 *      staff:
	 *          always possible so ignore all incoming relationships: payment & credit and also messages
	 *      group:
	 *          must not be an active swimmer i.e. in a future class
	 *          cant delete a carer with credits
	 *          cant delete last carer if still dependants
	 *
	 * Note: the person object will only contain basic info
	 */
	async isNotDeletable(token, person) {

		if (!token) return;
		if (!person) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		// Get the full details
		let personDetails = await this.find(person.id);
		sails.log.debug('ISNOTDELETABLEPERSON:', personDetails.person.isStaff, personDetails.person.isSwimmer, personDetails );

		// Rule: staff always possible
		if (personDetails.person.isStaff) return Promise.resolve(false);

		// Rule: must not be an active swimmer i.e. in a future class
		if (personDetails.person.isSwimmer && personDetails.person.classes && personDetails.person.classes.length) {
			return `${personDetails.person.firstName} is still enrolled in ${personDetails.person.classes.length} classes`
		}

		// Rule: cant delete a carer with credits
		let totalCredits = 0;
		personDetails.person.credits.forEach(credit => totalCredits += credit.amount);
		if (totalCredits > 0) {
			return Promise.resolve(person.firstName + ' still has $' + (totalCredits / 100).toString() + ' in credits');
		}

		// Rule: cant delete last carer if still dependants
		let nrDependants = personDetails.persons.filter(person => person.isDependant).length;
		let nrCarers = personDetails.persons.filter(person => person.isCarer).length;
		if (person.isCarer && nrCarers <= 1 && nrDependants > 0) {
			return Promise.resolve(person.firstName + ' is the only carer for ' + nrDependants + ' dependants');
		}
		if (nrCarers <= 1) {
			return Promise.resolve('There should be at least 1 carer for the ' + nrDependants + ' dependants');
		}

		let personPromise = Promise.resolve(false);

		return personPromise;
	},

}

