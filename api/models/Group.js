/**
 * Group.js
 *
 * @description :: A model definition for :Group
 *
 * "A model manages itself and outgoing relationships"
 *
 */

const db            = dbService.connect();
let shortid         = require('shortid');
let moment          = require('moment');
let Person          = require('../models/Person');

module.exports = {

	attributes: {
		name: {
			type: 'string',
			required: true,
		},
	},

	/*
	 * Create a new group adding to the venue from the token
	 * Return the updated group
	 */
	create(token, group = null) {

		if (!token) return;
		if (!group) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;
		sails.log.info('MODELGROUP CREATE', venue, group);

		// Generate an unique id
		let id = shortid.generate();

		// Create the node
		let groupPromise = db
			.run(
				'match (venue:Venue {id: $venueId}) ' +
				'with venue ' +
				'merge (venue)-[:HasGroup]->(groupF:Group {' +
				'id              : $groupId, ' +
				'name            : $name, ' +
				'address         : $address, ' +
				'suburb          : $suburb, ' +
				'postcode        : $postcode, ' +
				'state           : $state, ' +
			    'isPaused        : $isPaused, ' +
			    'notes           : $notes, ' +
				'createdAt       : $createdAt ' +
				'}) return group'
				,
				{
					venueId: venue.id,
					groupId: id,
					name: group.name || '',
					address: group.address || '',
					suburb: group.suburb || '',
					postcode: group.postcode || '',
					state: group.state || '',
					isPaused: group.isPaused || false,
					notes: group.notes || '',
					createdAt: moment().format(),
				}
			)
			.then(function (result) {
				sails.log.info('MODELGROUP Suc create group', result);

				let newGroups = dbService.formatResult(result, 'group');
				let newGroup = newGroups[0];

				return {
					group: newGroup
				};
			})
			.catch(function (e) {
				sails.log.warn('MODELGROUP Err create newGroup:', e)
				return e;
			});

		return groupPromise;
	},

	/*
	 * Find all group info for the requested group. Must be in the venue in the token.
	 * Include in the info a list:
	 *      - basic details of persons in the group
	 *      - payments done by this group
	 */
	async find(token, groupId) {

		if (!token) return;
		if (!groupId) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;
		sails.log.info('MODELGROUP.FIND', venue, groupId);

		let groupPromise = db
			.run(
				'match (venue:Venue {id: $venueId}) ' +
				'with venue ' +
				'match (venue)-[:HasGroup]->(group:Group {id: $groupId}) ' +
				'optional match (group)-[:HasPerson]->(persons:Person) where not exists(persons.deletedAt) ' +
				'optional match (group)-[:HasPayment]-(payments:Payment) ' +
				'optional match (payments)-[:PaidBy]-(payers:Person) where not exists(payers.deletedAt) ' +
				'optional match (payments)-[:PaidFor]-(swimmers:Person) where not exists(swimmers.deletedAt) ' +
				'optional match (payments)-[:PaidFor]-(classes:Class) where not exists(classes.deletedAt) ' +
				'optional match (classes)-[:TeachesLevel]-(levels:Level) where not exists(levels.deletedAt) ' +
				'optional match (payments)-[:PaidUsing]-(paymentMethods:PaymentMethod) ' +
				'return group, persons, payments, payers, swimmers, classes, levels, paymentMethods'
				,
				{
					venueId: venue.id,
					groupId: groupId,
				}
			)
			.then(response => {
				sails.log.info('MODELGROUP Suc find group', response);

				let group = dbService.formatResult(response, 'group')[0];
				let persons = dbService.formatResult(response, 'persons');
				let payments = dbService.formatResult(response, 'payments');
				let payers = dbService.formatResult(response, 'payers');
				let swimmers = dbService.formatResult(response, 'swimmers');
				let classes = dbService.formatResult(response, 'classes');
				let levels = dbService.formatResult(response, 'levels');
				let paymentMethods = dbService.formatResult(response, 'paymentMethods');

				// Nullify passwords for payers
				payers.forEach(payer => payer.password = null);

				// Add the persons to the group object
				group.persons = [];
				persons.forEach(person => {
					if (_.isEmpty(person)) return;

					person.password = null;
					group.persons.push(person)
				});
				group.persons = helperService.uniquifyArray(group.persons);

				// Add the levels to the classes. Expect only 1 level per class
				classes.forEach((oneClass, index) => oneClass.level = levels[index] || {});

				// Collate the payments
				collatedPayments = helperService.collateResults(payments, {
					payers,
					swimmers,
					classes,
					paymentMethods,
				});

				// Add the payments to the group object
				group.payments = collatedPayments;

				return {
					group: group,
				};

			})
			.catch(response => {
				sails.log.warn('MODELGROUP Err findGroup:', response)
				return response
			});

		return groupPromise;
	},

	/*
	 * Delete a group
	 */
	async delete(token, group = null) {

		if (!token) return;
		if (!group) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;
		sails.log.info('MODELGROUP DELETE', venue, group);

		let isNotDeletable = await this.isNotDeletable(token, group);
		if (isNotDeletable) {
			return Promise.reject(isNotDeletable);
		}

		let cypher =
			'match (venue:Venue {id: $venueId})-[:HasGroup]->(group:Group {id: $groupId}) ' +
			'set group.deletedAt = $deletedAt ' +
			'set group.deletedWhat = $deletedWhat ' +
			'with venue, group ' +
			'merge (venue)-[:HasDeletedList]->(deletedList:DeletedList) ' +
			'merge (deletedList)-[:IsDeleted]->(group) ';

		let groupPromise = db
			.run(cypher,
				{
					venueId: venue.id,
					groupId: group.id,
					deletedAt: moment().format(),
					deletedWhat: group.name,
				}
			)
			.then(function (s) {
				return true;
			})
			.catch(function (e) {
				sails.log.warn('MODELGROUP Err delete group:', e)
				return e;
			});

		return groupPromise;
	},

	/*
	 * Disallow delete of a group
	 * Return false (= delete is allowed) or a string with the reason
	 * rules:
	 *      cant delete a group where a person has credits
	 */
	async isNotDeletable(token, group) {

		if (!token) return;
		if (!group) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		// Get the full details
		let groupDetails = await this.find(token, group.id);
		sails.log.info('ISNOTDELETABLEGROUP:', groupDetails);

		// Rule: cant delete a group where a person has credits
		// So: get a list of credits for this group
		let creditPromises = [];
		let totalCredits = 0;
		let persons = '';
		groupDetails.group.persons.forEach(person => {
			creditPromises.push(Person.getCredit(person.id));
		});

		return Promise.all(creditPromises)
			.then(response => {

				// Check each credit
				response.forEach((personsCredit, index) => {
					if (!isNaN(personsCredit) && personsCredit > 0) {
						persons += groupDetails.group.persons[index].firstName + ' ';
						totalCredits += personsCredit;
					}
				});
				if (totalCredits > 0) return Promise.resolve('Persons: ' + persons + '- have a total credit of $' + (totalCredits/100).toString());

				// No credits so return false (= is deletable)
				return Promise.resolve(false);
			})
			.catch(response => {
				sails.log.warn('ERROR whle getting credits', response);
				return Promise.resolve(response);
			});
	},


}

