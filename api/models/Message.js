/**
 * Message.js
 *
 * @description :: A model definition for :Message
 *
 *  * "A model manages itself and outgoing relationships"
 */

const db            = dbService.connect();
const shortid       = require('shortid');
const moment        = require('moment');
const nodemailer    = require('nodemailer');
const fileSystem    = require('fs');

module.exports = {

	attributes: {
		title: {
			type: 'string',
			required: true,
		},
		content: {
			type: 'string',
			required: true,
		},
		datetime: {
			type: 'string',
			required: true,
		}
	},

	//
	// Get the messages for the person in the token
	//
	getFor(token) {

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;
		const person = decoded.payload.person;
		// sails.log.info('MODELMESSAGE GETFOR', venue, person);

		let messagesPromise = db
			.run('match (venue:Venue {id: $venueId})-[:Employs {admin: $isAdmin}]->(person:Person {id: $personId}) ' +
				'optional match (person)<-[messageFors:MessageFor]-(messages:Message) ' +
				'return messageFors, messages order by messages.datetime desc',
				{
					venueId: venue.id,
					personId: person.id,
					isAdmin: true,
				}
			)
			.then(result => {
				return this.formatMessages(result);
			})
			.catch(err => {
				sails.log.error(err);
				sails.log.info('Error AfterMessagesFetch ', err);
				return 'Error AfterMessagesFetch ' + err;
			});

		return messagesPromise;
	},

	//
	// Toggle the read status for this message
	//
	toggle(token, message) {

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;
		const person = decoded.payload.person;
		// sails.log.info('MODELMESSAGE TOGGLE', message);

		let messagesPromise = db
			.run('match (venue:Venue {id: $venueId})-[:Employs {admin: $isAdmin}]->(person:Person {id: $personId}) ' +
				 'match (person)<-[messageFor:MessageFor]-(message:Message {id: $messageId}) ' +
				 'set messageFor.isRead = $newReadStatus ' +
				 'with person ' +
				 'match (person)<-[messageFors:MessageFor]-(messages:Message) ' +
				 'return messageFors, messages order by messages.datetime desc',
				{
					venueId: venue.id,
					personId: person.id,
					isAdmin: true,
					messageId: message.id,
					newReadStatus: !message.isRead,
				}
			)
			.then(result => {
				return this.formatMessages(result);
			})
			.catch(err => {
				sails.log.error(err);
				sails.log.info('Error AfterMessagesFetch ', err);
				return 'Error AfterMessagesFetch ' + err;
			});

		return messagesPromise;
	},

	//
	// Send a message to all admins, or the person (who must be admin) in the token, for the venue in the token
	//
	// message format: object with properties:
	//      title: string
	//      content: string
	//      datetime: optional datetime ISO string: defaults to moment().format()
	// options format: object with properties:
	//      to: 'admins' for all admins
	//
	send(token, message, options = null) {

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;
		const person = decoded.payload.person;
		// sails.log.info('MODELMESSAGE SEND', venue, person, options);

		// Send to the person in the token (ie logged in in web) or all admins
		let addPersonId = '{id: $personId}';
		if (options && options.to === 'admins') {
			addPersonId = '';
		}

		let messagesPromise = db
			.run(
				 'create (message:Message {' +
										    'id: $messageId, ' +
										    'title: $title, ' +
										    'content: $content, ' +
										    'datetime: $datetime ' +
										 '}) ' +
				 'with message ' +
				 'match (venue:Venue {id: $venueId})-[:Employs {admin: $isAdmin}]->(persons:Person ' + addPersonId + ') ' +
				 'merge (message)-[:MessageFor {isRead: $readStatus}]->(persons) ',
				{
					venueId: venue.id,
					personId: person.id,
					isAdmin: true,
					messageId: shortid.generate(),
					title: message.title || '',
					content: message.content || '',
					datetime: message.datetime || moment().format(),
					readStatus: false,
				}
			)
			.then(result => {
				sails.log.info('AfterMessagesSend success'); // , result);
				return true;
			})
			.catch(err => {
				sails.log.error(err);
				sails.log.info('Error AfterMessagesSend ', err);
				return false;
			});

		return messagesPromise;
	},

	/*
	 * Take result of the query containing messages and messageFors
	 * Return array of messages
	 *    - formatted for with the correct status
	 *    - only unread or recent read messages
	 */
	formatMessages (result) {
		let messages = dbService.formatResult(result, 'messages');
		let messageFors = dbService.formatResult(result, 'messageFors');

		// Enforce a 'isRead' status, enforce a date, remove empty messages, remove non recent, read messages
		messages = messages.map((message, index) => {
			if (!helperService.isEmptyObject(message)) {
				message.isRead = messageFors[index].isRead || false;
				message.datetime = message.datetime || moment().format();
				if (!messageFors[index].isRead || moment().diff(message.datetime, 'days') < 14) {
					return message;
				}
			}
		});

		// sails.log.info('AfterMessagesToggle/GetFor:', messages, messageFors, result);
		return messages;
	},

	/*
	 * Send an email to a group or a single recipient
	 * Message the admins on the result
	 * Parameters passed as usual or as an object
	 *
	 *      tokenOrObject   string: token
	 *                      OR
	 *                      object: with all parameters. This may also contain attachments
	 *      from            string: email address - must be verified by Email gateway
	 *      to              string: a single email address
	 *                      OR
	 *                      object: with properties
	 *                          toAdmins: boolean
	 *                          toInstructors: boolean
	 *                          toAddress: email address
	 *      subject         string
	 *      content         string
	 *      isHtml            boolean, default false. if true, the content will be sent as html
	 */
	sendEmail(tokenOrObject, from, to, subject, content, isHtml = false) {

		if (!_.isObject(tokenOrObject)) {
			token = tokenOrObject;
		} else {
			token = tokenOrObject.token;
			from = tokenOrObject.from;
			to = tokenOrObject.to;
			subject = tokenOrObject.subject;
			content = tokenOrObject.content;
			attachments = tokenOrObject.attachments || [];
			isHtml = tokenOrObject.isHtml;
		}
		if (from === 'noReply') {
			from = 'no-reply@streamlinebookings.com';
		}
		sails.log.info('EMAIL PARAMETERS', from, to, subject, content, attachments);

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		// Only 1 recipient?
		if (!_.isObject(to)) {
			// Convert the singular  recipient to an array
			recipients = [ { email: to } ];
			return this.executeSend(from, recipients, subject, content, isHtml, attachments);
		}

		// Find the recipients' emails
		let emailPromise = db
			.run('match (venue {id: $venueId})-[employs:Employs]->(persons:Person) ' +
				'where false ' +
				(to.toAdmins ? 'or employs.admin = $toAdmins ' : '') +
				(to.toInstructors ? 'or employs.instructor = $toInstructors ' : '') +
				'return persons ',
				{
					venueId: venue.id,
					toAdmins: to.toAdmins,
					toInstructors: to.toInstructors,
				})
			.then(response => {
				let recipients = dbService.formatResult(response, 'persons');

				// Add any singular extra recipient
				if (to.toAddress) {
					recipients = recipients.concat([{email: to.toAddress}]);
				}

				return this.executeSend(from, recipients, subject, content, isHtml, attachments);
			})
			.catch(response => {
				sails.log.info('Database error while retrieving email addresses', response);
				return 'Database error while retrieving email addresses: ' + response;
			});

		return emailPromise;
	},

	executeSend (from, recipients, subject, content, isHtml, attachments) {

		// Set up the transporter
		let transporter = nodemailer.createTransport({
			host: 'email-smtp.eu-west-1.amazonaws.com',
			port: 2587,
			secure: false,                      // STARTTLS assumes a non-secure start
			auth: {
				user: sails.config.smtpUsername,
				pass: sails.config.smtpPassword,
			},
		});

		// Format the attachments array
		let attachmentsToMail = attachments.map(attachment => {
			// If there's a / in the name, assume a path is given
			// Otherwise use the attachments folder
			let path = attachment.indexOf('/') >= 0 ? '' : sails.config.attachmentsRootFolder;
			return {path: path + attachment}
		});

		// Email each recipient
		let mailPromises = [];
		let starttime = moment();
		recipients.forEach(recipient => {

			sails.log.info('SENDINGTO', recipient.email);

			let mailPromise = transporter.sendMail({
				from: from,
				to: recipient.email,
				subject: subject,
				text: content,
				html: isHtml ? content : null,    // Only send html as html
				attachments: attachmentsToMail,
			});

			mailPromises.push(mailPromise);
		});

		let allPromises = Promise
			.all(mailPromises)
			.then(response => {
				sails.log.info('ALLPROMISES success', response);

				// How long did it take?
				let duration = moment().diff(starttime, 'seconds');
				duration = moment.duration(duration, 'seconds').humanize();

				// Send reponses and messages
				let responseData;
				if (response) {
					let accepted = [];
					let rejected = [];
					response.forEach(oneResponse => {
						accepted = accepted.concat(oneResponse.accepted);
						rejected = rejected.concat(oneResponse.rejected);
					});

					// Message the admins
					let summary = accepted.length + ' Emails sent' +
						(rejected.length
							? '. ' + rejected.length + ' Emails failed'
							: '') +
						'. That took ' + duration;

					this.send(token, {
						title: summary,
						content: (rejected.length > 0 ? 'Failed: ' + rejected.join(', ') : 'No emails failed. ') +
								 'Email subject: ' + subject,
						to: 'admins',
					});

					responseData = {
						status: true,
						message: summary,
						rejected: rejected,
					};
				} else {
					responseData = {
						status: false,
						message: 'Unknown error while sending emails',
						rejected: [],
					};
				}
				return responseData;
			})
			.catch(response => {
				sails.log.info('ALLPROMISES error', response);
				return 'Error: ' + response;
			});

		return allPromises;
	},
	
	uploadAttachment(token, attachment) {

		// TODO: move attachments to venue subfolder
		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;
		sails.log.info('MODELMESSAGE ATTACHMENT', attachment.name);

		if (attachment && attachment.content && attachment.name) {
			let regexGroups = attachment.content.match(/^(data:)((.)+)(;base64,)/);
			if (regexGroups && regexGroups.length >= 2) {

				// Construct the file name for storage
				// let fileType = '.' + regexGroups[2].replace('image/', '');
				attachmentFile = /* venue.id + '/' +*/ attachment.name;
				let fileName = sails.config.attachmentsRootFolder + attachmentFile;
				sails.log.info('WRITING ATTACHMENT', fileName);

				// Get the image data
				let base64Data = attachment.content.replace(/^data:image\/(.)+;base64,/, '');

				sails.log.info('WRITING ATTACHMENT', attachmentFile);
				fileSystem.writeFile(fileName, base64Data, 'base64', function (err) {
					if (err) return Promise.reject('Unknown error while saving the attachment');
				});
			}
		}
		return Promise.resolve({
			name: attachment.name,
			status: true,
		});
	},

}

