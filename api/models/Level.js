/**
 * Level.js
 *
 * @description :: A model definition for :Level
 *
 * "A model manages itself and outgoing relationships"
 *
 */

const db            = dbService.connect();
const hashService   = require('sails-service-hash');
const bcrypt        = hashService('bcrypt');
let shortid         = require('shortid');
let moment          = require('moment');
let Message         = require('../models/Message');
let Payment         = require('../models/Payment');


module.exports = {

	attributes: {
		payment: {
			type: 'ref',
			required: true,
		},
	},

	/*
	 * List all the levels for the venue in the token
	 */
	list(token) {

		if (!token) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;
		// sails.log.info('LISTLEVELS FOR:', venue);

		let cypher = '' +
			'match (venue:Venue {id: $venueId})-[hasLevel:HasLevel]->(levels:Level) ' +
			'where not exists(levels.deletedAt) ' +
			'return levels';

		let levelPromise = db
			.run(cypher, {
				venueId: venue.id,
			})
			.then(result => {
				let levels = dbService.formatResult(result, 'levels');
				// sails.log.info('AfterLevelsFetch:', result, levels);
				return levels;
			})
			.catch(response => {
				sails.log.info('Error while getting levels ', response);
				return response;
			});

		return levelPromise;

	},

	/*
	 * Return details for a single level
	 */
	find(token, levelId) {

		if (!token) return;
		if (!levelId) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;


		let levelPromise = db
			.run(
				'match (venue:Venue {id: $venueId})-[:HasLevel]->(level:Level {id: $levelId}) ' +
				'optional match (level)-[:HasProficiency]->(proficiency:Proficiency) ' +
				// Find classes at this level in a current term
				'optional match (level)<-[:TeachesLevel]-(classes:Class)-[:InTerm]->(term:Term) ' +
					'where term.startDate <= $today and $today <= term.endDate ' +
				// Find swimmers in those classes
			    'optional match (classes)<-[:SwimsIn]-(swimmers:Person) ' +
				'return count(swimmers) as nrSwimmers, level, proficiency order by proficiency.order asc',
				{
					venueId: venue.id,
					levelId: levelId,
					today: moment().format('YYYY-MM-DD'),
				})
			.then(result => {

				let nrSwimmers = dbService.formatResult(result, 'nrSwimmers')[0];
				let oneLevel = dbService.formatResult(result, 'level')[0];
				let proficiencies = helperService.uniquifyArray(dbService.formatResult(result, 'proficiency'));

				// Define if the level isActive = has classes with enrolled swimmers within a current term
				oneLevel.isActive = nrSwimmers ? true : false;

				sails.log.info('AfterLevelFetch:', nrSwimmers, oneLevel, proficiencies);

				return {
					level: oneLevel,
					proficiencies: proficiencies,
				};
			})
			.catch(err => {
				sails.log.error(err);
				return 'Error while getting proficiencies for level ' + err;
			});

		return levelPromise;
	},

	/*
	 * Create a new level for a venue
	 */
	create(token, level) {

		if (!token) return;
		if (!level) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		sails.log.info('CREATING LEVEL', level);

		// Create the level for this venue
		let levelPromise = db
			.run(
				'match (venue:Venue {id: $venueId}) ' +
				'merge (venue)-[:HasLevel]->(level:Level {' +
				'id       : $levelId ' +
				(level.cap      ? ',cap      : $cap ' : '') +
				(level.color    ? ',color    : $color ' : '') +
				(level.duration ? ',duration : $duration ' : '') +
				(level.name     ? ',name     : $name ' : '') +
				(level.notes    ? ',notes    : $notes ' : '') +
				'}) return level',
				{
					venueId: venue.id,
					levelId: shortid.generate(),
					cap: level.cap,
					color: level.color,
					duration: level.duration,
					name: level.name,
					notes: level.notes,
				}
			)
			.then(function (result) {
				sails.log.info('Suc create level', result);
				level = dbService.formatResult(result, 'level')[0];
				return {
					level: level,
				};
			})
			.catch(function (err) {
				sails.log.error('Err create level:', err)
				return 'Error while creating level: ' + err;
			});

		return levelPromise;
	},

	/*
	 * Update existing level
	 */
	update(token, level) {

		if (!token) return;
		if (!level) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		let levelPromise = db
			.run('match (venue {id: $venueId})-[:HasLevel]->(level:Level {id: $levelId}) ' +
				(level.cap      ? 'set level.cap       = $cap ' : '') +
				(level.color    ? 'set level.color     = $color ' : '') +
				(level.duration ? 'set level.duration  = $duration ' : '') +
				(level.name     ? 'set level.name      = $name ' : '') +
				(level.notes    ? 'set level.notes     = $notes ' : '') +
				'return level',
				{
					venueId: venue.id,
					levelId: level.id,
					cap:  level.cap,
					color: level.color,
					duration: level.duration,
					name: level.name,
					notes: level.notes,
				}
			)
			.then(result => {
					sails.log.info('AfterLevelUpdate:', result);
					let level = dbService.formatResult(result, 'level')[0];
					return {
						level: level,
					};
				})
			.catch(err => {
				sails.log.error(err);
				return 'Error while finding level to update ' + err;
			});

		return levelPromise;
	},

	/*
	 * Delete a level (if allowed)
	 */
	async delete(token, level = null) {
		sails.log.info('DELETELEVEL:', level);

		if (!token) return;
		if (!level) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		let isNotDeletable = await this.isNotDeletable(token, level);
		if (isNotDeletable) {
			return Promise.reject(isNotDeletable);
		}

		let cypher = '' +
			'match (venue:Venue {id: $venueId})-[:HasLevel]->(level:Level {id: $levelId}) ' +
			'set level.deletedAt = $deletedAt ' +
			'set level.deletedWhat = $deletedWhat ' +
			'with venue, level ' +
			'merge (venue)-[:HasDeletedList]->(deletedList:DeletedList) ' +
			'merge (deletedList)-[:IsDeleted]->(level) ';

		let levelPromise = db
			.run(cypher, {
				levelId: level.id,
				venueId: venue.id,
				deletedAt: moment().format(),
				deletedWhat: 'Level ' + level.name + ' and proficiencies',
			})
			.then(response => {
				return true;
			})
			.catch(response => {
				sails.log.info('Error while deleting level ', response);
				return 'Error while removing level ' + response;
			});

		return levelPromise;
	},

	/*
	 * Disallow delete
	 * Return false (= delete is allowed) or a string with the reason
	 * rules:
	 *      only possible if no future classes at that level
	 *      only possible if no active swimmers at that level
	 */
	async isNotDeletable(token, level) {

		sails.log.info('ISNOTDELETABLELEVEL:', level);

		if (!token) return;
		if (!level) return;

		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		let cypher = '' +
			'match (venue:Venue {id: $venueId})-[:HasLevel]->(level:Level {id: $levelId}) ' +
			'optional match (classes:Class)-[:TeachesLevel]->(level) where not exists(classes.deletedAt) and classes.datetime >= $now ' +
			'optional match (swimmers:Person)-[:AtLevel]->(level) ' +
			'return count(classes) as nrClasses, count(swimmers) as nrSwimmers ';

		let levelPromise = db
			.run(cypher, {
				levelId: level.id,
				venueId: venue.id,
				now: moment().format(),
			})
			.then(response => {
				let nrClasses = dbService.formatResult(response, 'nrClasses')[0];
				let nrSwimmers = dbService.formatResult(response, 'nrSwimmers')[0];
				sails.log.info('Found numbers active classes, swimmers at level:', nrClasses, nrSwimmers);

				if (nrClasses + nrSwimmers === 0) return false;

				return 'Still ' + nrClasses + ' future classes and ' + nrSwimmers + ' swimmers at this level';
			})
			.catch(response => {
				sails.log.info('Error while determining level deletability ', response);
				return 'Error while determining level deletability ' + response;
			});

		return levelPromise;
	},

	/*
	 * Validate the level data
	 */
	validate (level) {
		let errors = [];
		let isValid = true;

		if (!level.name) {
			errors.push('A name must be given');
			isValid = false;
		}

		if (!level.color) {
			level.color = '#ffffe0';                 // Default colour: light yellow
		}

		return {
			isValid: isValid,
			errors: (errors.length > 0 ? errors : null),
			level: level
		};
	},

}

