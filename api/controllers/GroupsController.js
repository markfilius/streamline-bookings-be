/**
 * groupsController
 *
 * @description :: Server-side logic for managing groups
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const db          = dbService.connect();
const moment      = require('moment');
const hashService = require('sails-service-hash');
const bcrypt      = hashService('bcrypt');
let   shortid     = require('shortid');
let   validate    = require('validate.js');
let   Group       = require('../models/Group');
let   Person      = require('../models/Person');


module.exports = {

	/**
	 * Return a list of groups for a venue
	 */
	listGroups(req, res) {
		if (!allowedService.isAllowed(req.header('token'), 'canManageGroups')) {
			return res.forbidden('This action is not allowed');
		}

		const data = req.body;
		const decoded = jwTokenService.decode(req.header('token'));
		const venue = decoded.payload.venue;
		sails.log.info('VENUEINLISTGROUPS', venue);

		db
			.run(
				'match (venue:Venue {id: $venueId})-[hasGroup:HasGroup]->(groups:Group) ' +
					'where not exists (groups.deletedAt)  '+
				'return groups', {
					venueId: venue.id,
				})
			.then(result => {

				let groups = dbService.formatResult(result, 'groups');

				sails.log.info('AfterGroupsFetch:', result, groups);

				if (!groups || groups.length == 0) {
					return res.notFound('No groups have been found for this location');
				}

				return res.send({
					groups: groups
				})
			})
			.catch(err => {
				sails.log.error(err);
				return res.notFound('Error while getting groups for this location');
			});
	},

	/**
	 * Return details for a single group
	 *
	 * To do so:
	 *      find the group info which will include: basic info for all persons and payments done
	 *      select the first person
	 *      get their detailed information: that will include full details for all persons for their group (ie this group)
	 *      return group basic info, carers, dependants and payments
	 *
	 */
	findGroup(req, res) {

		const data = req.body;
		const token = req.header('token');

		sails.log.info('DATAFINDGROUP', data);

		if (!allowedService.isAllowed(token, 'canManageGroups')) {
			return res.forbidden('This action is not allowed');
		}

		if (!data.id) return;

		if (data.id == 'none') {
			// return empty group object
			return res.send({
				group: {
					id: 'none',
					name: '',
					address: '',
					suburb: '',
					postcode: '',
					state: '',
					email: '',
					isPaused: false,
				},
				groupPersons: [],
				groupDependants: [],
			})
		}

		return Group
			.find(token, data.id)
			.then(response => {
				sails.log.info('FOUND GROUP', response);
				let group = response.group;

				let firstPersonId = group.persons && group.persons.length > 0
					? group.persons[0].id
					: null;

				return Person
					.find(firstPersonId)
					.then(response => {
						sails.log.info('FOUND 1ST PERSON', response);
						let persons = response.persons;

						let carers, dependants;
						if (persons) {
							// Sort all persons alphabetically
							persons.sort((a, b) => a.firstName.localeCompare(b.firstName));

							// Split persons according to isDependant
							carers = persons.filter(person => !person.isDependant);
							dependants = persons.filter(person => person.isDependant);
						}

						return res.send({
							group: group,
							groupPersons: carers || [],
							groupDependants: dependants || [],
						});
					})
					.catch(response => {
						sails.log.error('Error while getting group persons', response);
						return res.notFound('Error while getting group persons: ' + data.name);
					});
			})
			.catch(response => {
				sails.log.error('Error while getting group', response);
				return res.notFound('Error while getting group: ' + data.name);

			});
	},

	/**
	 * Update, delete an group for a venue, or check if this group or email already exists
	 */
	updateGroup(req, res) {
		const data = req.body;

		const id = data.group.id;
		const token = req.header('token') || data.token;
		const action = req.param('action');
		let amCarer = false,
			amStaff = false,
			group = data.group;      // The updated or new group

		if (allowedService.isAllowed(token, 'canManageGroup')) {
			amCarer = true;
			personsGroupId = jwTokenService.decode(token).payload.person.group.id;  // The group of the logged in group

			if (personsGroupId !== group.id) return res.badRequest('You may only update your own group or family');

		} else if (allowedService.isAllowed(token, 'canManageGroups')) {
			amStaff = true;

		} else {
			sails.log.info('NOT ALLOWED - DECODED TOKEN', token, jwTokenService.decode(token));
			return res.forbidden('This action is not allowed');
		}

		sails.log.info('UPDATE DATA', action, data, amCarer, amStaff);

		switch (action) {

			case 'update':

				let validated = this['groups/validategroup'](data.group);
				if (!validated.isValid) {
					return res.badRequest(validated.errors);
				}

				group = validated.group;

				if (group.id && group.id != 'none') {
					// Update existing
					db
						.run('match (group:Group {id: $groupId}) set ' +
							'group.name     = $name, ' +
							'group.address  = $address, ' +
							'group.suburb   = $suburb, ' +
							'group.postcode = $postcode, ' +
							'group.state    = $state, ' +
							'group.phone    = $phone, ' +
							'group.email    = $email, ' +
							'group.isPaused = $isPaused, ' +
							'group.isActive = $isActive, ' +
							'group.notes    = $notes ' +
							'return group'
							,
							{
								groupId: group.id,
								name: group.name,
								address: group.address || '',
								suburb: group.suburb || '',
								postcode: group.postcode || '',
								state: group.state || '',
								phone: group.phone || '',
								email: group.email || '',
								isPaused: group.isPaused || false,
								isActive: group.isActive || true,       // For now: always true
								notes: group.notes || '',
							}
						)
						.then(result => {
							sails.log.info('AfterGroupUpdate:', result);
							let groups = dbService.formatResult(result, 'group');
							return res.send({group: groups[0]});
						})
						.catch(err => {
							sails.log.error('Error while finding group to update ', err);
							return res.notFound('Error while finding group to update ' + err);
						});

				} else {
					// Create new group
					sails.log.info('CREATING', group);
					Group
						.create(token, group)
						.then(response => {
							sails.log.info('Success creating group', response);
							return res.send({
								group: response.group
							});
						})
						.catch(function (e) {
							sails.log.error('Err creating group:', e);
							return res.badRequest(e);
						});
				}
				break;

			case 'delete':
				if (id && id != 'none') {

					sails.log.info('DELETING', group);
					Group
						.delete(token, group)
						.then(function (s) {
							sails.log.info('Suc after delete group: ', s);
							return res.ok();
						})
						.catch(function (e) {
							sails.log.error('Err after delete group: ', e);
							return res.badRequest(e);
						});
				}
				break;
		}
	},

	/**
	 * Validate and correct a group
	 * Return the corrected group and isValid indication and errors
	 */
	validateGroup (group) {
		let errors = [];
		let isValid = true;

		if (!group.name) {
			errors.push('A name must be given');
			isValid = false;
		}

		group.suburb = group.suburb ? group.suburb.toUpperCase() : '';
		group.state = group.state ? group.state.toUpperCase() : '';

		return {
			isValid: isValid,
			errors: (errors.length > 0 ? errors : null),
			group: group
		};
	},

	/**
	 * Add, update or delete a person for a group
	 * The details can include a payment method for a group carer
	 */
	updatePerson(req, res) {
		const data = req.body;
		const token = req.header('token') || data.token;
		const action = req.param('action');

		let amCarer = false,
			amStaff = false,
			groupId = null,
			person = {};

		if (allowedService.isAllowed(token, 'canManageGroup')) {
			amCarer = true;
			person = jwTokenService.decode(token).payload.person;   // The logged in person
			groupId = person.group.id;                              // The group of the logged in person

			if (data.paymentMethodDelete) {
				// A payment method is being deleted. That always belongs to the person logged in
				person.paymentMethodTokenized = data.paymentMethodTokenized;
				person.paymentMethodDelete = true;
			} else if (data.paymentMethodTokenized) {
				// A payment method is being updated. That always belongs to the person logged in
				person.paymentMethodTokenized = data.paymentMethodTokenized;
			} else {
				// Either the carer's or the dependant's personal details are being updated
				person = data.carer || data.dependant;
			}

		} else if (allowedService.isAllowed(token, 'canManageGroups')) {
			amStaff = true;
			person = data.groupPerson;
			groupId = data.group.id;

			if (!groupId || groupId === 'none') return res.badRequest('A group must be selected before updating persons');

		} else {
			sails.log.warn('NOT ALLOWED - DECODED TOKEN', token, jwTokenService.decode(token));
			return res.forbidden('This action is not allowed');
		}

		sails.log.info('UPDATE PERSON DATA', action, person, groupId, amCarer, amStaff);

		switch (action) {

			case 'update':

				let validated = this['groups/validateperson'](person);
				if (!validated.isValid) {
					return res.badRequest(validated.errors);
				}

				person = validated.person;

				if (person.id && person.id != 'none') {
					// Update existing
					return Person
						.update(token, person)
						.then(response => {

							return res.send({
								person: response.person
							});
						})
						.catch(e => {
							sails.log.error('Err updating Person:', e);
							return res.badRequest(e);
						});

				} else {
					// Create new person within a group
					sails.log.info('CREATING IN GROUPID', person, groupId);

					return Person
						.create(token, person, groupId)
						.then(s => {
							sails.log.info('Suc create Person', s);
							return res.send(s);
						})
						.catch(e => {
							sails.log.error('Err create Person:', e);
							return res.badRequest(e);
						});
				}
				break;

			case 'delete':
				if (person.id && person.id != 'none') {
					// Delete the relevant relationships
					const decoded = jwTokenService.decode(token);

					sails.log.info('DELETING', person);

					Person
						.delete(token, person, data.fromMobile ? 'mobile' : 'group')
						.then(function () {
							sails.log.info('Suc after delete Person: ');
							return res.ok();
						})
						.catch(function (response) {
							sails.log.error('Err after delete Person: ', response);
							return res.badRequest(response);
						});
				}
				break;
		}
	},

	/**
	 * Validate and correct a groupPerson
	 * Return the corrected person and isValid indication and errors
	 */
	validatePerson (person) {
		let errors = [];
		let isValid = true;
		let validateErrors = null;

		// Validate names
		if (!person.firstName && !person.lastName) {
			errors.push('A first or last name must be given');
			isValid = false;
		}

		// Validate statusses
		if (person.isCarer && person.isDependant) {
			errors.push('A person cannot be carer and dependant at the same time');
			isValid = false;
		}
		if (person.isDependant) person.isSwimmer = true;

		// Email is required for carers
		if (person.isCarer) {
			validateErrors = validate(person, {
				email: {
					presence: true,
					email: true
				}
			});
		}

		// Password
		if (person.setPassword) {
			validateErrors = validate(person, {
				password: { length: {minimum: 8}},
				passwordConfirm: {equality: 'password'},
			});
		}

		// Add the validate.js errors to the list
		if (validateErrors) {
			isValid = false;
			for (let prop in validateErrors) {
				if (validateErrors.hasOwnProperty(prop)) {
					errors.push(validateErrors[prop][0]);
				}
			}
		}

		return {
			isValid: isValid,
			errors: (errors.length > 0 ? errors : null),
			person: person
		};
	},

}

