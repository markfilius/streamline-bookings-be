/**
 * levelsController
 *
 * @description :: Server-side logic for managing levels
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const db            = dbService.connect();
const hashService   = require('sails-service-hash');
const bcrypt        = hashService('bcrypt');
let   shortid       = require('shortid');
let   Level         = require('../models/Level');


module.exports = {

	/**
	 * Return a list of levels for a venue
	 */
	listLevels(req, res) {
		if (!allowedService.isAllowed(req.header('token'), 'canManageLevels')) {
			return res.forbidden('This action is not allowed');
		}

		const data = req.body;
		const token = req.header('token');

		return Level
			.list(token)
			.then(result => {

				if (!result || result.length == 0) {
					return res.notFound('No levels have been found for this location');
				}
				return res.send({
					levels: result
				});
			})
			.catch(err => {
				sails.log.error(err);
				return res.badRequest('Error while getting levels for this location: ' + err);
			});
	},

	/**
	 * Return details for a single level
	 */
	findLevel(req, res) {
		const data = req.body;
		const token = req.header('token');

		sails.log.info('DATAFINDLEVEL', data);

		if (!allowedService.isAllowed(req.header('token'), 'canManageLevels')) {
			return res.forbidden('This action is not allowed');
		}

		if (!data.id) return;

		if (data.id == 'none') {
			// return empty level object
			return res.send({
				level: {
					id: 'none',
					name: '',
					cap: '',
					duration: '',
					notes: '',
				}
			});
		}

		Level
			.find(token, data.id)
			.then(response => {
				sails.log.info(response);
				return res.send(response);
			})
			.catch(err => {
				sails.log.error(err);
				return res.notFound(err);
			});
	},

	/**
	 * Create, update or delete an level for a venue
	 */
	updateLevel(req, res) {
		const data = req.body;

		const id = data.level.id;
		const token = req.header('token');
		const action = req.param('action');

		sails.log.info('UPDATE DATA', action, id, data, token);

		if (!allowedService.isAllowed(token, 'canManageLevels')) {
			return res.forbidden('This action is not allowed');
		}

		switch (action) {

			case 'update':

				let validated = Level.validate(data.level);
				if (!validated.isValid) {
					return res.badRequest(validated.errors);
				}

				data.level = validated.level;

				if (id && id != 'none') {
					Level
						.update(token, data.level)
						.then(result => {
							sails.log.info('AfterLevelUpdate:', result);
							return res.send(result);
						})
						.catch(response => {
							sails.log.error(response);
							return res.notFound(response);
						});

				} else {
					Level
						.create(token, data.level)
						.then(result => {
							sails.log.info('AfterLevelCreate:', result);
							return res.send(result);
						})
						.catch(response => {
							sails.log.error(response);
							return res.notFound(response);
						});
				}
				break;

			case 'delete':
				if (id && id != 'none') {
					// Delete the relevant relationship and node
					const decoded = jwTokenService.decode(token);
					const venue = decoded.payload.venue;

					// sails.log.info('DELETING RELATIONSHIP FOR', data.level);
					// sails.log.info('TOKENDECODED', decoded, venue);

					return Level
						.delete(token, data.level)
						.then(function () {
							sails.log.info('Suc after delete level: ');
							return res.ok();
						})
						.catch(function (e) {
							sails.log.info('Err after delete level: ', e);
							return res.badRequest(e);
						});
				}
				break;

			case 'validate':
				return res.send(
					Level.validate(data.level)
				);
				break;
		}
	},

	/**
	 * Add or delete a proficiency for a venue
	 */
	updateProficiency (req, res) {
		const data = req.body;

		const levelId = data.level.id;
		const token = req.header('token');
		const action = req.param('action');

		sails.log.info('UPDATE DATA', action, levelId, data);

		if (!allowedService.isAllowed(token, 'canManageLevels')) {
			return res.forbidden('This action is not allowed');
		}

		if (!levelId) return;

		switch (action) {

			case 'update':

				if (!data.proficiency.name) return;

				if (!data.proficiency.id) {

					// Add this proficiency to the level
					let proficiencyId = shortid.generate();

					db
						.run(
							 // Find the lowest/first proficiency
							 'match (l:Level {id: "' + levelId + '"}) ' +
							 'optional match (l)-[:HasProficiency]->(p:Proficiency) ' +
							 'with l, p order by p.order asc limit 1 ' +
							 // Create new prof with even lower order
							 'create (proficiency:Proficiency {' + 'name: "' + data.proficiency.name + '", id: "' + proficiencyId + '", order: p.order-1 }) ' +
							 // Create the relationship
							 'create (l)-[:HasProficiency]->(proficiency) ' +
							 'with l ' +
							 // Get the whole list of proficiencies
							 'match (l)-[:HasProficiency]->(proficiencies:Proficiency) ' +
							 'return proficiencies order by proficiencies.order asc'
						)
						.then(result => {
							sails.log.info('AfterProficiencyAdd:', result);
							let proficiencies = dbService.formatResult(result, 'proficiencies');

							// Need to test here if the proficiencies are returned with an 'order' property. That can happen for the very first prof
							let proficiency = proficiencies[0];
							if (proficiency.order) {
								return res.send({
									proficiency: proficiencies[0],
									proficiencies: proficiencies
								});
							}

							// There isn't an order for this prof. Add an order for each prof. One by one... (don't know how in 1 go...)
							let newOrder = 0;
							let newProficiencies = proficiencies.map(prof => {
								prof.order = ++newOrder;
								db
									.run(
										'match (proficiency:Proficiency {id: "' + prof.id + '"}) ' +
										'set proficiency.order = ' + newOrder.toString()
									)
									.then(result => {})
									.catch(response => {
										sails.log.info('ERRORsettingOrdersForFirstProf', response);
									});
								return prof;
							});
							sails.log.info('ResultAfterSettingOrdersForFirstProf', newProficiencies);

							return res.send({
								proficiency: newProficiencies[0],
								proficiencies: newProficiencies
							});
						})
						.catch(response => {
							sails.log.error(response);
							sails.log.info('Error while finding level/proficiency to add ', response);
							return res.notFound('Error while finding level/proficiency to add ' + response);
						});

				} else {
					// Update this proficiency
					db
						.run('match (proficiency:Proficiency {id: "' + data.proficiency.id + '"}) set ' +
							'proficiency.name  = "' + data.proficiency.name + '", ' +
							'proficiency.order = "' + data.proficiency.order + '" ' +
							' return proficiency'
						)
						.then(result => {
							sails.log.info('AfterProficiencyUpdate:', result);
							let proficiency = dbService.formatResult(result, 'proficiency');
							return res.send({proficiency: proficiency[0]});
						})
						.catch(response => {
							sails.log.error(response);
							sails.log.info('Error while finding proficiency to update ', response);
							return res.notFound('Error while finding proficiency to update ' + response);
						});
				}
				break;

			case 'reorder':

				if (!data.movingId) return;

				// Get all the proficiencies for this level
				db
					.run(
						'match (l:Level {id: "' + levelId + '"})-[:HasProficiency]->(proficiencies:Proficiency) ' +
						'return proficiencies order by proficiencies.order asc'
					)
					.then(result => {
						sails.log.info('AfterProficiencyReorder:', result);
						let proficiencies = dbService.formatResult(result, 'proficiencies');

						// Do the actual reorder, whereby restarting the order numbers starting at 2 in steps of 2
						// Skip the moving prof and set it's order to 1 above the movingAfter prof
						let newOrder = 2;
						let newMovingIdOrder = 1;
						let newProficiencies = proficiencies.map(prof => {
							if (prof.id !== data.movingId) {
								prof.order = newOrder;
								newOrder += 2;
							}
							if (prof.id === data.movedAfterId) {
								newMovingIdOrder = prof.order + 1;
							}
							return prof;
						});
						// Update the moving prof itself
						newProficiencies = newProficiencies.map(prof => {
							if (prof.id === data.movingId) {
								prof.order = newMovingIdOrder;
							}
							return prof;
						});
						// Sort the profs
						newProficiencies.sort((a, b) => {
							return a.order - b.order;
						});
						sails.log.info('AfterProficiencyReorderUpdate:', newProficiencies);

						// Save the new profs. One by one... (don't know how in 1 go...)
						newProficiencies.forEach(prof => {
							db
								.run(
									'match (proficiency:Proficiency {id: "' + prof.id + '"}) ' +
									'set proficiency.order = ' + prof.order.toString()
								)
								.then(result => {})
								.catch(err => {
									sails.log.info('ERRORAFTERSETORDER', err);
									// Sending an error to the client won't work as we've already returned (line 428)
								});
						});

						return res.send({
							proficiencies: newProficiencies
						});
					})
					.catch(err => {
						sails.log.error(err);
						sails.log.info('Error while finding level/proficiency to add ', err);
						return res.notFound('Error while finding level/proficiency to add ' + err);
					});
				break;

			case 'delete':
				if (!data.proficiencyId) return;

				// Delete the relevant relationship and node
				const decoded = jwTokenService.decode(token);
				const venue = decoded.payload.venue;

				db
					.run(
						'match ' +
						'(proficiency:Proficiency {id: "' + data.proficiencyId + '"}) ' +
						'<-[h:HasProficiency]- ' +
						'(l:Level {id: "' + data.level.id + '"}) ' +
						'delete proficiency, h ' +
						'return proficiency'
					)
					.then(function (response) {
						sails.log.info('Suc after delete prof: ', response);
						let proficiency = dbService.formatResult(response, 'proficiency');
						return res.ok(proficiency[0]);
					})
					.catch(function (e) {
						sails.log.info('Err after delete prof: ', e);
						return res.badRequest(e);
					});
				break;

			// case 'validate':
			// 	// Validate: there is a name
			// 	// Return status and messages
			//
			// 	let errors = [];
			// 	let isValid = true;
			//
			// 	if (!data.level.name) {
			// 		errors.push('A name must be given');
			// 		isValid = false;
			// 	}
			//
			// 	return res.send({
			// 		isValid: isValid,
			// 		errors: (errors.length > 0 ? errors : null),
			// 		level: data.level
			// 	});
			// 	break;
		}
	}

}

