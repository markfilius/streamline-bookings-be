/**
 * venuesController
 *
 * @description :: Server-side logic for managing venues
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const db = dbService.connect();
const hashService = require('sails-service-hash');
const bcrypt = hashService('bcrypt');
let   shortid = require('shortid');


module.exports = {

	/**
	 * Return a list of venues and the parent school
	 */
	listVenues(req, res) {
		// if (!allowedService.isAllowed(req.header('token'), 'canManageVenues')) {
		// 	return res.forbidden('This action is not allowed');
		// }

		// const data = req.body;
		// const token = req.header('token') || data.token;

		db
			.run('match (schools:School)-[:HasVenue]->(venues:Venue) return schools, venues')
			.then(result => {

				let schools = dbService.formatResult(result, 'schools');
				let venues = dbService.formatResult(result, 'venues');

				sails.log.info('AfterVenuesFetch:', result, venues);

				if (!venues || venues.length == 0) {
					return res.notFound('No schools or venues have been found');
				}

				// Add the school to each venue
				venues.forEach((venue, index) => {
					venue.school = schools[index];
				});

				return res.send({
					venues: venues
				})
			})
			.catch(err => {
				sails.log.error(err);
				return res.notFound('Error while getting venues');
			});
	},

}

