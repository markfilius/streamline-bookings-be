/**
 * calendarController
 *
 * @description :: Server-side logic for managing calendar
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const moment      = require('moment');
let   Class       = require('../models/Class');
let   Payment     = require('../models/Payment');


module.exports = {

	/**
	 * Return the contents for a calendar for a venue
	 */
	listCalendar(req, res) {
		if (!allowedService.isAllowed(req.header('token'), 'canManageCalendar')) {
			return res.forbidden('This action is not allowed');
		}

		// const data = req.body;
		const token = req.header('token');

		Class
			.listAll(token)
			.then(response => {
				// sails.log.info('CLASS LIST ALL SUCCESS', response);
				return res.send(response);
			})
			.catch(response => {
				return res.badRequest(response);
			});
	},

	/**
	 * Return the classes for a level for a venue
	 *
	 * Will be called from the carers app
	 */
	listClasses(req, res) {
		const data = req.body;

		const levelId = data.levelId;
		const token = data.token;
		const decoded = jwTokenService.decode(token);
		// const venue = decoded.payload.venue;
		const person = decoded.payload.person;

		sails.log.info('LIST CLASSES DATA', levelId);

		if (!person.canMakeBookings) {
			return res.forbidden('This action is not allowed');
		}

		// Get the classes for this venue, for that levelId, future only
		Class
			.listAll(token, levelId, true)
			.then(response => {
				return res.send(response);
			})
			.catch(response => {
				return res.notFound(response);
			});
	},

	/**
	 * Update the calendar with classes
	 *
	 * Will be called
	 * - from the admin website by pools to manage the calendar
	 * - from the carers app to book a lesson
	 */
	updateCalendar(req, res) {
		const data = req.body;

		const id = data.class.id;
		const action = req.param('action');
		const token = req.header('token') || data.token;
		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;
		const person = decoded.payload.person;

		sails.log.info('UPDATE DATA', action, id, data);

		let validated;
		let swimmer = {};
		let cypher = '';
		switch (action) {

			// Add or update a class
			case 'update':

				if (!allowedService.isAllowed(token, 'canManageCalendar')) {
					return res.forbidden('This action is not allowed');
				}

				validated = Class.validate(data.class);
				if (!validated.isValid) {
					sails.log.info('INVALID CLASS', validated.errors);
					return res.badRequest(validated.errors);
				}

				if (id && id != 'none') {
					// Update existing
					Class
						.update(token, data.class)
						.then(response => {
							return res.send(response);
						})
						.catch(response => {
							return res.notFound(response);
						});

				} else {
					// Create new class
					Class
						.create(token, data.calendar, data.class)
						.then(response => {
							return res.send(response);
						})
						.catch(response => {
							return res.badRequest(response);
						});
				}
				break;

			// Generate the repeating classes
			case 'generate':
				if (!allowedService.isAllowed(token, 'canManageCalendar')) {
					return res.forbidden('This action is not allowed');
				}

				// validated = Class.validate(data.class);
				// if (!validated.isValid) {
				// 	sails.log.info('INVALID CLASS', validated.errors);
				// 	return res.badRequest(validated.errors);
				// }

				if (id && id != 'none') {
					Class
						.generate(token, data.calendar, data.class)
						.then(response => {
							return res.send(response);
						})
						.catch(response => {
							return res.notFound(response);
						});

				}
				break;

			// Delete a class
			case 'delete':

				if (!allowedService.isAllowed(token, 'canManageCalendar')) {
					return res.forbidden('This action is not allowed');
				}

				if (id && id != 'none') {
					// Delete the relevant relationship and node
					sails.log.info('DELETING RELATIONSHIP FOR', data.calendar, data.class);
					Class
						.delete(token, data.calendar, data.class)
						.then(response => {
							return res.send(response);
						})
						.catch(response => {
							return res.badRequest(response);
						});
				}
				break;

			// Find if a class is deletable or not
			case 'notDeletable':

				if (!allowedService.isAllowed(token, 'canManageCalendar')) {
					return res.forbidden('This action is not allowed');
				}

				if (id && id != 'none') {
					// Delete the relevant relationship and node
					sails.log.info('FIND NOTDELETABLE FOR', data.class);
					Class
						.isNotDeletable(token, data.class)
						.then(response => {
							return res.send(response);
						})
						.catch(response => {
							return res.badRequest(response);
						});
				}
				break;

			// Book or cancel a class
			case 'book':
			case 'cancel':
				if (!person.canMakeBookings) {
					return res.forbidden('This action is not allowed');
				}

				validated = Class.validate(data.class);
				if (!validated.isValid) {
					sails.log.info('INVALID CLASS');
					return res.badRequest(validated.errors);
				}
				if (!id || id === 'none') {
					sails.log.info('NO CLASS GIVEN');
					return res.badRequest('A class must be selected');
				}

				swimmer = data.swimmer;
				if (!swimmer) {
					sails.log.info('NO PERSON GIVEN');
					return res.badRequest('A person must be selected');
				}

				if (action === 'cancel') {
					let hoursDiff = -1 * moment().diff(data.class.datetime, 'hours');
					if (hoursDiff < 24) return res.badRequest('Cancelling must be more than 24 hours in advance');
				}

				// Update a swimmer for this class
				if (action === 'book') {

					data.class.oneOrTerm = data.oneOrTerm;

					// In parallel: handlePayment administration ....
					this['calendar/handlepayment'](res, token, venue, person, data.class, swimmer, data.payment);

					// .... and the booking itself
					return this['calendar/bookswimmerintoclass'](res, token, venue, person, data.class, swimmer);
				}
				if (action === 'cancel') {
					// In parallel: handleCrediting ....
					this['calendar/handlecrediting'](res, token, venue, person, data.class, swimmer);

					// .... and removal
					return this['calendar/removeswimmerfromclass'](res, token, venue, person, data.class, swimmer);

				}
				break;

			// Join the waiting list for a class
			case 'joinwait':
			case 'leavewait':
				if (!person.canMakeBookings) {
					return res.forbidden('This action is not allowed');
				}

				if (!id || id === 'none') {
					sails.log.info('NO CLASS GIVEN');
					return res.badRequest('A class must be selected');
				}

				swimmer = data.swimmer;
				if (!swimmer) {
					sails.log.info('NO PERSON GIVEN');
					return res.badRequest('A person must be selected');
				}

				if (action === 'joinwait') {
					// A swimmer joins the waitingList for this class
					Class
						.joinWaitingList(token, venue, person, data.class, swimmer, data.paymentMethod.id)
						.then(response => {
							return res.send({queueLength: response.queueLength});
						})
						.catch(response => {
							sails.log.warn('CLASSWAITINGLIST Error ', response);
							return res.notFound('Error while joining waiting list ' + response);
						});
				}
				if (action === 'leavewait') {
					// A swimmer leaves the waitingList for this class
					Class
						.leaveWaitingList(token, venue, person, data.class, swimmer)
						.then(response => {
							return res.send({queueLength: response.queueLength});
						})
						.catch(response => {
							sails.log.warn('CLASSWAITINGLIST Error ', response);
							return res.notFound('Error while joining waiting list ' + response);
						});
				}
				break;
		}
	},

	bookSwimmerIntoClass(res, token, venue, person, oneClass, swimmer) {

		return Class
			.book(token, person, oneClass, swimmer)
			.then(response => {
				sails.log.info('CLASS BOOK success', response);

				// Get the classes for this venue, for that levelId, future only and return to mobile app
				Class
					.listAll(token, oneClass.level.id, true)
					.then(response => {
						return res.send(response);
					})
					.catch(response => {
						return res.notFound(response);
					});
			})
			.catch(response => {
				sails.log.warn('CLASS BOOK Error ', response);
				return res.notFound('Error while booking person into class: ' + response);
			});
	},

	handlePayment(res, token, venue, person, oneClass, swimmer, payment) {

		// Create a payment and invoice
		Payment
			.create(token, payment, oneClass, swimmer)
			.then(response => {
				sails.log.info('CALENDARCONTROLLERHANDLEPAYMENT success', response);
			})
			.catch(response => {
				sails.log.warn('CALENDARCONTROLLERHANDLEPAYMENT error', response);
				res.badRequest('Error while creating payment: ' + response);
			});
	},

	removeSwimmerFromClass(res, token, venue, person, oneClass, swimmer) {

		Class
			.removeSwimmer(token, oneClass, swimmer)
			.then(response => {
				sails.log.info('CALENDARREMOVESWIMMER success', response);

				// Expect response === true
				return res.send(response);
			})
			.catch(response => {
				sails.log.warn('CALENDARREMOVESWIMMER error', response);
				res.badRequest('Error while removing swimmer: ' + response);
			});

	},

	handleCrediting(res, token, venue, person, oneClass, swimmer) {

		// Update the credits after cancelling a lesson
		sails.log.info('CREATE CREDITS FOR', token, oneClass, swimmer);
		Payment
			.creditPayment(token, oneClass, swimmer)
			.then(response => {
				sails.log.info('CALENDARHANDLECREDIT success', response);
			})
			.catch(response => {
				sails.log.warn('CALENDARHANDLECREDIT error', response);
				res.badRequest('Error while crediting: ' + response);
			});

	},

}

