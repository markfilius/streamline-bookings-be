/**
 * settingsController
 *
 * @description :: Server-side logic for managing settings
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const db          = dbService.connect();
const hashService = require('sails-service-hash');
const bcrypt      = hashService('bcrypt');
let   moment      = require('moment');
let   shortid     = require('shortid');
let   validate    = require('validate.js');
let   fileSystem  = require('fs');
let   Class       = require('../models/Class');



let   beVersionNumber = '1.0'



module.exports = {

	/**
	 * Return the version
	 */
	getVersion(req, res) {
		return res.send({
			version: beVersionNumber,
		});
	},

	/**
	 * Return a list of settings for a school that has this venue
	 */
	listSettings(req, res) {
		const data = req.body;
		// const venue = data.venue;
		const token = req.header('token');
		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		sails.log.info('LISTSETTINGS', venue, req.wantsJSON);

		if (!allowedService.isAllowed(token, 'canManageSettings')) {
			return res.forbidden('This action is not allowed');
		}

		let venuesPromise = db
			.run('match (school:School)-[:HasVenue]->(venue:Venue {id: $venueId}) ' +
				'with school ' +
				'match (school)-[:HasVenue]->(venues:Venue)' +
				'return school, venues',
				{
					venueId: venue.id,
				}
			);

		let termsPromise = db
			.run('match (venue:Venue {id: $venueId})-[:HasTerm]->(terms:Term)' +
				'where not exists (terms.deletedAt)  ' +
				'optional match (terms)<-[:InTerm]-(:Class)<-[:SwimsIn]-(swimmers:Person) ' +
				'return count(swimmers) as nrSwimmers, terms order by terms.startDate desc',
				{
					venueId: venue.id,
				}
			);

		Promise.all([venuesPromise, termsPromise])
			.then(results => {

				let school = dbService.formatResult(results[0], 'school');
				let venues = dbService.formatResult(results[0], 'venues');
				let terms = dbService.formatResult(results[1], 'terms');
				let nrSwimmers = dbService.formatResult(results[1], 'nrSwimmers');

				venues = helperService.uniquifyArray(venues);

				// React wants a key field for iterable data structures. Much easier/cleaner to do it here than add clientside
				// And add some states: isActive, isCurrent
				terms = terms.map((term, index) => {
					term.key = term.id;

					// Define if current
					term.isCurrent = moment().isBetween(term.startDate, moment(term.endDate).add(1, 'day')) ? true : false;

					// Define if active = current or has classes with enrolled swimmers
					term.isActive = term.isCurrent || (nrSwimmers[index] ? true : false);

					return term;
				});

				sails.log.info('AfterSettingsFetch:', school, venues, terms);

				if (!school || school.length == 0) {
					return res.notFound('A school has not been found for this location');
				}

				return res.send({
					school: school[0],
					venues: venues,
					terms: terms,
				});
			})
			.catch(err => {
				sails.log.error(err);
				return res.notFound('Error while getting settings for this location');
			});
	},

	/**
	 * Update, delete an setting for ......
	 */
	updateSetting(req, res) {

		const data = req.body;
		const action = req.param('action');
		const token = req.header('token');
		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;
		let selectedVenue = data.selectedVenue || null;
		let school = data.school || null;
		let term = data.term || null;

		sails.log.info('UPDATE DATA', action, school, selectedVenue, term);
		// sails.log.info('UPDATE DATA2', data);
		// sails.log.info('TOKENDECODED', decoded, venue);

		if (!allowedService.isAllowed(token, 'canManageSettings')) {
			return res.forbidden('This action is not allowed');
		}

		// Write any logo to the images folder
		let logoFile;
		if (selectedVenue && selectedVenue.logo) {
			let regexGroups = selectedVenue.logo.match(/^(data:)((.)+)(;base64,)/);
			if (regexGroups && regexGroups.length >= 2) {

				// Construct the file name for storage
				let fileType = '.' + regexGroups[2].replace('image/', '');
				logoFile = 'logos/' + shortid.generate() + fileType;
				let fileName = sails.config.imagesRootFolder + logoFile;

				// Get the image data
				let base64Data = selectedVenue.logo.replace(/^data:image\/(.)+;base64,/, '');

				sails.log.info('WRITING LOGOFILE', logoFile);
				fileSystem.writeFile(fileName, base64Data, 'base64', function (err) {
					if (err) return res.badRequest('Unknown error while saving the logo');
				});
			}
		}

		switch (action) {

			case 'update':

				let validated = this['settings/validatesetting'](school, selectedVenue, term);
				if (!validated || !validated.isValid) {
					return res.badRequest(validated.errors);
				}

				if (school) {
					// Update existing school data
					db
						.run('match (school:School {id: $schoolId}) set ' +
							'school.abn         = $abn, ' +
							'school.name        = $name, ' +
							'school.phoneNumber = $phoneNumber, ' +
							'school.website     = $website ' +
							'return school',
							{
								schoolId: school.id,
								abn: school.abn || '',
								name: school.name || '',
								phoneNumber: school.phoneNumber || '',
								website: school.website || '',
							}
						)
						.then(result => {
							sails.log.info('AfterSchoolUpdate:', result);
							let school = dbService.formatResult(result, 'school');
							return res.send({school: school[0]});
						})
						.catch(err => {
							sails.log.error(err);
							sails.log.info('Error while finding school to update ', err);
							return res.notFound('Error while finding school to update ' + err);
						});
				}

				if (selectedVenue) {

					if (!selectedVenue.id && selectedVenue.id !== 'none') {
						// Create new venue (node)
						sails.log.info('CREATING', selectedVenue);

						// Generate an unique id
						let id = shortid.generate();

						// Find my school
						school = decoded.payload.school;

						// Create the node
						db
							.run(
								'merge (venue:Venue {' +
								'id         : "' + id + '", ' +
								'name       : "' + selectedVenue.name + '" ' +
								'}) return venue'
							)
							.then(function (result) {
								sails.log.info('Suc create venue', result)

								let venue = dbService.formatResult(result, 'venue');
								venue = venue[0];

								// Create the relationship
								db
									.run(
										'match (v:Venue {id: "' + venue.id + '"}) ' +
										'match (s:School {id: "' + school.id + '"}) ' +
										'merge (s)-[:HasVenue]->(v)'
									)
									.then(function (s) {
										sails.log.info('Suc create HasVenue', s)
										return res.send({
											venue: venue
										});
									})
									.catch(function (e) {
										sails.log.info('Err create HasVenue:', e)
										return res.badRequest(e);
									});

							})
							.catch(function (e) {
								sails.log.info('Err create venue:', e)
								return res.badRequest(e);
							});

					} else {

						// Update this venue
						db
							.run('match (venue:Venue {id: $venueId}) set ' +
								'venue.abn        = $abn, ' +
								'venue.accountNumber = $accountNumber, ' +
								'venue.accountName = $accountName, ' +
								'venue.address1    = $address1, ' +
								'venue.address2    = $address2, ' +
								'venue.address3    = $address3, ' +
								'venue.bsb         = $bsb, ' +
								'venue.email       = $email, ' +
								(logoFile ? 'venue.logo = $logo, ' : '') +
								'venue.name        = $name, ' +
								'venue.phoneNumber = $phoneNumber, ' +
								'venue.website     = $website ' +
								'return venue',
								{
									venueId: selectedVenue.id,
									abn: selectedVenue.abn || '',
									accountNumber: selectedVenue.accountNumber || '',
									accountName: selectedVenue.accountName || '',
									address1: selectedVenue.address1 || '',
									address2: selectedVenue.address2 || '',
									address3: selectedVenue.address3 || '',
									bsb: selectedVenue.bsb || '',
									email: selectedVenue.email || '',
									logo: logoFile || '',
									name: selectedVenue.name || '',
									phoneNumber: selectedVenue.phoneNumber || '',
									website: selectedVenue.website || '',
								}
							)
							.then(result => {
								sails.log.info('AfterVenueUpdate:', result);
								let venue = dbService.formatResult(result, 'venue');
								return res.send({
									venue: venue[0],
								});
							})
							.catch(err => {
								sails.log.error(err);
								sails.log.info('Error while finding venue to update ', err);
								return res.notFound('Error while finding venue to update ' + err);
							});

					}
				}

				if (term) {

					if (!term.id && term.id !== 'none') {
						// Add term data to this venue
						sails.log.info('CREATING', term);

						// Generate an unique id
						let termId = shortid.generate();
						db
							.run(
								'match (venue:Venue {id: $venueId}) ' +
								'merge (venue)-[:HasTerm]->(term:Term {' +
									'id        : $termId, ' +
									'name      : $name, ' +
									'startDate : $startDate, ' +
									'endDate   : $endDate, ' +
									'termRate  : $termRate, ' +
									'singleRate: $singleRate ' +
								'}) return term',
								{
									termId: termId,
									venueId: venue.id,
									name: term.name,
									startDate: term.startDate,
									endDate: term.endDate,
									termRate: parseFloat(term.termRate) || 0,           // Expect the value in cents
									singleRate: parseFloat(term.singleRate) || 0,       // Expect the value in cents
								}
							)
							.then(result => {
								sails.log.info('AfterTermAddToVenue:', result);
								let term = dbService.formatResult(result, 'term');
								return res.send({term: term[0]});
							})
							.catch(err => {
								sails.log.error(err);
								sails.log.info('Error while adding term to venue ', err);
								return res.notFound('Error while adding term to venue ' + err);
							});

					} else {

						// Update this term
						db
							.run(
								'match (venue:Venue {id: $venueId}) ' +
								'match (venue)-[:HasTerm]->(term:Term {id: $termId}) ' +
								'set term.name      = $name ' +
								'set term.startDate = $startDate ' +
								'set term.endDate   = $endDate ' +
								'set term.termRate  = $termRate ' +
								'set term.singleRate= $singleRate ' +
								'return term',
								{
									termId: term.id,
									venueId: venue.id,
									name: term.name,
									startDate: term.startDate,
									endDate: term.endDate,
									termRate: parseFloat(term.termRate) || 0,           // Expect the value in cents
									singleRate: parseFloat(term.singleRate) || 0,       // Expect the value in cents
								}
							)
							.then(result => {
								sails.log.info('AfterTermUpdate:', result);
								let term = dbService.formatResult(result, 'term')[0];

								return Class
									.attachClassesToTerm(token, term)
									.then(response => {
										sails.log.info('Success after attaching classes ', response);

										return res.send({
											term: term,
											countInTerm: response.countClassesInTerm,
											countNotInTerm: response.countClassesNotInTerm,
										});
									})
									.catch(response => {
										sails.log.info('Error while attaching classes ', response);
										return res.notFound(response);
									})

							})
							.catch(err => {
								sails.log.error(err);
								sails.log.info('Error while finding term to update ', err);
								return res.notFound('Error while finding term to update ' + err);
							});
					}
				}

				break;

			case 'delete':
				if (selectedVenue) {

					// Find my school
					school = decoded.payload.school;

					// Delete the relevant relationship and node
					sails.log.info('DELETING RELATIONSHIP FOR', selectedVenue, school);

					if (selectedVenue.id === venue.id) {
						sails.log.info('Err cant remove myself');
						return res.badRequest('Can\'t remove the current location from ' + school.name);
					}

					// TODO delete the venue too if no other HasVenue relationships
					db
						.run(
							'match ' +
							'(school:School {id: "' + school.id + '"}) ' +
							'-[has:HasVenue]-> ' +
							'(venue:Venue {id: "' + selectedVenue.id + '"}) ' +
							'delete has'
						)
						.then(function () {
							sails.log.info('Suc after delete venue: ');
							return res.ok();
						})
						.catch(function (e) {
							sails.log.info('Err after delete venue: ', e);
							return res.badRequest(e);
						});
				}

				if (term) {

					// Delete the relevant relationship and node
					sails.log.info('DELETING TERM', term);

					db
						.run(
							'match (venue:Venue {id: $venueId})-[hasTerm:HasTerm]->(term:Term {id: $termId}) ' +
							'set term.deletedAt = $deletedAt ' +
							'set term.deletedWhat = $deletedWhat ' +
							'with venue, term ' +
							'optional match (classesIn:Class) ' +
								'where not exists(classesIn.deletedAt) and (classesIn)-[:InTerm]->(term) ' +
							'with venue, term, count(classesIn) as countClassesInTerm ' +
							'merge (venue)-[:HasDeletedList]->(deletedList:DeletedList) ' +
							'merge (deletedList)-[:IsDeleted]->(term) ' +
							'return countClassesInTerm',
							{
								venueId: venue.id,
								termId: term.id,
								deletedAt: moment().format(),
								deletedWhat: term.name,
							}
						)
						.then(function (result) {
							sails.log.info('Suc after delete term: ', result);

							let count = dbService.formatResult(result, 'countClassesInTerm')[0];

							return res.send({
								countInTerm: count,
							});
						})
						.catch(function (e) {
							sails.log.info('Err after delete term: ', e);
							return res.badRequest(e);
						});
				}
				break;

			case 'validate':
				// Validate: there is a name
				// Return status and messages

				return res.send(
					this['settings/validatesetting'](data.setting)
				);
				break;
		}
	},

	validateSetting (school, venue, term) {
		let errors = [];
		let isValid = true;
		let validateErrors = {};

		if (school) {
			if (!school.name) {
				errors.push('A school name must be given');
				isValid = false;
			}

			// Validate website, length of abn
			if (school.website) validateErrors = Object.assign(validateErrors, validate(school, {website: {url: true}}));
			if (school.abn) validateErrors = Object.assign(validateErrors, validate(school, {abn: {length: {is: 11}}}));

			// Add the validate.js errors to the list
			if (!helperService.isEmptyObject(validateErrors)) {
				isValid = false;
				for (let prop in validateErrors) {
					if (validateErrors.hasOwnProperty(prop)) {
						errors.push(validateErrors[prop][0]);
					}
				}
			}
			return {
				isValid: isValid,
				errors: (errors.length > 0 ? errors : null),
			};
		}

		if (venue) {
			if (!venue.name) {
				errors.push('A location name must be given');
				isValid = false;
			}

			// Validate email, website, length of abn and bsb
			if (venue.email) validateErrors = Object.assign(validateErrors, validate(venue, {email: {email: true}}));
			if (venue.website) validateErrors = Object.assign(validateErrors, validate(venue, {website: {url: true}}));
			if (venue.abn) validateErrors = Object.assign(validateErrors, validate(venue, {abn: {length: {is: 11}}}));
			if (venue.bsb) validateErrors = Object.assign(validateErrors, validate(venue, {bsb: {length: {minimum: 6, maximum: 7}}}));

			// Add the validate.js errors to the list
			if (!helperService.isEmptyObject(validateErrors)) {
				isValid = false;
				for (let prop in validateErrors) {
					if (validateErrors.hasOwnProperty(prop)) {
						errors.push(validateErrors[prop][0]);
					}
				}
			}

			return {
				isValid: isValid,
				errors: (errors.length > 0 ? errors : null),
			};
		}

		if (term) {
			if (!term.name) {
				errors.push('A term name must be given');
				isValid = false;
			}
			if (!term.startDate) {
				errors.push('A term start date must be given');
				isValid = false;
			}
			if (!term.endDate) {
				errors.push('A term end date must be given');
				isValid = false;
			}
			if (term.endDate <= term.startDate) {
				errors.push('The term start date must be earlier than the end date');
				isValid = false;
			}
			if (!term.termRate || term.termRate <= 0) {
				errors.push('The term rate must be given');
				isValid = false;
			}
			if (!term.singleRate || term.singleRate <= 0) {
				errors.push('The single rate must be given');
				isValid = false;
			}

			return {
				isValid: isValid,
				errors: (errors.length > 0 ? errors : null),
			};

		}

		return {
			isValid: isValid,
			errors: (errors.length > 0 ? errors : null),
		};
	},

	/**
	 * Add or delete a proficiency for a venue
	 */
	updateProficiency (req, res) {
		const data = req.body;

		const settingId = data.setting.id;
		const token = req.header('token');
		const action = req.param('action');

		sails.log.info('UPDATE DATA', action, settingId, data);

		if (!allowedService.isAllowed(token, 'canManageSettings')) {
			return res.forbidden('This action is not allowed');
		}

		if (!settingId) return;

		switch (action) {

			case 'update':

				if (!data.proficiency.name) return;

				if (!data.proficiency.id) {

					// Add this proficiency to the setting
					let proficiencyId = shortid.generate();

					db
						.run('match (l:Setting {id: "' + settingId + '"}) ' +
							 'create (proficiency:Proficiency {' + 'name: "' + data.proficiency.name + '", id: "' + proficiencyId + '", order: "-1"}) ' +
							 'create (l)-[:HasProficiency]->(proficiency) ' +
							 'return proficiency'
						)
						.then(result => {
							sails.log.info('AfterProficiencyAdd:', result);
							let proficiency = dbService.formatResult(result, 'proficiency');
							return res.send({proficiency: proficiency[0]});
						})
						.catch(err => {
							sails.log.error(err);
							sails.log.info('Error while finding setting/proficiency to add ', err);
							return res.notFound('Error while finding setting/proficiency to add ' + err);
						});

				} else {
					// Update this proficiency
					db
						.run('match (proficiency:Proficiency {id: "' + data.proficiency.id + '"}) set ' +
							'proficiency.name  = "' + data.proficiency.name + '", ' +
							'proficiency.order = "' + data.proficiency.order + '" ' +
							' return proficiency'
						)
						.then(result => {
							sails.log.info('AfterProficiencyUpdate:', result);
							let proficiency = dbService.formatResult(result, 'proficiency');
							return res.send({proficiency: proficiency[0]});
						})
						.catch(err => {
							sails.log.error(err);
							sails.log.info('Error while finding proficiency to update ', err);
							return res.notFound('Error while finding proficiency to update ' + err);
						});
				}
				break;

			case 'delete':
				if (!data.proficiencyId) return;

				// Delete the relevant relationship and node
				const decoded = jwTokenService.decode(token);
				const venue = decoded.payload.venue;

				sails.log.info('DELETING PROFICIENCY ', data.proficiencyId);
				sails.log.info('TOKENDECODED', decoded, venue);

				db
					.run(
						'match ' +
						'(proficiency:Proficiency {id: "' + data.proficiencyId + '"}) ' +
						'<-[h:HasProficiency]- ' +
						'(l:Setting {id: "' + data.setting.id + '"}) ' +
						'delete proficiency, h ' +
						'return proficiency'
					)
					.then(function (response) {
						sails.log.info('Suc after delete prof: ', response);
						let proficiency = dbService.formatResult(response, 'proficiency');
						return res.ok(proficiency[0]);
					})
					.catch(function (e) {
						sails.log.info('Err after delete prof: ', e);
						return res.badRequest(e);
					});
				break;

			// case 'validate':
			// 	// Validate: there is a name
			// 	// Return status and messages
			//
			// 	let errors = [];
			// 	let isValid = true;
			//
			// 	if (!data.setting.name) {
			// 		errors.push('A name must be given');
			// 		isValid = false;
			// 	}
			//
			// 	return res.send({
			// 		isValid: isValid,
			// 		errors: (errors.length > 0 ? errors : null),
			// 		setting: data.setting
			// 	});
			// 	break;
		}
	},

}

