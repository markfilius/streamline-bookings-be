/**
 * StaffController
 *
 * @description :: Server-side logic for managing staff persons: instructors, admins and general staff
 */

const db            = dbService.connect();
const moment        = require('moment');
const hashService   = require('sails-service-hash');
const bcrypt        = hashService('bcrypt');
let   shortid       = require('shortid');
const nodemailer    = require('nodemailer');
const Person        = require('../models/Person');

// Provide an empty person to ensure all fields are available
const emptyPerson = {
	firstName: '',
	lastName: '',
	displayName: '',
	email: '',
	phoneNumber: '',
	address1: '',
	address2: '',
	address3: '',
	emergency1: '',
	emergency2: '',
	dob: '',
	password: '',
	passwordConfirm: '',
	setPassword: false,
	isInstructor: false,
	isAdmin: false,
	isActive: false,
};


// TODO move more to the Person model


module.exports = {

	/**
	 * Return a list of staff for a venue
	 */
	listStaff(req, res) {
		if (!allowedService.isAllowed(req.header('token'), 'canManageStaff')) {
			return res.forbidden('This action is not allowed');
		}

		const data = req.body;
		const token = req.header('token');

		Person
			.list(token, 'staff', data.role)
			.then(response => {
				return res.send(response);
			})
			.catch(response => {
				return res.notFound(response);
			});
	},

	/**
	 * Return details and schedule for a single staffMember
	 */
	findStaffMember(req, res) {
		const data = req.body;

		sails.log.info('DATAFINDStaffMember', data);

		if (!allowedService.isAllowed(req.header('token'), 'canManageStaff')) {
			return res.forbidden('This action is not allowed');
		}

		if (!data.id) return;

		if (data.id == 'none') {
			// return empty staffMember object
			return res.send({
				staffMember: emptyPerson,
				schedule: [],
			})
		}

		let midnightUtc = moment().hours(0).minutes(0).seconds(0).utc();

		db
			.run('match (:Venue {name: $venueName}) ' +
				 '-[employs:Employs]-> ' +
				 '(person:Person {id: $personId}) ' +
				 'optional match (person)-[:Instructs]->(classes:Class) ' +
				 'where classes.datetime > $starttime ' +
				 'and   classes.datetime < $endtime ' +
				 'and   not exists(classes.deletedAt) ' +
				 'optional match (classes)-[:TeachesLevel]->(levels:Level) ' +
				 'return employs, person, classes, levels order by classes.datetime asc',
				{
					venueName: data.venue.name,
					personId: data.id,
					starttime: midnightUtc.format('YYYY-MM-DD') ,
					endtime:  midnightUtc.add(2, 'months').format('YYYY-MM-DD')
				})
			.then(result => {

				let staff = dbService.formatResult(result, 'person');
				let employs = dbService.formatResult(result, 'employs');
				let classes = dbService.formatResult(result, 'classes');
				let levels = dbService.formatResult(result, 'levels');

				// Create schedule from classes and level-per-class
				classes = helperService.uniquifyArray(classes);
				let schedule = [];
				classes.forEach((oneClass, index) => {
					if (helperService.isEmptyObject(oneClass)) return;

					oneClass.level = levels[index];
					schedule.push(oneClass);
				});

				// sails.log.info('AfterStaffFetchStaff:', result, staff, employs);
				// sails.log.info('AfterStaffFetchClasses:', classes);
				// sails.log.info('AfterStaffFetchLevels:', levels);
				// sails.log.info('AfterStaffFetchSchedule:', schedule);

				if (!staff || staff.length == 0) {
					return res.notFound('StaffMember ' + data.displayName + ' not found');
				}

				// Ensure all fields are available and nullify password
				staff = staff.map(staffMember => {

					// Merge the person with empty person to fill any default values
					staffMember = Object.assign({}, emptyPerson, staffMember);

					staffMember.password = null;

					return staffMember;
				});

				let staffMember = staff[0];

				// Update roles
				employs = employs[0];
				staffMember.isAdmin      = employs.admin ? true : false;
				staffMember.isInstructor = employs.instructor ? true : false;
				staffMember.isActive     = staffMember.isAdmin || staffMember.isInstructor || false;

				return res.send({
					staffMember: staffMember,
					schedule: schedule,
				});
			})
			.catch(err => {
				sails.log.error(err);
				return res.notFound('Error while getting staffMember ' + data.displayName);
			});
	},

	/**
	 * Update, delete an staffMember for a venue, or check if an email address already exists
	 */
	updateStaffMember(req, res) {
		const data = req.body;

		const id = data.staffMember.id;
		const token = req.header('token');
		const action = req.param('action');

		sails.log.info('UPDATE DATA', action, id, data);
		
		if (!allowedService.isAllowed(token, 'canManageStaff')) {
			return res.forbidden('This action is not allowed');
		}

		switch (action) {

			case 'update':

				const decoded = jwTokenService.decode(token);
				const venue = decoded.payload.venue;
				sails.log.info('TOKENDECODED', decoded, venue);

				let validated = sails.helpers.staff.validateStaffMember(data).execSync();

				if (!validated.isValid) {
					return res.badRequest(validated.errors);
				}

				if (id && id != 'none') {

					// Update existing
					sails.log.info('UPDATING', data.staffMember);
					db
						.run('match (person:Person {id: $personId}) set ' +
							'person.firstName   = $firstName, ' +
							'person.lastName    = $lastName, ' +
							'person.displayName = $displayName, ' +
							'person.name        = $displayName, ' +
							'person.email       = $email, ' +
							(data.staffMember.setPassword ? 'person.password = $password, ' : '') +
							'person.phoneNumber = $phoneNumber, ' +
							'person.dob         = $dob, ' +
							'person.address1    = $address1, ' +
							'person.address2    = $address2, ' +
							'person.address3    = $address3, ' +
							'person.emergency1  = $emergency1, ' +
							'person.emergency2  = $emergency2 ' +
							'with person ' +

							'match (:Venue {name: $venueName})-[employs:Employs]->(person) set ' +
							'employs.instructor = $isInstructor, ' +
							'employs.admin      = $isAdmin ' +

							'return employs, person',
							{
								personId: id,
								firstName: data.staffMember.firstName,
								lastName: data.staffMember.lastName,
								displayName: data.staffMember.displayName,
								email: data.staffMember.email,
								password: bcrypt.hashSync(data.staffMember.password || '-_r4Nd0m-_'),
								phoneNumber: data.staffMember.phoneNumber,
								dob: data.staffMember.dob,
								address1: data.staffMember.address1,
								address2: data.staffMember.address2,
								address3: data.staffMember.address3,
								emergency1: data.staffMember.emergency1,
								emergency2: data.staffMember.emergency2,
								venueName: venue.name,
								isInstructor: (data.staffMember.isInstructor ? true : false),
								isAdmin: (data.staffMember.isAdmin ? true : false),
							}
						)
						.then(result => {
							sails.log.info('AfterStaffMemberUpdate:', result);
							let staff = dbService.formatResult(result, 'person');
							let staffMember = staff[0];

							// Nullify password
							staffMember.password = null;

							// Update roles
							let employs = dbService.formatResult(result, 'employs');
							employs = employs[0];
							staffMember.isAdmin      = employs.admin ? true : false;
							staffMember.isInstructor = employs.instructor ? true : false;
							staffMember.isActive     = staffMember.isAdmin || staffMember.isInstructor || false;

							return res.send({staffMember: staffMember});
						})
						.catch(err => {
							sails.log.error('Error while finding staffMember to update ', err);
							return res.notFound('Error while finding staffMember to update ' + err);
						});

				} else {
					
					// Create new staffMember (node)
					sails.log.info('CREATING', data.staffMember);

					// Generate an unique id, the name (useful only for Neo4J) and the standard password
					const name = data.staffMember.displayName
						? data.staffMember.displayName
						: data.staffMember.firstName + ' ' + data.staffMember.lastName;
					const password = '$2a$10$LWGf0eVXuRRvGkAKg2ZIOOTswf6GBX1E1SgSwJLHRlgtGQl5FuFiC';        // 123
					let random = Math.floor(Math.random() * 100000) + 100000;
					let id = data.staffMember.firstName + data.staffMember.lastName + shortid.generate();
					id = id.replace(/\W+/g, '').toLowerCase();

					let self = this;

					// Create the node
					db
						.run(
							'merge (person:Person {' +
							'id          : $personId, ' +
							'firstName   : $firstName, ' +
							'lastName    : $lastName, ' +
							'displayName : $displayName, ' +
							'name        : $displayName, ' +
							'email       : $email, ' +
							(data.staffMember.setPassword ? 'password: $password, ' : '') +
							'phoneNumber : $phoneNumber, ' +
							'dob         : $dob, ' +
							'address1    : $address1, ' +
							'address2    : $address2, ' +
							'address3    : $address3, ' +
							'emergency1  : $emergency1, ' +
							'emergency2  : $emergency2, ' +
							'password    : $password ' +
							'}) return person',
							{
								personId: id,
								firstName: data.staffMember.firstName,
								lastName: data.staffMember.lastName,
								displayName: data.staffMember.displayName,
								email: data.staffMember.email,
								password: bcrypt.hashSync(data.staffMember.password || '_r4Nd0m-'),
								phoneNumber: data.staffMember.phoneNumber,
								dob: data.staffMember.dob,
								address1: data.staffMember.address1,
								address2: data.staffMember.address2,
								address3: data.staffMember.address3,
								emergency1: data.staffMember.emergency1,
								emergency2: data.staffMember.emergency2,
								password: password,
							}
						)
						.then(function (result) {
							sails.log.info('Suc create inst', result)

							let staff = dbService.formatResult(result, 'person');
							let staffMember = staff[0];

							// Nullify password
							staffMember.password = null;

							// Create the relationship
							db
								.run(
									'match (v:Venue {name: "' + venue.name + '"}) ' +
									'match (p:Person {id: "' + staffMember.id + '"}) ' +
									'merge (v)-[employs:Employs {' +
										'instructor: ' + (data.staffMember.isInstructor ? true : false) + ', ' +
										'admin: ' + (data.staffMember.isAdmin ? true : false) +
									'}]->(p) ' +
									'return employs'
								)
								.then(function (result) {
									sails.log.info('Suc create Employs', result);

									// Update roles
									let employs = dbService.formatResult(result, 'employs');
									employs = employs[0];
									staffMember.isAdmin      = employs.admin ? true : false;
									staffMember.isInstructor = employs.instructor ? true : false;
									staffMember.isActive     = staffMember.isAdmin || staffMember.isInstructor || false;

									return res.send({
										staffMember: staffMember
									});
								})
								.catch(function (e) {
									sails.log.error('Err create Employs:', e)
									return res.badRequest(e);
								});

						})
						.catch(function (e) {
							sails.log.error('Err create inst:', e)
							return res.badRequest(e);
						});
				}
				break;

			case 'delete':
				if (id && id != 'none') {
					// Delete the relevant relationship
					Person
						.delete(token, data.staffMember, 'staff')
						.then(function () {
							sails.log.info('Suc after delete staff: ');
							return res.ok();
						})
						.catch(function (e) {
							sails.log.error('Err after delete staff: ', e);
							return res.badRequest(e);
						});
				}
				break;
		}
	}
}

