/**
 * LoginController
 *
 * @description :: Server-side logic for managing logins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const db            = dbService.connect();
const hashService   = require('sails-service-hash');
const bcrypt        = hashService('bcrypt');
let   validate      = require('validate.js');
let   Group         = require('../models/Group');
let   Person        = require('../models/Person');
let   Message       = require('../models/Message');


module.exports = {

	login(req, res) {
		const data = req.body;
		sails.log.info('LoginAttempt:', data);

		// Validate input params
		if (!data.email || !data.password) return res.badRequest('Email and password required');

		if (data.email) {
			let validateErrors = validate(data, {
				email: {email: true}
			});
			if (validateErrors) return res.badRequest('Incorrect format for email address');
		}

		// Find the person for the email address
		// TODO active on mobile - do here on every login! e.g.
		// TODO set person.isActiveMobile = true
		db
			.run('match (person:Person) ' +
				    'where person.email =~ "(?i)' + data.email + '" ' +   // case insensitive match
				    'and not exists(person.deletedAt) ' +
				 'return person')
			.then(result => {
				sails.log.info('AfterPersonFetch:', result);

				if (!result || result.records.length == 0) {
					return res.notFound('Email address not found');
				}

				let person = result.records[0].get('person').properties;
				if (!person.password) {
					return res.notFound('Password isn\'t set for ' + person.email);
				}

				const passwordOk = bcrypt.compareSync(data.password, person.password);
				if (!passwordOk) {
					return res.forbidden('Password incorrect');
				}

				if (data.fromMobile) {
					this['login/mobilelogin'](res, data, person);

				} else {
					this['login/weblogin'](res, data, person);
				}
			})
			.catch(err => {
				sails.log.info('AfterPersonFetch ERROR:', err);
				if (err.name === 'Neo4jError') {
					err = 'From the database that\'s probably unavailable';
				}
				return res.badRequest('Error: ' + err);
			});
	},

	webLogin (res, data, person) {

		// Login from the web is only for admins
		// Find my location and school
		// TODO - do we need Person.find to get person info??? not really, these are admins
		db
			.run('match (venue:Venue)-[employs:Employs]->(p:Person {email: $email}) where not exists(p.deletedAt) ' +
				'with venue, employs ' +
				'match (school:School)-[:HasVenue]->(venue) ' +
				'return school, venue, employs',
				{
					email: person.email
				})
			.then(result => {
				sails.log.info('venues', result);

				// TODO For now: only take the first venue
				let school = result.records[0].get('school').properties;
				let venue = result.records[0].get('venue').properties;
				let employs = result.records[0].get('employs').properties;

				sails.log.info('school+venue+employs', school, venue, employs);

				// The role and permissions
				venue.canManageStaff = venue.canManageLevels = venue.canManageSettings = false;
				venue.canManageCalendar = venue.canManageGroups = false;
				venue.isAdmin = venue.isInstructor = venue.isActive = false;
				if (employs.admin) {
					venue.isActive = true;
					venue.isAdmin = true;
					venue.canManageCalendar = true;
					venue.canManageGroups = true;
					venue.canManageLevels = true;
					venue.canManageSettings = true;
					venue.canManageStaff = true;
				}
				if (employs.instructor) {
					venue.isActive = true;
					venue.isInstructor = true;
				}

				person.password = null;
				return res.send({
					token: jwTokenService.issue({person, venue, school}),
					person: person,
					venue: venue,
					school: school
				})
			})
			.catch(err => {
				sails.log.error(err);
				return res.notFound('Error while getting location for this person');
			});
	},

	mobileLogin (res, data, person) {

		// Login from mobile is only for carers
		// Find my location, (family) group and dependants

		if (!person.isCarer) return res.forbidden('Login is only allowed for carers');

		// We want full info for this person and their group (including levels and financial info)
		let personPromise = Person.find(person.id);
		personPromise
			.then(response => {
				sails.log.info('FOUNDTHISPERSONtologin', response);

				let token = jwTokenService.issue({
					person: response.person,
					venue: response.venue,
				});

				return res.send({
					token: token,
					person: response.person,
					venue: response.venue,
					group: response.group,
					persons: response.persons,
				});
			})
			.catch(response => {
				sails.log.error(response);
				return res.notFound('Error while getting location and group for this person');
			});
	},

	register(req, res) {
		const data = req.body;
		sails.log.info('RegisterAttempt:', data);

		// Validate input params
		if (!data.person) return res.badRequest('Personal details are required');
		if (!data.venue) return res.badRequest('A school is required');

		let validateErrors = validate(data.person, {
			email: {presence: true, email: true},
			firstName: {presence: true, length: {minimum: 2}},
			lastName: {presence: true, length: {minimum: 2}},
			password: {presence: true, length: {minimum: 8}},
		});
		if (validateErrors) {
			let errorMessages = [];
			Object.values(validateErrors).forEach(value => {
				errorMessages.push(value.join(', '));
			});
			return res.badRequest(errorMessages.join(', '));
		}

		// Use this school and venue
		let school = data.venue.school;
		let venue = data.venue;
		venue.school = null;

		// Create the person
		let person = data.person;
		person.setPassword = true;
		person.isCarer = true;
		person.isDependant = false;
		person.isMainContact = true;

		// Create the person
		let group = {};
		group.name = person.lastName + ' family';

		// Create a token
		let token = jwTokenService.issue({person, venue, school});

		Person
			.exists(token, person)
			.then(response => {

				if (response > 0) {
					return res.badRequest('Email address ' + person.email + ' is already registered');
				}

				// Add the group to the db
				Group
					.create(token, group)
					.then(response => {
						sails.log.info('GROUP RETURNED IS', response);

						// Update the group is
						group = response.group;

						// Add this person to the group
						Person
							.create(token, person, group.id)
							.then(response => {
								sails.log.info('PERSON RETURNED IS', response);

								// Update the person
								person = response.person;

								// Send a message to the admins
								Message.send(token,
									{
										title: 'New registration from mobile app',
										content: person.firstName + ' ' + person.lastName +
												' (' + person.email + ')' +
												' has registered',
									}, {
										to: 'admins',
									});

								// Return a logged in status
								person.password = null;
								return res.send({
									token: token,
									person: person,
									venue: venue,
									group: group,
									persons: [person],
								});

							})
							.catch(err => {
								return res.badRequest('Unknown error while creating a person during register ' + err);
							});
					})
					.catch(err => {
						return res.badRequest('Unknown error while creating a group during register ' + err);
					});
			})
			.catch(err => {
				return res.badRequest('Unknown error while checking person exists during register ' + err);
			});
	},


}

