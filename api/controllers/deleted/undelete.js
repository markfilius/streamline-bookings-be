/**
 * deletedController
 *
 * @description :: Server-side logic for managing deleted items
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const db            = dbService.connect();
const moment        = require('moment');


module.exports =

	/**
	 * Undelete an item from the deletedList
	 * Return the updated list
	 *
	 * General rules:
	 *      objects can/should include a notDeletable="reason" property
	 *      outgoing relationships:
	 *          leave them so they are deleted or restored with the node
	 *      incoming relationships:
	 *          if any, then the node is not deletable
	 *          any list function must check on .deletedAt property
     *      on delete:
     *          add a deletedAt timestamp
	 *          add a deletedWhat string used as description in the bin
	 */
	function undelete(req, res) {

		sails.log.info('UNDELETE called');

		const data = req.body;
		const deleted = data.deleted || {};
		const token = req.header('token');
		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		let cypher =
			'match (venue {id: $venueId})-[:HasDeletedList]->(list:DeletedList) ' +
			'match (list)-[isDeleted:IsDeleted]->(deleted {id: $deletedId}) ' +
			'remove deleted.deletedAt ' +
			'remove deleted.deletedWhat ' +
			'delete isDeleted ' +
			'with list ' +
			'optional match (list)-[:IsDeleted]->(deleted) where deleted.deletedAt >= $fewDaysAgo ' +
			'return deleted order by deleted.deletedAt desc';

		return nodesPromise = db
			.run(cypher, {
				venueId: venue.id,
				deletedId: deleted.id,
				fewDaysAgo: moment().subtract(3, 'days').format(),
			})
			.then(result => {
				let deleted = dbService.formatResult(result, 'deleted');
				sails.log.info('AfterUNDeletedFetch:', result, deleted);
				return res.send({
					deleted: helperService.uniquifyArray(deleted),
				});
			})
			.catch(response => {
				sails.log.info('Error while getting UNdeleted ', response);
				return res.badRequest(response);
			});
	}



