/**
 * deletedController
 *
 * @description :: Server-side logic for managing deleted items
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const db            = dbService.connect();
const moment        = require('moment');


module.exports =

	/**
	 * Return a list of deleted items for this venue
	 */
	function listItems(req, res) {

		sails.log.info('LISTDELETEDITEMS called');

		const token = req.header('token');
		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		let cypher =
			'match (venue {id: $venueId})-[:HasDeletedList]->(list:DeletedList) ' +
			'optional match (list)-[:IsDeleted]->(deleted) where deleted.deletedAt >= $fewDaysAgo ' +
			'return deleted order by deleted.deletedAt desc';

		return nodesPromise = db
			.run(cypher, {
				venueId: venue.id,
				fewDaysAgo: moment().subtract(3, 'days').format(),
			})
			.then(result => {
				let deleted = dbService.formatResult(result, 'deleted');
				sails.log.info('AfterDeletedFetch:', result, deleted);
				return res.send({
					deleted: helperService.uniquifyArray(deleted),
				});
			})
			.catch(response => {
				sails.log.info('Error while getting deleted ', response);
				return res.badRequest(response);
			});
	}



