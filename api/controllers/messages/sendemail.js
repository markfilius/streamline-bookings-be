/**
 * messagesController - send email action
 *
 * @description :: Sending 1 or more emails
 */

const db            = dbService.connect();
const moment        = require('moment');
const Message       = require('../../models/Message');


module.exports =

	/**
	 * Send an email to a group of recipients
	 */
	async function sendEmail(req, res) {

		const data = req.body;
		const token = req.header('token') || data.token;
		let attachments = data.attachments || [];
		let content = data.content || '';
		let subject = data.subject || '';
		let from = data.from || '';
		let to = data.to || {};
		const decoded = jwTokenService.decode(token);
		const venue = decoded.payload.venue;

		sails.log.info('SEND EMAIL', data);

		Message
			.sendEmail({
				token: token,
				from: from,
				to: to,
				subject: subject,
				content: content,
				isHtml: false,
				attachments: attachments,
			})
			.then(response => {
				sails.log.info('send email response', response);
				res.send(response);
			})
			.catch(response => res.badRequest(response));
	}



