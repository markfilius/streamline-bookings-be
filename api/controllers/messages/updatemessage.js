/**
 * messagesController
 *
 * @description :: Server-side logic for managing messages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const moment    = require('moment');
const Message   = require('../../models/Message');


module.exports =

	/**
	 * Send a specific message or toggle read status for a message
	 */
	function updateMessage(req, res) {

		const data = req.body;
		let message = data.message || {};

		const token = req.header('token') || data.token;
		const action = req.param('action');

		sails.log.info('UPDATE MESSAGE', action, data);

		switch (action) {

			case 'toggle':

				Message
					.toggle(token, message)
					.then(messages => {
						// sails.log.info('MESSAGESPROMISE-TOGGLE-RETURNED', messages);
						return res.send({
							messages: messages,
						});
					})
					.catch(err => res.badRequest(err));

				break;

			case 'cancel-class':

				message.title = data.classChosen.person.firstName + data.classChosen.person.lastName + ' has Cancelled';
				message.content = data.classChosen.person.firstName + data.classChosen.person.lastName +
									' has cancelled for ' +
									data.classChosen.level.name +
									' at ' +
									moment(data.classChosen.datetime).format(sails.config.globals.momentDatetimeFormat);
				message.datetime = moment().format();

				Message
					.send(token, message, {to: 'admins'})
					.then(messages => {
						// sails.log.info('MESSAGESPROMISE-CANCELCLASS-RETURNED', messages);
						return res.send({
							messages: messages,
						});
					})
					.catch(err => res.badRequest(err));

				break;
		}
	}



