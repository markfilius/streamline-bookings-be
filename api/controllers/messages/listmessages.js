/**
 * messagesController
 *
 * @description :: Server-side logic for managing messages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const moment    = require('moment');
const Message   = require('../../models/Message');


module.exports =

	/**
	 * Return a list of messages for logged in person (ie in the token)
	 */
	function listMessages(req, res) {

		const token = req.header('token');
		// sails.log.info('LISTMESSAGES called');

		Message
			.getFor(token)
			.then(messages => {
				// sails.log.info('MESSAGESPROMISDRETURNED', messages);
				return res.send({
					messages: messages,
				});
			})
			.catch(err => res.badRequest(err));
	}



