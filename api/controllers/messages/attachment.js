/**
 * messagesController - upload and attachment
 *
 * @description :: Upload 1 attachment for use in an email
 */

const db            = dbService.connect();
const moment        = require('moment');
const Message       = require('../../models/Message');


module.exports =

	/**
	 * Send an email to a group of recipients
	 */
	async function attachment(req, res) {

		const data = req.body;
		const token = req.header('token') || data.token;
		let attachment = data.attachment || {};
		// const decoded = jwTokenService.decode(token);
		// const venue = decoded.payload.venue;

		sails.log.info('UPLOAD ATTACHMENT', attachment.name);

		Message
			.uploadAttachment(token, attachment)
			.then(response => {
				sails.log.info('attachment response', response);
				res.send(response);
			})
			.catch(response => res.badRequest(response));
	}



