let validate = require('validate.js');


module.exports = {

	friendlyName: 'Validate staff member',

	description: 'Validate staff member',

	sync: true,

	inputs: {
		staffMember: {
			type: 'ref',
			description: 'Staff member object',
			required: true,
		},
	},

	exits: {},

	fn: function (inputs, exits) {

 		// Validate: there is a last name, there is a display name, if email address exists, role is chosen
		// Return status, messages and adjusted person object

		let errors = [];
		let isValid = true;
		let staffMember = inputs.staffMember;
		let validateErrors;

		if (!staffMember.lastName) {
			errors.push('A last name must be given');
			isValid = false;
		}

		if (!staffMember.displayName) {
			errors.push('A display name must be given');
			isValid = false;
		}

		// Check staff member role and status
		staffMember.isInstructor = staffMember.isInstructor || false;
		staffMember.isAdmin      = staffMember.isAdmin || false;
		staffMember.isActive     = staffMember.isInstructor || staffMember.isAdmin || false;

		if (staffMember.isInactive && !staffMember.isActive) {
			staffMember.isActive = staffMember.isInstructor = staffMember.isAdmin = false;
		} else {
		    staffMember.isInactive = !staffMember.isActive;
		}

		if (!staffMember.isInstructor && !staffMember.isAdmin) {
			errors.push('Staff member ' + staffMember.displayName + ' is set as Inactive');
		}

		// Check existing email address and return. First: presence of an email address, then on unique email
		if (!staffMember.email) {
			errors.push('An email address must be given');
			isValid = false;
		}

		// Password
		if (staffMember.setPassword) {
			validateErrors = validate(staffMember, {
				password: { length: {minimum: 8}},
				passwordConfirm: {equality: 'password'},
			});
		}

		// Add the validate.js errors to the list
		if (validateErrors) {
			isValid = false;
			for (let prop in validateErrors) {
				if (validateErrors.hasOwnProperty(prop)) {
					errors.push(validateErrors[prop][0]);
				}
			}
		}

		return exits.success({
			isValid: isValid,
			errors: (errors.length > 0 ? errors : null),
			staffMember: staffMember
		});

		// TODO the test below is async, so either return a promise from this function, or pref, do this synchronously
		// Now test unique email
		// db
		// 	.run(
		// 		'match (person:Person {email: "' + staffMember.email + '"}) where person.id <> "' + staffMember.id + '" return person'
		// 	)
		// 	.then(result => {
		// 	  sails.log.info('AfterStaffMemberExists:', result);
		// 	  let staff = dbService.formatResult(result, 'person');
		//
		// 	  if (staff.length > 0) {
		// 	      errors.push('This email address already exists');
		// 	  }
		// 	  return {
		// 	      isValid: isValid,
		// 	      errors: (errors.length > 0 ? errors : null),
		// 	      staffMember: staffMember
		// 	  };
		// 	})
		// 	.catch(err => {
		// 		sails.log.error(err);
		// 		sails.log.info('Error while finding staffMember to update ', err);
		// 		return res.notFound('Error while finding staffMember to update ' + err);
		// 	});
		//
	}

};

