/**
 * Production environment settings
 *
 * This file can include shared settings for a production environment,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

let Buffer = require('buffer').Buffer;

if (!process.env.PORT || !process.env.BOLT_PORT) {
	console.log('You must set environment variables: PORT, BOLT_PORT and should set SLB_ENV');
	console.log('E.g. dev  environ: SLB_ENV=dev PORT=9057 BOLT_PORT=5702');
	console.log('E.g. demo environ: SLB_ENV=demo PORT=9058 BOLT_PORT=5802');
	console.log('');
	return;
}

module.exports = {

	// General
	port: process.env.PORT,                             // Active port

	// Database
	neo4j: {
		url: 'bolt://localhost:' + (process.env.BOLT_PORT).toString(),
		username: 'neo4j',
		password: 'pluizje',
	},

	// Folders
	// Get images from the public website
	attachmentsRootFolder: './attachments/',            // General  attachments
	imagesRootFolder: '../web/imgs/app/',               // Get images from the public website
	invoicesRootFolder: '../web/invoices/',             // Get invoices from the public website

	// Return messages with 403, 404, etc
	keepResponseErrors: true,

	// Set the log level
	log: { level: 'warn' },

	// Payment Gateway
	// FatZebra
	payGateUrl: 'https://gateway.sandbox.fatzebra.com.au/v1.0/',
	payGateAuth: 'Basic ' + Buffer.from('TESTstreamlinebookings:3df4b3bb71a31aa13bb2994ea04540c2bcd1f805').toString('base64'),
	currency: 'AUD',

	// JudoPay
	// payGateUrl: 'https://gw1.judopay-sandbox.com/',
	// payGateAuth: 'Basic ' + Buffer.from('CXqeHXubEnhgpz0L:a4024f6285af0e64096b5e0e6ae736d210a69dc14b7a562db17dc45d4829c2f7').toString('base64'),
	// payGateId: '100236-439',
	// currency: 'GBP',

	// Email Gateway: AWS SES
	smtpUsername: 'AKIAI4ERMZG6TCLC3D3Q',
	smtpPassword: 'Au4XvzvJ0itEX4ZhnWdn6jmIfuBinO9abyWEMIa5je8I',

};
