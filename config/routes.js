/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

    /***************************************************************************
    *                                                                          *
    * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
    * etc. depending on your default view engine) your home page.              *
    *                                                                          *
    * (Alternatively, remove this and add an `index.html` file in your         *
    * `assets` directory)                                                      *
    *                                                                          *
    ***************************************************************************/

    // '/': {
    //   view: 'homepage'
    // },

    /***************************************************************************
    *                                                                          *
    * Custom routes here...                                                    *
    *                                                                          *
    * If a request to a URL doesn't match any of the custom routes above, it   *
    * is matched against Sails route blueprints. See `config/blueprints.js`    *
    * for configuration options and examples.                                  *
    *                                                                          *
    ***************************************************************************/


    'POST /api/v1/login'                 : 'LoginController.login',
    'POST /api/v1/register'              : 'LoginController.register',

    'POST /api/v1/venues'                : 'VenuesController.listVenues',

    'POST /api/v1/staff'                 : 'StaffController.listStaff',
    'POST /api/v1/staff-member'          : 'StaffController.findStaffMember',
    'PUT  /api/v1/staff-member/:action'  : 'StaffController.updateStaffMember',

    'POST /api/v1/groups'                : 'GroupsController.listGroups',
    'POST /api/v1/group'                 : 'GroupsController.findGroup',
    'PUT  /api/v1/group/:action'         : 'GroupsController.updateGroup',

    'PUT  /api/v1/person/:action'        : 'GroupsController.updatePerson',

    'POST /api/v1/levels'                : 'LevelsController.listLevels',
    'POST /api/v1/level'                 : 'LevelsController.findLevel',
    'PUT  /api/v1/level/:action'         : 'LevelsController.updateLevel',

    'PUT  /api/v1/proficiency/:action'   : 'LevelsController.updateProficiency',

	'GET  /api/v1/version'               : 'SettingsController.getVersion',
	'POST /api/v1/settings'              : 'SettingsController.listSettings',
    'PUT  /api/v1/setting/:action'       : 'SettingsController.updateSetting',

    'POST /api/v1/calendar'              : 'CalendarController.listCalendar',
    'POST /api/v1/calendar/classes'      : 'CalendarController.listClasses',
    'PUT  /api/v1/calendar/:action'      : 'CalendarController.updateCalendar',

    'POST /api/v1/messages'              : 'messages.listmessages',
    'POST /api/v1/messages/send-email'   : 'messages.sendemail',
    'POST /api/v1/messages/attachment'   : 'messages.attachment',
    'PUT  /api/v1/messages/:action'      : 'messages.updatemessage',

    'POST /api/v1/deleted'               : 'deleted.listitems',
    'PUT  /api/v1/deleted/undelete'      : 'deleted.undelete',
};
