# be

a [Sails](http://sailsjs.org) application


# Database

_Copy the databse_
    
    cd streamlinebookings/
    cp -R db db-backup-<date>
    
_Clear the database_

    MATCH (n)
    DETACH DELETE n
    
or

    MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r
    
or

    $ rm -rf data/graph.db 
    and start up the server again
    

_Rebuild using_

    $ sails console
    dbService.migrate()         // See api/services/dbService.js
    

